﻿#include <iostream>

#include <dsl/Waveform.h>

int main()
{
    dsl::Waveform w;
    std::cout << w.gain << '\n';

    return 0;
}

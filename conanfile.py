from conan import ConanFile

class dsl4agilentRecipe(ConanFile):
    name = "dsl4agilent"
    version = "2.1.2"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Nicolas Leclercq"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/inputoutput/lib/dsl4agilent"
    description = "Agilent/Acqiris Digitizers Support Library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"

    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("acqirisdevicedriver/4.0.0@soleil/stable")

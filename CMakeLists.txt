﻿cmake_minimum_required(VERSION 3.15)
project(dsl4agilent)

option(BUILD_SHARED_LIBS "Build using shared libraries" ON)

find_package(yat4tango CONFIG REQUIRED)
find_package(acqirisdevicedriver CONFIG REQUIRED)

add_compile_definitions(
    PROJECT_NAME=${PROJECT_NAME}
    PROJECT_VERSION=${PROJECT_VERSION}
    _AGILENT_SUPPORT_
)

file(GLOB_RECURSE sources
    src/*.cpp
)

set(includedirs 
    src
    include
)

add_library(dsl4agilent ${sources})
target_include_directories(dsl4agilent PRIVATE ${includedirs})
target_link_libraries(dsl4agilent PRIVATE yat4tango::yat4tango)
target_link_libraries(dsl4agilent PRIVATE acqirisdevicedriver::acqirisdevicedriver)

if (MAJOR_VERSION)
    set_target_properties(dsl4agilent PROPERTIES VERSION ${PROJECT_VERSION} SOVERSION ${MAJOR_VERSION})
endif()

install(DIRECTORY ${CMAKE_SOURCE_DIR}/include/ DESTINATION include
    FILES_MATCHING PATTERN "*"
)

install(TARGETS dsl4agilent LIBRARY DESTINATION ${LIB_INSTALL_DIR})

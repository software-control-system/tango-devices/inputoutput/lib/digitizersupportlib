// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ContinuousAIConfig.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <dsl/DSLConfig.h> 
#include <dsl/ContinuousAIConfig.h>

// ============================================================================
// CONSTS
// ============================================================================
#define TTL_LEVEL             	    2.5
#define DSL_DEFAULT_SAMPLING_RATE		100000.0
#define DSL_DEFAULT_SAMPLING_CLOCK	0
#define DSL_DEFAULT_NUM_WAVEFORMS 	1
#define DSL_DEFAULT_BUFFER_DEPTH 		2048
#define DSL_DEFAULT_TIMEOUT     		1000
#define DSL_DEFAULT_WATER_MARK_UNIT	yat::MessageQ::NUM_OF_MSGS
#define DSL_DEFAULT_LO_WATER_MARK		128
#define DSL_DEFAULT_HI_WATER_MARK		256

#if !defined (__DSL_INLINE__)
# include <dsl/ContinuousAIConfig.i>
#endif // __DSL_INLINE__

namespace dsl {

// ============================================================================
// ContinuousAIConfig::ContinuousAIConfig
// ============================================================================
ContinuousAIConfig::ContinuousAIConfig ()
  : //- no active channels
    channels_(0),
    //- default num of ADC(s) per channel
    adc_per_channel_ (DSL_DEFAULT_NUM_ADC_PER_CHANNEL),
    //- default trigger source
    trigger_source_ (DSL_DEFAULT_TRIGGER_SOURCE),
    //- default trigger level
    trigger_level_ (DSL_DEFAULT_TRIGGER_LEVEL),
    //- default trigger slope
    trigger_slope_ (DSL_DEFAULT_TRIGGER_SLOPE),
    //- default trigger coupling
    trigger_coupling_ (DSL_DEFAULT_TRIGGER_COUPLING),
    //- default trigger delay
    trigger_delay_ (0.0),
    //- disable 10MHz external reference clock
    external_ref_clock_enabled_ (false),
    //- default sampling rate source
    sampling_rate_source_ (dsl::internal_sampling_rate_source),
    //- default sampling rate
    sampling_rate_ (DSL_DEFAULT_SAMPLING_RATE),
    //- default num of wavefoms
    num_waveforms_ (DSL_DEFAULT_NUM_WAVEFORMS),
    //- default num of [[samples per channel] per buffer]  
    buffer_depth_ (DSL_DEFAULT_BUFFER_DEPTH),
    //- default timeout: 1 seconds
    timeout_ (DSL_DEFAULT_TIMEOUT),
    //- default fft mode
    fft_mode_(DSL_DEFAULT_FFT_MODE),
  	//- default daq mode
		daq_mode_(DSL_DEFAULT_DAQ_MODE),
    //- default <treat_timeout_as_error> mode
    treat_timeout_as_error_ (false),
    //- default water marks unit
    msgq_wm_unit_(DSL_DEFAULT_WATER_MARK_UNIT),
    //- default low water mark (msgQ)
		msgq_lo_wm_(DSL_DEFAULT_LO_WATER_MARK),
    //- default high water mark (msgQ)
		msgq_hi_wm_(DSL_DEFAULT_HI_WATER_MARK)
#if defined(_SIMULATION_)
    //- default simulator
    , simulator_ (0)
#endif
{
  //- noop
}

// ============================================================================
// ContinuousAIConfig::ContinuousAIConfig
// ============================================================================
ContinuousAIConfig::ContinuousAIConfig (const ContinuousAIConfig& _src)
{
  *this = _src;
}

// ============================================================================
// ContinuousAIConfig::operator=
// ============================================================================
ContinuousAIConfig& ContinuousAIConfig::operator= (const ContinuousAIConfig& _src)
{
  //- no self assign
  if (this == &_src)
    return *this;

  //- analog input active channels
  this->channels_ = _src.channels_;
  
  //- num of ADCs per channel
  this->adc_per_channel_ = _src.adc_per_channel_;

  //- external digital trigger source
  this->trigger_source_ = _src.trigger_source_;
  
  //- trigger level
  this->trigger_level_ = _src.get_trigger_level();
  
  //- trigger slope
  this->trigger_slope_ = _src.get_trigger_slope();
  
  //- trigger coupling
  this->trigger_coupling_ = _src.get_trigger_coupling();
  
  //- trigger delay
  this->trigger_delay_ = _src.get_trigger_delay();
  
  //- external_ref_clock on/off
  this->external_ref_clock_enabled_ = _src.external_ref_clock_enabled_;
  
  //- sampling rate source
  this->sampling_rate_source_ = _src.sampling_rate_source_;
  
  //- sampling rate
  this->sampling_rate_ = _src.sampling_rate_;
  
  //- num of waveforms to acquire
  this->num_waveforms_ = _src.num_waveforms_;

  //- num of samples per channel per buffer
  this->buffer_depth_ = _src.buffer_depth_;
  
  //- timeout
  this->timeout_ = _src.timeout_;
  
  //- fft mode
  this->fft_mode_ = _src.fft_mode_;
  
  //- daq mode
  this->daq_mode_ = _src.daq_mode_;
  
  //- <treat_timeout_as_error> mode
  this->treat_timeout_as_error_ = _src.treat_timeout_as_error_;  
  
  //- water mark unit (msgQ)
  this->msgq_wm_unit_ = _src.msgq_wm_unit_;
  
  //- low water mark (msgQ)
  this->msgq_lo_wm_ = _src.msgq_lo_wm_;
  
  //- high water mark (msgQ)
  this->msgq_hi_wm_ = _src.msgq_hi_wm_;
  
#if defined(_SIMULATION_)
  //- hw simultator
  this->simulator_ = _src.simulator_;
#endif

  //- return self
  return *this;
}

// ============================================================================
// ContinuousAIConfig::~ContinuousAIConfig
// ============================================================================
ContinuousAIConfig::~ContinuousAIConfig ()
{
  //- noop
}

// ============================================================================
// ContinuousAIConfig::dump
// ============================================================================
void ContinuousAIConfig::dump ()
{
  for (unsigned int i = 0; i < channels_.size(); i++) 
    std::cout << "- AI channel " 
              << i 
              << "............activated" 
              << std::endl;
  
  std::cout << "- ADCs per channel........"
            << this->adc_per_channel_
            << std::endl;

  std::cout << "- trigger source..........";
  switch (trigger_source_)
  {
    case dsl::trigger_internal_src:
      std::cout << "internal" << std::endl;
      break;
    case dsl::trigger_digital_external_src:
      std::cout << "digital external" << std::endl;
      break;
    case trigger_digital_pfi0_src:
      std::cout << "digital pfi-0" << std::endl;
      break;
    case trigger_digital_pfi1_src:
      std::cout << "digital pfi-1" << std::endl;
      break;
    case trigger_digital_pfi2_src:
      std::cout << "digital pfi-2" << std::endl;
      break;
    case trigger_analog_chan0_src:
      std::cout << "analog chan-0" << std::endl;
      break;
    case trigger_analog_chan1_src:
      std::cout << "analog chan-1" << std::endl;
      break;
    case trigger_analog_external_src:
      std::cout << "analog external" << std::endl;
      break;
    default:
      std::cout << "UNKNOWN/INVALID VALUE" << std::endl;
      break;
  }
  
  std::cout << "- trigger level..........." << trigger_level_ << " V" << std::endl;
  
  std::cout << "- trigger slope...........";
  switch (trigger_slope_)
  {
    case dsl::trigger_slope_positive:
      std::cout << "trigger_slope_positive" << std::endl;
      break;
    case dsl::trigger_slope_negative:
      std::cout << "trigger_slope_negative" << std::endl;
      break;
    default:
      std::cout << "UNKNOWN/INVALID VALUE" << std::endl;
      break;
  }
  
  std::cout << "- trigger coupling........";
  switch (trigger_coupling_)
  {
    case dsl::trigger_coupling_dc:
      std::cout << "trigger_coupling_dc" << std::endl;
      break;
    case dsl::trigger_coupling_ac:
      std::cout << "trigger_coupling_ac" << std::endl;
      break;
    case dsl::trigger_coupling_dc_50Ohms:
      std::cout << "trigger_coupling_dc_50Ohms" << std::endl;
      break;
    case dsl::trigger_coupling_ac_50Ohms:
      std::cout << "trigger_coupling_dc_50Ohms" << std::endl;
      break;
    default:
      std::cout << "UNKNOWN/INVALID VALUE" << std::endl;
      break;
  }
  
  std::cout << "- trigger delay..........." << trigger_delay_ << std::endl;
  
  std::cout << "- external ref clock......" 
            << ((! external_ref_clock_enabled_) ? "disabled" : "enabled") 
            << std::endl;
            
  std::cout << "- sampling source.........";
  switch (sampling_rate_source_)
  {
    case dsl::internal_sampling_rate_source:
      std::cout << "internal_sampling_rate_source" << std::endl;
      break;
    case dsl::external_sampling_rate_source:
      std::cout << "external_sampling_rate_source" << std::endl;
      break;
    default:
      std::cout << "UNKNOWN/INVALID VALUE" << std::endl;
      break;
  }
  
  std::cout << "- sampling rate..........." << sampling_rate_ << std::endl;
  
  std::cout << "- num. waveforms.........." << num_waveforms_ << std::endl;

  std::cout << "- buffer depth............" << buffer_depth_ << std::endl;
  
  std::cout << "- timeout................." << timeout_ << std::endl;
  
  std::cout << "- fft mode................";
  switch (fft_mode_)
  {
    case dsl::fft_none:
      std::cout << "none" << std::endl;
      break;
    case dsl::fft_amplitude_spectrum_db:
      std::cout << "amplitude spectrum (dB)" << std::endl;
      break;
    case dsl::fft_phase_spectrum:
      std::cout << "phase spectrum" << std::endl;
      break;
    case dsl::fft_amplitude_spectrum_volts_rms:
      std::cout << "amplitude spectrum (Volts RMS)" << std::endl;
      break;
  }
  
  std::cout << "- daq mode................";
  switch (daq_mode_)
  {
    case dsl::continuous:
      std::cout << "continuous" << std::endl;
      break;
    case dsl::user_pulsed:
      std::cout << "user pulsed" << std::endl;
      break;
  }
  
  std::cout << "- timeout = error........." 
            << (this->treat_timeout_as_error_ ? "true" : "false") 
            << std::endl;
            
  std::cout << "- msgQ-wm-unit............";
  switch (msgq_wm_unit_)
  {
    case yat::MessageQ::NUM_OF_MSGS:
      std::cout << "num of messages" << std::endl;
      break;
    case yat::MessageQ::NUM_OF_BYTES:
      std::cout << "num of bytes" << std::endl;
      break;
  }
  
  std::cout << "- msgQ-lo-wm.............."
  					<< this->msgq_lo_wm_            
            << std::endl;  
            
  std::cout << "- msgQ-hi-wm.............."
  					<< this->msgq_hi_wm_            
            << std::endl;  

#if defined(_SIMULATION_)
  std::cout << "- simulator registred....."
            << (this->simulator_ ? "true" : "false")            
            << std::endl;  
#endif
}

} // namespace dsl



// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    DAQException.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <dsl/DAQException.h>

#if !defined (__DSL_INLINE__)
# include <dsl/DAQException.i>
#endif // __DSL_INLINE__

namespace dsl {

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException ()
 : yat::Exception()
{
  //- noop
}

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const char * _origin)
: yat::Exception((const char *)"unknown", 
                 (const char *)"unknown", 
                 _origin, 
                 DAQException::UNDEFINED, 
                 yat::ERR)
{
  //- noop
}

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const std::string &_origin)
: yat::Exception((const char *)"unknown", 
                 (const char *)"unknown", 
                 _origin.c_str(), 
                 DAQException::UNDEFINED, 
                 yat::ERR)
{
  //- noop
}

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const char * _reason,
					                  const char * _desc,
					                  const char * _origin,
                            int _code, 
                            int _severity)
 : yat::Exception(_reason, _desc, _origin, _code, _severity)
{
  //- noop
}


// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const std::string& _reason,
					                  const std::string& _desc,
					                  const std::string& _origin,
                            int _code, 
                            int _severity)
 : yat::Exception(_reason, _desc, _origin, _code, _severity)
{
  //- noop
}

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const yat::Exception& _src)
  : yat::Exception(_src)
{
  //- noop
}
  
// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const DAQException& _src)
  : yat::Exception(_src)
{
  //- noop
}

// ============================================================================
// DAQException::~DAQException
// ============================================================================
DAQException::~DAQException ()
{
  //- noop
}

// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException & DAQException::operator= (const DAQException& _src) 
{
  yat::Exception::operator=(_src);

  return *this;
}

// ============================================================================
// DAQException::dump
// ============================================================================
void DAQException::dump () const
{
  std::cout << "|------------------------------------------------------" 
  					<< std::endl
  					<< "| dsl::DAQException"
            << std::endl
            << "|------------------------------------------------------" 
            << std::endl; 
  
  for (unsigned int i = 0; i < this->errors.size(); i++) 
  {
    std::cout << "| err[" << i << "].reason....." << this->errors[i].reason.c_str() << std::endl; 
    std::cout << "| err[" << i << "].desc......." << this->errors[i].desc.c_str() << std::endl; 
    std::cout << "| err[" << i << "].origin....." << this->errors[i].origin.c_str() << std::endl; 
    std::cout << "| err[" << i << "].code.......";
    switch (this->errors[i].code)
    {
      case NO_DAQ_TRIGGER:
        std::cout << "NO_DAQ_TRIGGER";
        break;  
      case DAQ_BUFFER_OVERRUN:
        std::cout << "DAQ_BUFFER_OVERRUN";
        break;  
      case PROC_TASK_OVERLOAD:
        std::cout << "PROC_TASK_OVERLOAD";
        break;  
      case PERFORMING_CALIBRATION:
        std::cout << "PERFORMING_CALIBRATION";
        break;  
      case RECOVERING_ERROR:
        std::cout << "RECOVERING_ERROR";
        break;  
      default:
        std::cout << "UNDEFINED";
        break;  
    }
    std::cout << std::endl;
    std::cout << "| err[" << i << "].severity..." << this->errors[i].severity << std::endl; 
    std::cout << "|------------------------------------------------------" << std::endl; 
  }
}

// ============================================================================
// DataLostException::DataLostException
// ============================================================================
DataLostException::DataLostException (int err_code)
: DAQException("lost data", 
               "DAQ aborted due data lost", 
               "DAQ internal process", 
               err_code, 
               yat::ERR)
{
  //- noop
}

// ============================================================================
// DataLostException::~DataLostException
// ============================================================================
DataLostException::~DataLostException ()
{
  //- noop
}

// ============================================================================
// TimeoutException::TimeoutException
// ============================================================================
TimeoutException::TimeoutException (int err_code)
: DAQException("timeout expired", 
               "DAQ timeout expired [no data received]", 
               "DAQ internal process", 
               err_code, 
               yat::ERR)
{
  //- noop
}

// ============================================================================
// TimeoutException::~TimeoutException
// ============================================================================
TimeoutException::~TimeoutException ()
{
  //- noop
}

// ============================================================================
// DeviceBusyException::DeviceBusyException
// ============================================================================
DeviceBusyException::DeviceBusyException (const char *_reason,
					                                const char *_desc,
					                                const char *_origin,
                                          int _code, 
                                          int _severity)
: DAQException(_reason, _desc, _origin, _code, _severity) 
{
  //- noop
}

// ============================================================================
// DeviceBusyException::DeviceBusyException
// ============================================================================
DeviceBusyException::DeviceBusyException (const std::string& _reason,
					                                const std::string& _desc,
					                                const std::string& _origin,
                                          int _code, 
                                          int _severity)
: DAQException(_reason, _desc, _origin, _code, _severity) 
{
  //- noop
}

// ============================================================================
// DeviceBusyException::~DeviceBusyException
// ============================================================================
DeviceBusyException::~DeviceBusyException ()
{
  //- noop
}

} // namespace dsl



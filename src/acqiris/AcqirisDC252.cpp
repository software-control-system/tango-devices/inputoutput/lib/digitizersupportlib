// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    AcqirisDC252.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h> 

#if defined(_ACQIRIS_SUPPORT_) || defined(_AGILENT_SUPPORT_)

#include <iostream>
#include <dsl/Rsrc.h>
#include <dsl/acqiris/AcqirisDC252.h>

namespace dsl { 

// ============================================================================
// STATIC MEMBERS
// ============================================================================
yat::Mutex AcqirisDC252::repository_lock;
//-----------------------------------------------------------------------------
AcqirisDC252::Repository AcqirisDC252::repository;
//-----------------------------------------------------------------------------

// ============================================================================
// AcqirisDC252::instanciate
// ============================================================================
AcqirisDC252 * AcqirisDC252::instanciate (unsigned short _id, bool _calibration, bool _exclusive_access)
  throw (DAQException)
{
  //- lock
  yat::AutoMutex<> guard(AcqirisDC252::repository_lock);

  //- search requested board in the repository
  RepositoryIterator it = AcqirisDC252::repository.find(_id);

  //- already instanciated?
  if (it != AcqirisDC252::repository.end()) 
  {
    //- can't get ownership if _exclusive_access requested
    if (_exclusive_access || it->second->exclusive_access_) 
    {
      //- can't get exclusive access to hardware
      throw DAQException("hardware already in use",
                         "could not get exclusive access to the hardware",
                         "AcqirisDC252::instanciate");
    }
    //- share the board
    else 
    {
      return it->second->duplicate();
    }
  }

  //- instanciate the digitizer
  AcqirisDC252 * d = new AcqirisDC252(_id);
  if (d == 0) 
  {
    //- out of memory
    throw DAQException("out of memory",
                            "out of memory error",
                            "AcqirisDC252::instanciate");
  }

  //- update board's <exclusive_access> flag
  d->exclusive_access_ = _exclusive_access;

  //- update "calibration at init" flag
  d->is_calibration_at_init_ = _calibration;
  
  //- open driver session
  try 
  {
    d->register_hardware();
  }
  catch (const DAQException&)
  {
    //- release digitizer
    d->release();
    throw;
  }
  catch (...)
  {
    //- release digitizer
    d->release();
    //- hardware registration failed
    throw DAQException("hardware registration failed",
                            "unknown exception caught while trying to register the board",
                            "AcqirisDC252::instanciate");
  }

  return d;
}

// ============================================================================
// AcqirisDC252::duplicate
// ============================================================================
AcqirisDC252 * AcqirisDC252::duplicate ()
{
  return dynamic_cast<AcqirisDC252*>(this->SharedObject::duplicate()); 
}
 
// ============================================================================
// AcqirisDC252::AcqirisDC252
// ============================================================================
AcqirisDC252::AcqirisDC252(unsigned short _id) 
  : AcqirisDigitizer(dsl::ACQIRIS_DC252, _id)
{
  //- std::cout << "AcqirisDC252::AcqirisDC252 <-" << std::endl;

  //- store digitizer into the repository
  RepositoryValue pair(_id, this);
  AcqirisDC252::repository.insert(pair);

  //- std::cout << "AcqirisDC252::AcqirisDC252 ->" << std::endl;
}

// ============================================================================
// AcqirisDC252::~AcqirisDC252
// ============================================================================
AcqirisDC252::~AcqirisDC252()
{
  //- lock
  yat::AutoMutex<> guard(AcqirisDC252::repository_lock);
										 
  //- std::cout << "AcqirisDC252::~AcqirisDC252 <-" << std::endl;

  //- release hardware
  this->release_hardware();

  //- remove board from repository
  RepositoryIterator it = AcqirisDC252::repository.find(this->id_);
  if (it != AcqirisDC252::repository.end()) 
  {
    AcqirisDC252::repository.erase(it);    
  }

  //- std::cout << "AcqirisDC252::~AcqirisDC252 <-" << std::endl;
}

// ============================================================================
// AcqirisDC252::clock_frequency
// ============================================================================
double AcqirisDC252::max_sampling_rate () const
{
	return (double)ACQIRIS_DC252_MAX_SAMPLING_RATE;
}

// ============================================================================
// AcqirisDC252::has_10mhz_ref_clock_output_support
// ============================================================================
bool AcqirisDC252::has_10mhz_ref_clock_output_routing_support () const
{
  return false;
}
   
// ============================================================================
// Returns the numbers of channels of the board
// ============================================================================
unsigned short AcqirisDC252::num_channels () const
{
	return static_cast<unsigned short>(ACQIRIS_DC252_NUM_CHANNELS);
}

// ============================================================================
// AcqirisDC252::dump
// ============================================================================
void AcqirisDC252::dump () const
{
  //- std::cout << "Digitizer::manufacturer.......Agilent-Acqiris" << std::endl;
	//- std::cout << "Digitizer::type...............DC-XXX" << std::endl;
	//- std::cout << "Digitizer::board id..........." << this->id_ << std::endl;
	//- std::cout << "Digitizer::hardware handle...." << this->hh_ << std::endl;
	//- std::cout << "Digitizer::exclusive access..." << this->exclusive_access_ << std::endl;
}

} // namespace dsl

#endif //- _ACQIRIS_SUPPORT_

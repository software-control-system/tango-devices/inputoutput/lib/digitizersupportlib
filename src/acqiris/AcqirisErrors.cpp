// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ADLink.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h> 

#if defined(_ACQIRIS_SUPPORT_) || defined(_AGILENT_SUPPORT_)

#include <dsl/acqiris/AcqirisRsrc.h>

// ============================================================================ 
// error code to error text
// =========================================================================== 
#define ACQ_MAX_MESSAGE_BUF_SIZE 256
static char err_txt[ACQ_MAX_MESSAGE_BUF_SIZE] = "no error";

namespace dsl {

// =========================================================================== 
// ERROR HANDLING: error code to error text 
// =========================================================================== 
const char* acqiris_error_text (dsl::HardwareHandle _hh, int _err_code)
{
#if !defined(_SIMULATION_)
  ::AcqrsD1_errorMessage(_hh, _err_code, err_txt);
#endif
  return static_cast<const char*>(err_txt);
}

} // namespace dsl

#endif //- _ACQIRIS_SUPPORT_

// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed AcqirisDigitizer Support Library
//
// = FILENAME
//    AcqirisDigitizer.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h> 

#if defined(_ACQIRIS_SUPPORT_) || defined(_AGILENT_SUPPORT_)

#include <iostream>
#include <yat/threading/Thread.h>
#include <dsl/acqiris/AcqirisDC211.h>
#include <dsl/acqiris/AcqirisDC252.h>

// ============================================================================
// CONSTs
// ============================================================================

#define RSRC_NAME_PREFIX  static_cast<const char *>("PCI::INSTR")
#define EDGE_TRIGGER      0
#define VPOSITIVE         0
#define VNEGATIVE         1
#define TRGDC             0
#define TRGAC             1
#define TRGDC50OHMS       3
#define TRGAC50OHMS       4
#define SAMPLING_CLOCK    1
#define REFERENCE_CLOCK   2
#define DEF_THRESHOLD     1000
#define UNUSED            0

namespace dsl {

// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define SUPPORTED_HARDWARE "current DSL impl. supports the following ACQIRIS boards: DC211, DC252"

// ============================================================================
// AcqirisDigitizer::instanciate
// ============================================================================
Digitizer * AcqirisDigitizer::instanciate (unsigned short _type, 
                                           unsigned short _id,
                                           bool _exclusive_access,
										   bool _calibration)
  throw (DAQException)
{
  Digitizer * d = 0;

  //- type of board
  switch (_type)  
  {
    //- AGILENT-ACQIRIS-211
    case dsl::ACQIRIS_DC211:
      d = AcqirisDC211::instanciate(_id, _calibration, _exclusive_access);
      break;
    //- AGILENT-ACQIRIS-252
    case dsl::ACQIRIS_DC252:
      d = AcqirisDC252::instanciate(_id, _calibration, _exclusive_access);
      break;
    //- unsupported hardware
    default:
      throw DAQException("unsupported or invalid hardware type",
                         SUPPORTED_HARDWARE,
                         "AcqirisDigitizer::instanciate");
      break;
  }

  return d;
}

// ============================================================================
// AcqirisDigitizer::AcqirisDigitizer
// ============================================================================
AcqirisDigitizer::AcqirisDigitizer (unsigned short _type, 
                                    unsigned short _id) 
  : Digitizer(_type, _id), daq_buffer_depth_ (0)
{
  //- std::cout << "AcqirisDigitizer::AcqirisDigitizer <-" << std::endl;
  //- std::cout << "AcqirisDigitizer::AcqirisDigitizer ->" << std::endl;
}

// ============================================================================
// AcqirisDigitizer::~AcqirisDigitizer
// ============================================================================
AcqirisDigitizer::~AcqirisDigitizer ()
{
  //- std::cout << "AcqirisDigitizer::~AcqirisDigitizer <-" << std::endl;
  //- std::cout << "AcqirisDigitizer::~AcqirisDigitizer ->" << std::endl;
}

// ============================================================================
// AcqirisDigitizer::register_hardware
// ============================================================================
int AcqirisDigitizer::register_hardware ()
{
  //- std::cout << "AcqirisDigitizer::register_hardware () entering - calibration required? " << is_calibration_at_init_ << std::endl;
#if !defined(_SIMULATION_) 

  //- build resource name
  std::string rname(RSRC_NAME_PREFIX);

  //- add <this->id_> to the string (should use std C++ streams)
  char id_str[3] = {0,0,0};
  ::sprintf(id_str, "%d", this->id_);

  //- concat
  rname += std::string(id_str);

  //- std::cout << "AcqirisDigitizer::register_hardware::rsrc name::" << rname << std::endl;
 
  int err = 0;
  // check if calibration is required at init or not...
  if (is_calibration_at_init_)
  {
    //- initialize with default options (including hardware calibration)
    //- blocking action (about 15s)
    err = ::AcqrsD1_init(const_cast<char*>(rname.c_str()), false, false, &this->hh_);
  }
  else
  {
    //- initialize with option: NO CALIBRATION
	std::string no_calib = "CAL=False";
    err = ::AcqrsD1_InitWithOptions(const_cast<char*>(rname.c_str()), false, false, const_cast<char*>(no_calib.c_str()), &this->hh_);
  }

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    throw DAQException("initialization failed [AcqrsD1_Init failed]",
                       dsl::acqiris_error_text(this->hh_, err),
						           "AcqirisDigitizer::register_hardware");
  }
#else // _SIMULATION_
  std::cout << "register_hardware SIMULATED" << std::endl;
  if (is_calibration_at_init_)
  {
	std::cout << "calibrating..." << std::endl;
    Sleep(15000);
  }
#endif // _SIMULATION_  

  return 0;
}

// ============================================================================
// AcqirisDigitizer::release_hardware
// ============================================================================
int AcqirisDigitizer::release_hardware ()
{
	//- be sure the digitizer is registered
	if (this->hh_ == kUNDEFINED_HH) 
  {
	  return 0;
	}

#if !defined(_SIMULATION_) 

	//- close...
  int err = ::AcqrsD1_closeAll();

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    throw DAQException("closing failed [AcqrsD1_closeAll failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::release_hardware");
  }
	
#else // _SIMULATION_
	
	return 0;
	
#endif // _SIMULATION_ 

  return 0;
} 

// ============================================================================
// AcqirisDigitizer::configure_continuous_ai
// ============================================================================
void AcqirisDigitizer::configure_continuous_ai (const dsl::ContinuousAIConfig& _ai_config)
    throw (DAQException)
{
  //- store config locally
  this->ai_config_ = _ai_config;

  this->ai_config_.dump();

#if ! defined(_SIMULATION_) 

  //- check registration id (may throw an exception)
  this->check_registration();

  //- channels configuration -------------------------------------------------
  //--------------------------------------------------------------------------

  //- get active channels
  const ActiveAIChannels chans = this->ai_config_.get_active_channels();
  
  //- setup channels mask
  ViInt32 used_channels = 0x01;
  switch (chans.size())
  {
    //- Acqiris board with 1 channel
    case ACQIRIS_1_CHANNEL:
      used_channels = 0x01;
      break;
    //- Acqiris board with 2 channels
    case ACQIRIS_2_CHANNELS:
      used_channels = 0x03;
      break;
    //- Acqiris board with 4 channels
    case ACQIRIS_4_CHANNELS:
      used_channels = 0x0f;
      break;
    default:
      break;
  }

  //- configure channel(s) combination
  DigitizerErr err = ::AcqrsD1_configChannelCombination(this->hh_,
                                                        this->ai_config_.get_num_adc_per_channel(),
                                                        used_channels);
  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    throw DAQException("channel configuration failed  [AcqrsD1_configChannelCombination failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::configure_continuous_ai");
  }

  //- configure each active channel
  for (size_t i = 0; i < chans.size(); i++)
  {
    //- impedance
    ViInt32 impedance = (chans[i].impedance == impedance_50Ohms) 
		                  ? ACQIRIS_VAL_50_OHMS 
		                  : ACQIRIS_VAL_1_MEG_OHM;

    ViInt32 coupling = ACQIRIS_DC_50_OHMS;
    switch (impedance)
    {
      case ACQIRIS_VAL_1_MEG_OHM:
        coupling = (chans[i].coupling == dc_coupling)
				         ? ACQIRIS_DC_1_MEG_OHM
				         : ACQIRIS_AC_1_MEG_OHM;
        break;
      case ACQIRIS_VAL_50_OHMS:
		    coupling = (chans[i].coupling == dc_coupling)
			           ? ACQIRIS_DC_50_OHMS
			           : ACQIRIS_AC_50_OHMS;
        break;
      default:
        break;
    }

    
	  //- bandwidth
    ViInt32 bandwidth = ACQIRIS_VAL_FULL_BANDWIDTH;
	  switch (chans[i].bandwidth)
	  {
		  case bandwidth_20MHz:
		    bandwidth = ACQIRIS_VAL_20MHZ_BANDWIDTH;
		    break;
		  case bandwidth_25MHz:
		    bandwidth = ACQIRIS_VAL_25MHZ_BANDWIDTH;
		    break;
		  case bandwidth_35MHz:
		    bandwidth = ACQIRIS_VAL_35MHZ_BANDWIDTH;
		    break;
		  case bandwidth_200MHz:
		    bandwidth = ACQIRIS_VAL_200MHZ_BANDWIDTH;
		    break;
		  case bandwidth_700MHz:
		    bandwidth = ACQIRIS_VAL_700MHZ_BANDWIDTH;
		    break;
		  default:
		    break;
	  }

    err = ::AcqrsD1_configVertical(this->hh_,
												           chans[i].id + 1,
												           chans[i].range,
												           chans[i].offset, 
                                   coupling,
                                   bandwidth);

    if (err)
      std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

    if (err < 0)
    { 
      throw DAQException("channel configuration failed [AcqrsD1_configVertical failed]",
                         dsl::acqiris_error_text (this->hh_, err),
						             "AcqirisDigitizer::configure_continuous_ai");
    }
  } //- for each active channel 
  
  //- time scale configuration -----------------------------------------------
  //--------------------------------------------------------------------------
  if (this->ai_config_.get_sampling_rate_source() != dsl::external_sampling_rate_source)
  {
    ViReal64 sampling_interval = 1. / this->ai_config_.get_sampling_rate();

    err = ::AcqrsD1_configHorizontal(this->hh_,
                                     sampling_interval,
                                     this->ai_config_.get_trigger_delay());

    if (err)
      std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

    if (err < 0)
    { 
      throw DAQException("sampling interval configuration failed [AcqrsD1_configHorizontal failed]",
                         dsl::acqiris_error_text (this->hh_, err),
						             "AcqirisDigitizer::configure_continuous_ai");
    }
  }

  /*
  ViReal64 actual_sampling_interval = 0.;
  ViReal64 actual_delay = 0.;
  err = AcqrsD1_getHorizontal(this->hh_,
                              &actual_sampling_interval,
                              &actual_delay);
  if (err >= 0)
  {
    std::cout << "AcqirisDigitizer::configure_continuous_ai::actual_sampling_interval: " 
              << actual_sampling_interval 
              << " Hz"
              << std::endl;

    std::cout << "AcqirisDigitizer::configure_continuous_ai::actual_delay: " 
              << actual_delay 
              << " s"
              << std::endl;
  }
  */

  //- memory configuration ---------------------------------------------------
  //--------------------------------------------------------------------------
  ViInt32 num_samples = this->ai_config_.num_active_channels() 
                      * this->ai_config_.get_buffer_depth();

  err = ::AcqrsD1_configMemory(this->hh_, 
                               num_samples, 
                               this->ai_config_.get_num_waveforms());

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    throw DAQException("memory configuration failed [AcqrsD1_configMemory failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::configure_continuous_ai");
  }

  /*
  ViInt32 best_num_samples = 0;
  err = AcqrsD1_bestNominalSamples(this->hh_, &best_num_samples);
  if (err >= 0)
  {
    std::cout << "AcqirisDigitizer::configure_continuous_ai::best_num_samples: " 
              << best_num_samples 
              << " samples"
              << std::endl;

    std::cout << "AcqirisDigitizer::configure_continuous_ai::computed num samples: " 
              << num_samples 
              << " samples"
              << std::endl;
  }
  */

  //- trigger configuration --------------------------------------------------
  //--------------------------------------------------------------------------
  ViInt32 trig_ptrn;
  ViInt32 trig_source;
  ViInt32 trig_class = EDGE_TRIGGER;
  switch(this->ai_config_.get_trigger_source())
  {
    case trigger_internal_src:
      trig_source = 1;
      trig_ptrn = 0x00000001;
      break;
    case trigger_digital_external_src:
      trig_source = -1;
      trig_ptrn = 0x80000000;
      break;
    case trigger_analog_chan0_src:
      trig_source = 1;
      trig_ptrn = 0x00000001;
      break;
    case trigger_analog_chan1_src:
      trig_source = 2;
      trig_ptrn = 0x00000002;
      break;
    default:
     throw DAQException("invalid trigger configuration",
                        "there is currently no support for the specified trigger source",
						            "AcqirisDigitizer::configure_continuous_ai");
     break;
  }

  ViInt32 trig_coupling;
  switch(this->ai_config_.get_trigger_coupling())
  {
    case trigger_coupling_ac:
      trig_coupling = TRGAC;
      break;
    case trigger_coupling_dc:
      trig_coupling = TRGDC;
      break;
    case trigger_coupling_dc_50Ohms:
      trig_coupling = TRGDC50OHMS;
      break;
    case trigger_coupling_ac_50Ohms:
      trig_coupling = TRGAC50OHMS;
      break;
    default:
     throw DAQException("invalid trigger configuration",
                        "there is currently no support for the specified trigger coupling",
						            "AcqirisDigitizer::configure_continuous_ai");
     break;
  }

  ViInt32 trig_slope;
  switch(this->ai_config_.get_trigger_slope())
  {
    case trigger_slope_negative:
      trig_slope = VNEGATIVE;
      break;
    case trigger_slope_positive:
      trig_slope = VPOSITIVE;
      break;
    default:
     throw DAQException("invalid trigger configuration",
                        "there is currently no support for the specified trigger slope",
						            "AcqirisDigitizer::configure_continuous_ai");
     break;
  }

  err = ::AcqrsD1_configTrigClass(this->hh_,
                                  trig_class,
                                  trig_ptrn,
                                  UNUSED,
                                  UNUSED,
                                  UNUSED,
                                  UNUSED);

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    throw DAQException("trigger configuration failed [AcqrsD1_configTrigClass failed]",
                       dsl::acqiris_error_text (this->hh_, err),
					             "AcqirisDigitizer::configure_continuous_ai");
  }

  err = ::AcqrsD1_configTrigSource(this->hh_,
                                   trig_source,
                                   trig_coupling,
                                   trig_slope,
                                   1.e3 * this->ai_config_.get_trigger_level(),
                                   UNUSED);

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  {
    throw DAQException("trigger configuration failed [AcqrsD1_configTrigSource failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::configure_continuous_ai");
  }

  //- 10MHz external reference clock configuration ---------------------------
  //--------------------------------------------------------------------------
  if (this->ai_config_.external_reference_clock_enabled())
  {  
    err = ::AcqrsD1_configExtClock(this->hh_,
                                   REFERENCE_CLOCK,
                                   DEF_THRESHOLD,
                                   UNUSED,
                                   UNUSED,
                                   UNUSED);

    if (err)
      std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

    if (err < 0)
    { 
      throw DAQException("10MHz external reference clock configuration failed [AcqrsD1_configExtClock failed]",
                         dsl::acqiris_error_text (this->hh_, err),
						             "AcqirisDigitizer::configure_continuous_ai");
    }
  }

  //- external sampling clock configuration ----------------------------------
  //--------------------------------------------------------------------------

  if (this->ai_config_.get_sampling_rate_source() == dsl::external_sampling_rate_source)
  {
    ViReal64 sampling_interval = 1. / this->ai_config_.get_sampling_rate();

    err = ::AcqrsD1_configExtClock(this->hh_,
                                   SAMPLING_CLOCK,
                                   DEF_THRESHOLD,
                                   num_samples,
                                   sampling_interval,
                                   sampling_interval);

    if (err)
      std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

    if (err < 0)
    { 
      throw DAQException("external sampling clock configuration failed [AcqrsD1_configExtClock failed]",
                         dsl::acqiris_error_text (this->hh_, err),
						             "AcqirisDigitizer::configure_continuous_ai");
    }
  }

  //- 10MHz reference clock output [I/O-B] -----------------------------------
  //--------------------------------------------------------------------------
  if (this->has_10mhz_ref_clock_output_routing_support())
	{
    ViInt32   connector  = 2;    // I/O-B Connector
    ViInt32   signal     = 19;   // 10 MHz reference clock
    ViInt32   qualifier1 = 0;
    ViReal64  qualifier2 = 0.;
    
    err = ::AcqrsD1_configControlIO(this->hh_,
                                    connector,
                                    signal,
                                    qualifier1,
                                    qualifier2);

    if (err)
      std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

    if (err < 0)
    { 
      throw DAQException("10MHz reference clock configuration failed [AcqrsD1_configControlIO failed]",
                         dsl::acqiris_error_text (this->hh_, err),
                         "AcqirisDigitizer::configure_continuous_ai");
    }
  }

#endif // _SIMULATION_ 
}


// ============================================================================
// AcqirisDigitizer::start_continuous_ai
// ============================================================================
void AcqirisDigitizer::start_continuous_ai ()
    throw (DAQException)
{
  //- std::cout << "AcqirisDigitizer::start_continuous_ai <-" << std::endl;

#if !defined(_SIMULATION_) 

  //- check registration id (may throw an exception)
  this->check_registration();

#endif // _SIMULATION_

  this->daq_buffer_depth_ = this->ai_config_.get_num_waveforms() 
                          * this->ai_config_.num_active_channels() 
                          * this->ai_config_.get_buffer_depth();

 //- std::cout << "AcqirisDigitizer::start_continuous_ai ->" << std::endl;
}

// ============================================================================
// AcqirisDigitizer::stop_continuous_ai
// ============================================================================
void AcqirisDigitizer::stop_continuous_ai ()
    throw (DAQException)
{
  //- std::cout << "AcqirisDigitizer::stop_continuous_ai <-" << std::endl;
  
#if ! defined(_SIMULATION_)

  //- check registration id (may throw an exception)
  this->check_registration();

  int err = ::AcqrsD1_stopAcquisition(this->hh_);

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    throw DAQException("stop acquisition failed [AcqrsD1_stopAcquisition failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::stop_continuous_ai");
  }


#endif // _SIMULATION_

  //- std::cout << "AcqirisDigitizer::stop_continuous_ai ->" << std::endl;
}

// ============================================================================
// AcqirisDigitizer::get_data
// ============================================================================
dsl::AIScaledData * AcqirisDigitizer::get_data (unsigned long timeout_ms) 
    throw (DAQException)
{
  //- std::cout << "AcqirisDigitizer::get_data <-" << std::endl;
  
  ViInt32 num_samples_per_segment = this->ai_config_.num_active_channels() 
                                  * this->ai_config_.get_buffer_depth();

  ViInt32 total_num_samples = num_samples_per_segment 
                            * this->ai_config_.get_num_waveforms();

  //- allocate the destination buffer
  dsl::AIScaledData * d = DSL_NEW(dsl::AIScaledData(total_num_samples));
  if (d == 0)
  {
    throw DAQException("out of memory",
                       "memory allocation failed",
                       "AcqirisDigitizer::get_data");
  }
  d->force_length(total_num_samples);

#if ! defined(_SIMULATION_)

  int err = AcqrsD1_acquire(this->hh_); // Start the acquisition

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    delete d;
    throw DAQException("data acquisition failed [AcqrsD1_acquire failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::get_data");
  }

  err = AcqrsD1_waitForEndOfAcquisition(this->hh_, static_cast<ViInt32>(timeout_ms));

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    if (err == ACQIRIS_ERROR_ACQ_TIMEOUT)
    {
      delete d;
      ::AcqrsD1_stopAcquisition(this->hh_);
      throw dsl::TimeoutException(dsl::TimeoutException::NO_DAQ_TRIGGER);
    }
    else
    {
      delete d;
      throw DAQException("data acquisition failed [AcqrsD1_waitForEndOfAcquisition failed]",
                         dsl::acqiris_error_text (this->hh_, err),
						             "AcqirisDigitizer::get_data");
    }
  }

	AqDataDescriptor data_desc;
  yat::Buffer<AqSegmentDescriptor> seg_desc(this->ai_config_.get_num_waveforms());
  seg_desc.force_length(seg_desc.capacity());

  ViInt32 read_mode = 0;
  ViInt32 segment_offset = 0;
  if (this->ai_config_.get_num_waveforms() > 1)
  {
    read_mode = 1;
    segment_offset = num_samples_per_segment;
  }

	AqReadParameters read_params;
  ::memset(&read_params, 0, sizeof(AqReadParameters));
	read_params.dataType = 3; // 3 = 64 bits doubles
	read_params.readMode = read_mode;
	read_params.firstSegment = 0;
	read_params.nbrSegments = this->ai_config_.get_num_waveforms();
	read_params.firstSampleInSeg = 0;
	read_params.segmentOffset = segment_offset; // unused parameter
	read_params.nbrSamplesInSeg = num_samples_per_segment;
	read_params.dataArraySize = sizeof(double) * d->capacity();
	read_params.segDescArraySize = sizeof(AqSegmentDescriptor) * read_params.nbrSegments;

  const ActiveAIChannels chans = this->ai_config_.get_active_channels();

  //-----------------------------------------
  //-TODO: add support for multichannel board
  //-----------------------------------------  
  err = AcqrsD1_readData(this->hh_,
                         chans[0].id + 1, //- mod required:: chan ids start from 1
                         &read_params,
                         d->base(),
                         &data_desc,
                         seg_desc.base());

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    delete d;
    throw DAQException("could not read channels [AcqrsD1_readData failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::get_data");
  }

  /*
  std::cout << "AqDataDescriptor::returnedSamplesPerSeg: " << data_desc.returnedSamplesPerSeg << std::endl;
  std::cout << "AqDataDescriptor::indexFirstPoint: " << data_desc.indexFirstPoint << std::endl;
  std::cout << "AqDataDescriptor::sampTime: " << data_desc.sampTime << std::endl;
  std::cout << "AqDataDescriptor::vGain: " << data_desc.vGain << std::endl;
  std::cout << "AqDataDescriptor::vOffset: " << data_desc.vOffset << std::endl;
  std::cout << "AqDataDescriptor::returnedSegments: " << data_desc.returnedSegments << std::endl;
  std::cout << "AqDataDescriptor::nbrAvgWforms: " << data_desc.nbrAvgWforms << std::endl;
  std::cout << "AqDataDescriptor::actualTriggersInAcqLo: " << data_desc.actualTriggersInAcqLo << std::endl;
  std::cout << "AqDataDescriptor::actualTriggersInAcqHi: " << data_desc.actualTriggersInAcqHi << std::endl;
  std::cout << "AqDataDescriptor::actualDataSize: " << data_desc.actualDataSize << std::endl;

  for (size_t i = 0; i < seg_desc.length(); i ++)
  {
    AqSegmentDescriptor & sd = seg_desc[i];
    std::cout << "AqDataDescriptor::horPos: " << sd.horPos << std::endl;
    std::cout << "AqDataDescriptor::timeStampLo: " << sd.timeStampLo << std::endl;
    std::cout << "AqDataDescriptor::timeStampHi: " << sd.timeStampHi << std::endl;
  }
  */

#endif //- _SIMULATION_

  //- std::cout << "AcqirisDigitizer::get_data ->" << std::endl;
  
  return d;
}

// ============================================================================
// AcqirisDigitizer::get_waveforms
// ============================================================================
dsl::Waveforms * AcqirisDigitizer::get_waveforms (size_t requested_num_wfms, unsigned long timeout_ms) 
    throw (DAQException)
{
  //- std::cout << "AcqirisDigitizer::get_waveforms <-" << std::endl;
  
  dsl::Waveforms * wfms = 0;
  
  size_t num_wfms = (requested_num_wfms > ACQIRIS_MAX_NUM_WAVEFORMS)
                  ? ACQIRIS_MAX_NUM_WAVEFORMS
                  : requested_num_wfms;

#if ! defined(_SIMULATION_)

  int err = AcqrsD1_acquire(this->hh_); 

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    throw DAQException("data acquisition failed [AcqrsD1_acquire failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::get_data");
  }

  err = AcqrsD1_waitForEndOfAcquisition(this->hh_, static_cast<ViInt32>(timeout_ms));

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    if (err == ACQIRIS_ERROR_ACQ_TIMEOUT)
    {
      ::AcqrsD1_stopAcquisition(this->hh_);
      throw dsl::TimeoutException(dsl::TimeoutException::NO_DAQ_TRIGGER);
    }
    else
    {
      throw DAQException("data acquisition failed [AcqrsD1_waitForEndOfAcquisition failed]",
                         dsl::acqiris_error_text (this->hh_, err),
						             "AcqirisDigitizer::get_data");
    }
  }

  ViInt32 num_samples_per_segment = this->ai_config_.num_active_channels() 
                                  * this->ai_config_.get_buffer_depth();

  ViInt32 total_num_samples = num_samples_per_segment * num_wfms;

	AqDataDescriptor data_desc;
  yat::Buffer<AqSegmentDescriptor> seg_desc(this->ai_config_.get_num_waveforms());
  seg_desc.force_length(seg_desc.capacity());

  ViInt32 read_mode = 0;
  ViInt32 segment_offset = 0;
  if (num_wfms > 1)
  {
    read_mode = 1;
    segment_offset = num_samples_per_segment;
  }

	AqReadParameters read_params;
  ::memset(&read_params, 0, sizeof(AqReadParameters));
	read_params.dataType = 3; // '3' means 64 bits doubles
	read_params.readMode = read_mode;
	read_params.firstSegment = 0;
	read_params.nbrSegments = num_wfms;
	read_params.firstSampleInSeg = 0;
	read_params.segmentOffset = segment_offset; // unused parameter
	read_params.nbrSamplesInSeg = num_samples_per_segment;
	read_params.dataArraySize = sizeof(double) * total_num_samples;
	read_params.segDescArraySize = sizeof(AqSegmentDescriptor) * read_params.nbrSegments;

  dsl::AIScaledData d(total_num_samples);
  d.force_length(total_num_samples);

  const ActiveAIChannels chans = this->ai_config_.get_active_channels();

  //-----------------------------------------
  //-TODO: add support for multichannel board
  //-----------------------------------------  
  err = AcqrsD1_readData(this->hh_,
                         chans[0].id + 1, 
                         &read_params,
                         d.base(),
                         &data_desc,
                         seg_desc.base());

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    throw DAQException("could not read channels [AcqrsD1_readData failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::get_data");
  }

  /*
  std::cout << "AqDataDescriptor::returnedSamplesPerSeg: " << data_desc.returnedSamplesPerSeg << std::endl;
  std::cout << "AqDataDescriptor::indexFirstPoint: " << data_desc.indexFirstPoint << std::endl;
  std::cout << "AqDataDescriptor::sampTime: " << data_desc.sampTime << std::endl;
  std::cout << "AqDataDescriptor::vGain: " << data_desc.vGain << std::endl;
  std::cout << "AqDataDescriptor::vOffset: " << data_desc.vOffset << std::endl;
  std::cout << "AqDataDescriptor::returnedSegments: " << data_desc.returnedSegments << std::endl;
  std::cout << "AqDataDescriptor::nbrAvgWforms: " << data_desc.nbrAvgWforms << std::endl;
  std::cout << "AqDataDescriptor::actualTriggersInAcqLo: " << data_desc.actualTriggersInAcqLo << std::endl;
  std::cout << "AqDataDescriptor::actualTriggersInAcqHi: " << data_desc.actualTriggersInAcqHi << std::endl;
  std::cout << "AqDataDescriptor::actualDataSize: " << data_desc.actualDataSize << std::endl;

  for (size_t i = 0; i < seg_desc.length(); i ++)
  {
     AqSegmentDescriptor & sd = seg_desc[i];
     std::cout << "AqDataDescriptor::horPos: " << sd.horPos << std::endl;
     std::cout << "AqDataDescriptor::timeStampLo: " << sd.timeStampLo << std::endl;
     std::cout << "AqDataDescriptor::timeStampHi: " << sd.timeStampHi << std::endl;
  }
  */

  wfms = new dsl::Waveforms;
  if (wfms == 0)
  { 
    throw DAQException("OUT_OF_MEMORY",
                       "memory allocation failed",
						           "AcqirisDigitizer::get_waveforms");
  }

  for (ViInt32 s = 0; s < data_desc.returnedSegments; s++)
  {
    dsl::Waveform * wfm = new dsl::Waveform;
    if (wfm == 0)
    { 
      delete wfms;
      throw DAQException("OUT_OF_MEMORY",
                         "memory allocation failed",
						             "AcqirisDigitizer::get_waveforms");
    }

    wfm->ord = new dsl::AIScaledData(data_desc.returnedSamplesPerSeg);
    wfm->ord->force_length(data_desc.returnedSamplesPerSeg);
    *(wfm->ord) = d.base() + data_desc.indexFirstPoint + (s * data_desc.returnedSamplesPerSeg);
    
    wfm->x_increment = 1. / this->ai_config_.get_sampling_rate();
    
    wfm->abs = new dsl::AIScaledData(wfm->ord->length());
    wfm->abs->force_length(data_desc.returnedSamplesPerSeg);
    for (size_t i = 0; i < wfm->abs->length(); i++)
    	*(wfm->abs->base() + i) = static_cast<dsl::AIDataType>(i) * wfm->x_increment;

    wfms->push_back(wfm);
  }

#else
  
  if (! this->ai_config_.simulator())
  {
  	yat::Thread::sleep(1);
    return new dsl::Waveforms;
  }
  
  yat::Timer t;
    
  wfms = this->ai_config_.simulator()->get_waveforms(num_wfms, timeout_ms);
  
  if (t.elapsed_msec() >= timeout_ms)
  { 
    delete wfms;
    throw DAQException("TIMEOUT_EXPIRED",
                       "simulator::get_waveforms took more than the <specified timeout> to complete",
						           "AcqirisDigitizer::get_waveforms");
  } 
  
#endif //- _SIMULATION_

  //- std::cout << "AcqirisDigitizer::get_waveforms ->" << std::endl;
  
  return wfms;
}

// ============================================================================
// AcqirisDigitizer::auto_calibrate
// ============================================================================
void AcqirisDigitizer::auto_calibrate ()
  throw (DAQException)
{
  //- std::cout << "AcqirisDigitizer::auto_calibrate <-" << std::endl;

#if ! defined(_SIMULATION_)

	int err = AcqrsD1_calibrate(this->hh_);

  if (err)
    std::cout << dsl::acqiris_error_text(this->hh_, err) << std::endl;

  if (err < 0)
  { 
    throw DAQException("auto calibration failed [AcqrsD1_calibrate failed]",
                       dsl::acqiris_error_text (this->hh_, err),
						           "AcqirisDigitizer::auto_calibrate");
  }
  
#else

	yat::Thread::sleep(2500);

#endif

  //- std::cout << "AcqirisDigitizer::auto_calibrate ->" << std::endl;
}

} // namespace dsl

#endif // _ACQIRIS_SUPPORT_



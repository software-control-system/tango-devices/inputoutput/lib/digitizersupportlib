// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Waveform.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <dsl/Waveform.h>

#if !defined (__DSL_INLINE__)
# include <dsl/Waveform.i>
#endif // __DSL_INLINE__

// ============================================================================
// CONSTs
// ============================================================================
#define kNUM_OF_DBL_MEMBERS 5

namespace dsl {

// ============================================================================
// Waveform::Waveform
// ============================================================================
Waveform::Waveform ()
 : abs(0), ord(0), relative_ts(0.), absolute_ts(0.), x_increment(0.), gain(1.), offset(0.)
{
  //- std::cout << "Waveform::Waveform <-" << std::endl;
  //- std::cout << "Waveform::Waveform ->" << std::endl;
}

// ============================================================================
// Waveform::~Waveform
// ============================================================================
Waveform::~Waveform ()
{ 
	//- std::cout << "Waveform::~Waveform <-" << std::endl;
  
  //- release memory
  if (this->abs)
  {
    delete this->abs;
    this->abs = 0;
  }
  
  if (this->ord)
  {
    delete this->ord;
    this->ord = 0;
  }
  
  //- std::cout << "Waveform::~Waveform ->" << std::endl;
}

// ============================================================================
// Waveforms::Waveforms
// ============================================================================
Waveforms::Waveforms () : v(0)
{

}

// ============================================================================
// Waveforms::~Waveforms
// ============================================================================
Waveforms::~Waveforms ()
{
  for (unsigned long i = 0; i < v.size(); i++)
  {
    if (this->v[i]) 
    { 
      delete this->v[i];
      this->v[i] = 0; 
    }
  }
  this->v.clear();
}


// ============================================================================
// Waveforms::size
// ============================================================================
size_t Waveforms::size () const
{
  size_t ds, total_size = 0;
  
  for (unsigned long i = 0; i < v.size(); i++)
  {
    if (this->v[i]) 
    { 
      ds  = (this->v[i]->abs) ? this->v[i]->abs->size() : 0;
      ds += (this->v[i]->ord) ? this->v[i]->ord->size() : 0;
      total_size += kNUM_OF_DBL_MEMBERS * sizeof(double) + ds;
    }
  }
  
  return total_size;
}

} // namespace dsl



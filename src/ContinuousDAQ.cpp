// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ContinuousDAQ.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <dsl/DSLConfig.h> 
#include <dsl/ContinuousDAQ.h>

#if !defined (__DSL_INLINE__)
# include <dsl/ContinuousDAQ.i>
#endif // __DSL_INLINE__

namespace dsl {

// ============================================================================
// ContinuousDAQ::ContinuousDAQ
// ============================================================================
ContinuousDAQ::ContinuousDAQ ()
 : exception_status_(HANDLING_NONE),
	 state_ (ContinuousDAQ::UNKNOWN)
{
  //- std::cout << "ContinuousDAQ::ContinuousDAQ <-" << std::endl;
  //- std::cout << "ContinuousDAQ::ContinuousDAQ ->" << std::endl;
}

// ============================================================================
// ContinuousDAQ::~ContinuousDAQ
// ============================================================================
ContinuousDAQ::~ContinuousDAQ ()
{
  //- std::cout << "ContinuousDAQ::~ContinuousDAQ <-" << std::endl;
  //- std::cout << "ContinuousDAQ::~ContinuousDAQ ->" << std::endl;
} 

// ============================================================================
// ContinuousDAQ::calibrate_hardware
// ============================================================================
void ContinuousDAQ::calibrate_hardware ()
  throw (DAQException, DeviceBusyException)
{
  //- noop
}

// ============================================================================
// ContinuousDAQ::calibration_completed
// ============================================================================
bool ContinuousDAQ::calibration_completed ()
{
  return this->exception_status_ != ContinuousDAQ::HANDLING_CALIBRATION;
}

// ============================================================================
// ContinuousDAQ::latch_error
// ============================================================================
void ContinuousDAQ::latch_error (const DAQException& e, bool switch_to_fault)
{
  if (this->state_ == ContinuousDAQ::FAULT)
    return;

  if (switch_to_fault)
    this->state_ = ContinuousDAQ::FAULT;

  this->daq_exception_ = e;
}

// ============================================================================
// ContinuousDAQ::reset_error
// ============================================================================
void ContinuousDAQ::reset_error (DaqState s)
{
  this->state_ = s;
  this->daq_exception_ = DAQException();
}

// ============================================================================
// ContinuousDAQ::has_error
// ============================================================================
bool ContinuousDAQ::has_error () const
{
  return ! this->daq_exception_.errors.empty();
}

// ============================================================================
// ContinuousDAQ::error
// ============================================================================
const DAQException & ContinuousDAQ::error () const
{
  return this->daq_exception_;
}

} // namespace dsl



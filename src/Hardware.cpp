// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Hardware.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/Hardware.h>

#if !defined (__DSL_INLINE__)
# include <dsl/Hardware.i>
#endif // __DSL_INLINE__

namespace dsl {

// ============================================================================
// Hardware::Hardware
// ============================================================================
Hardware::Hardware (unsigned short _type, unsigned short _id) 
	: yat::SharedObject(),
		type_ (_type), 
		id_ (_id),
		exclusive_access_ (true)
{
  //- std::cout << "Hardware::Hardware <-" << std::endl;
  //- std::cout << "Hardware::Hardware ->" << std::endl;
}

// ============================================================================
// Hardware::~Hardware
// ============================================================================
Hardware::~Hardware ()
{
  //- std::cout << "Hardware::~Hardware <-" << std::endl;
  //- std::cout << "Hardware::~Hardware ->" << std::endl;
}

// ============================================================================
// Hardware::dump
// ============================================================================
void Hardware::dump () const 
{
  //- noop
}

} // namespace dsl








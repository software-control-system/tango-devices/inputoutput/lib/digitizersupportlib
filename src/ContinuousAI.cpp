// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ContinuousAI.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include <iostream>
#include <yat/time/Timer.h> 
#include <dsl/DSLConfig.h> 
#include <dsl/AIData.h> 
#include <dsl/DSLTaskMessage.h>
#include <dsl/ContinuousAI.h>
#include <dsl/DAQException.h>

#if !defined (__DSL_INLINE__)
# include <dsl/ContinuousAI.i>
#endif // __DSL_INLINE__
  
//-----------------------------------------------------------------------------
// POST MSG SHORTCUT
//-----------------------------------------------------------------------------
#define POST_MSG(MSG_TYPE, MSG_PRIO, MSG_TMO) \
do \
{ \
  try { \
    bool have_to_wait = MSG_TMO ? true : false; \
    yat::Message * m = DSL_NEW(yat::Message(MSG_TYPE, MSG_PRIO, have_to_wait)); \
    if (have_to_wait) \
      this->wait_msg_handled(m, MSG_TMO); \
    else \
      this->post(m); \
  } \
  catch (const dsl::DAQException &) { \
  	throw; \
  } \
  catch (const yat::Exception & e) { \
  	throw DAQException(e); \
  } \
  catch (...) { \
  	throw DAQException(); \
  } \
} \
while (0)

//-----------------------------------------------------------------------------
// POST MSG SHORTCUT
//-----------------------------------------------------------------------------
#define POST_MSG_WITH_DATA(MSG_TYPE, MSG_PRIO, MSG_DATA, MSG_DATA_SIZE, MSG_TMO) \
do \
{ \
  try { \
    bool have_to_wait = MSG_TMO ? true : false; \
    yat::Message * m = DSL_NEW(yat::Message(MSG_TYPE, MSG_PRIO, have_to_wait)); \
    m->size_in_bytes(MSG_DATA_SIZE); \
    m->attach_data(MSG_DATA); \
    if (have_to_wait) \
      this->wait_msg_handled(m, MSG_TMO); \
    else \
      this->post(m); \
  } \
  catch (const dsl::DAQException &) { \
  	throw; \
  } \
  catch (const yat::Exception & e) { \
  	throw DAQException(e); \
  } \
  catch (...) { \
  	throw DAQException(); \
  } \
} \
while (0)

//-----------------------------------------------------------------------------
// POST MSG SHORTCUT
//-----------------------------------------------------------------------------
#define POST_EXCEPTION(_E) \
 POST_MSG_WITH_DATA(MessageType::DSL_MSG_ERROR, MessagePriority::DSL_MSG_PRIO_ERROR, _E, 0, 0)

//-----------------------------------------------------------------------------
// POST MSG SHORTCUT
//-----------------------------------------------------------------------------
#define POST_TIMEOUT(_E) \
 POST_MSG_WITH_DATA(MessageType::DSL_MSG_TIMEOUT, MessagePriority::DSL_MSG_PRIO_TIMEOUT, _E, 0, 0)

//-----------------------------------------------------------------------------
// POST MSG SHORTCUT
//-----------------------------------------------------------------------------
#define POST_ERROR(_R, _D, _S) \
  do \
  { \
    DAQException _E(static_cast<const char*>(_R), \
                    static_cast<const char*>(_D), \
                    static_cast<const char*>(_S)); \
    POST_EXCEPTION(_E); \
  } \
  while (0);
//-----------------------------------------------------------------------------

namespace dsl {

// ============================================================================
// DaqThread 
// ============================================================================
class DSL_EXPORT DaqThread : public yat::Thread
{
public:  
  //- ctor
	DaqThread (DAQMode _mode) 
    : yat::Thread(), 
      daq_mode_(_mode),
      go_on_(true), 
      suspended_(false),
      daq_pulsed_(true),
      daq_pulse_lock_(),
      daq_pulse_condition_(daq_pulse_lock_)
  {
    //- std::cout << "DaqThread::DaqThread <-" << std::endl;
    //- std::cout << "DaqThread::DaqThread ->" << std::endl;
  }
  
  //- dtor
  virtual ~DaqThread () 
  {
    //- std::cout << "DaqThread::~DaqThread <-" << std::endl;
    //- std::cout << "DaqThread::~DaqThread ->" << std::endl;
  }
  
  //- suspend
  inline void suspend () 
  {
		//- std::cout << "DaqThread::suspend <-" << std::endl;
		yat::AutoMutex<> guard(this->daq_pulse_lock_);
    this->suspended_ = true;
    //- std::cout << "DaqThread::suspend ->" << std::endl;
  }
  
  //- resume
  inline void resume () 
  {
    //- std::cout << "DaqThread::resume <-" << std::endl;
    {
    	yat::AutoMutex<> guard(this->daq_pulse_lock_);
      this->suspended_ = false;
  		this->daq_pulse_condition_.signal();
    }  
    //- std::cout << "DaqThread::resume ->" << std::endl;
  }
  
  //- suspended
  inline bool suspended () 
  {
    return this->suspended_;
  }

  //- should the thread go on?
  inline bool go_on () const 
  {
    return this->go_on_;
  }
  
  //- set daq mode 
  inline void daq_mode (DAQMode _mode) 
  {
  	this->daq_mode_ = _mode;
  }
  
  //- returns daq mode
  inline DAQMode daq_mode () const 
  {
  	return this->daq_mode_;
  }
  
  //- causes the thread to start undetached
  void start_undetached (Thread::IOArg _arg)
    throw (yat::Exception)
  {
  	//- std::cout << "DaqThread::start_undetached <-" << std::endl;
  	this->argin_ = _arg;
  	this->Thread::start_undetached();
    //- std::cout << "DaqThread::start_undetached ->" << std::endl;
  }
  
  //- impl of the yat::Thread::exit pure virtual method 
  virtual void exit () 
  {
    //- std::cout << "DaqThread::exit <-" << std::endl;
    {
    	yat::AutoMutex<> guard(this->daq_pulse_lock_);
      this->go_on_ = false;
  		this->daq_pulse_condition_.signal();
    }  
    this->join(0);
    //- std::cout << "DaqThread::exit ->" << std::endl;
  }
  
  //- raise a daq pulse
  inline void raise_daq_pulse ()
  {
    {
    	yat::AutoMutex<> guard(this->daq_pulse_lock_);
      this->daq_pulsed_ = true;
  		this->daq_pulse_condition_.signal();
    }  
  }
  
  //- wait for a daq pulse to be raised
  inline void wait_daq_pulse ()
  {
    {
      yat::AutoMutex<> guard(this->daq_pulse_lock_);
      while ((! this->daq_pulsed_ || this->suspended_) && this->go_on_)
        this->daq_pulse_condition_.timed_wait(500);
      this->daq_pulsed_ = false;
    }
  }
  
protected:

  //- the thread entry point:
  //- delegates activity to <ContinuousAI::daq_thread_entry_point>
  virtual IOArg run_undetached (Thread::IOArg _arg) 
  {
  	//- std::cout << "DaqThread::run_undetached <-" << std::endl;
    ContinuousAI * ai = reinterpret_cast<ContinuousAI*>(this->argin_);
    if (ai == 0) return 0;
    ai->daq_thread_entry_point(this);
    //- std::cout << "DaqThread::run_undetached ->" << std::endl;
    return 0;
  }
  
private:
  //- daq mode
  DAQMode daq_mode_;
  
  //- the thread will "continue to work" till this flag switches to <false>
  volatile bool go_on_;
  
  //- thread suspended "flag"
  volatile bool suspended_;
  
  //- stuffs for <daq_mode_ == dsl::user_pulsed>
  //- the conditon var. will also be used to resume/suspend the daq thread
  volatile bool daq_pulsed_;
  yat::Mutex daq_pulse_lock_;
  yat::Condition daq_pulse_condition_;
  
  //- entry point _argin
  Thread::IOArg argin_;
};

// ============================================================================
// ContinuousAI::ContinuousAI
// ============================================================================
ContinuousAI::ContinuousAI ()
  : daq_thread_(0), digitizer_ (0)
{
  //- std::cout << "ContinuousAI::ContinuousAI <-" << std::endl;
  //- std::cout << "ContinuousAI::ContinuousAI ->" << std::endl;
}

// ============================================================================
// ContinuousAI::~ContinuousAI
// ============================================================================
ContinuousAI::~ContinuousAI ()
{
  //- std::cout << "ContinuousAI::~ContinuousAI <-" << std::endl;
  //- std::cout << "ContinuousAI::~ContinuousAI ->" << std::endl;
}

// ============================================================================
// ContinuousAI::handle_message
// ============================================================================
void ContinuousAI::handle_message (yat::Message & msg)
	throw (yat::Exception)
{
  // Filter message... 
  //---------------------------
  switch (msg.type())
  {
    // <TASK_INIT> message
    //-----------------------------
    case yat::TASK_INIT:
    {
      //- noop (see ContinuousAI::init)
    }
    break;
    // <TASK_EXIT> message
    //-----------------------------
    case yat::TASK_EXIT:
    {
      this->fini_i();
    }
    break;
    // <TASK_TIMEOUT> message
    //-----------------------------
    case yat::TASK_TIMEOUT:
    {
      //- noop (see DSL_MSG_TIMEOUT msg handling)
    }
    // <TASK_PERIODIC> message
    //-----------------------------
    case yat::TASK_PERIODIC:
    {
      //- noop
    }
    break;
    // <START> message
    //-----------------------------
    case MessageType::DSL_MSG_START:
    {
      //- std::cout << "MessageType::DSL_MSG_START <-" << std::endl;
      try
      {
        this->start_i();
      }
      catch (const DAQException & e) { e.dump(); }
      catch (...) { }
      //- std::cout << "MessageType::DSL_MSG_START ->" << std::endl;
    }
    break;
    // <STOP>/<ABORT> message
    //-------------------------
    case MessageType::DSL_MSG_STOP:
    case MessageType::DSL_MSG_ABORT:
    {
      //- std::cout << "MessageType::DSL_MSG_STOP <-" << std::endl;
      try
      {
        this->stop_i();
      }
      catch (const DAQException & e) { e.dump(); }
      catch (...) { }
      //- std::cout << "MessageType::DSL_MSG_STOP ->" << std::endl;
    }
    break;
    // <CALIBRATION> message
    //-----------------------------
    case MessageType::DSL_MSG_CALIB:
    {
      //- std::cout << "MessageType::DSL_MSG_CALIB <-" << std::endl;
      try
      {
        this->calibrate_hardware_i();
      }
      catch (const DAQException & e) { e.dump(); }
      catch (...) { }
      //- std::cout << "MessageType::DSL_MSG_CALIB ->" << std::endl;
    }
    break;
    // <CONFIG> message
    //-----------------------------
    case MessageType::DSL_MSG_CONFIG:
    {
      //- std::cout << "MessageType::DSL_MSG_CONFIG <-" << std::endl;
      try
      {
        ContinuousAIConfig & cfg = msg.get_data<ContinuousAIConfig>();
        this->configure_i(cfg);
      }
      catch (const DAQException & e) { e.dump(); }
      catch (...) { }
      //- std::cout << "MessageType::DSL_MSG_CONFIG ->" << std::endl;
    }
    break;
    // <DATA> message
    //-----------------------------
    case MessageType::DSL_MSG_DATA:
    {
      //- detach data (i.e. get ownership)
      Waveforms * wfms = 0;
      msg.detach_data(wfms);
      //- call user defined handle_input
      try 
      {
        if (this->daq_state_is(ContinuousDAQ::RUNNING))
          this->handle_waveforms(wfms);
      }
      catch (const DAQException & e) { e.dump(); }
      catch (...) { }
    }
    break;
    // <TIMEOUT> message
    //-----------------------------
    case MessageType::DSL_MSG_TIMEOUT:
    {
      //- should we treat timeout as an error? : no
      if (! this->config_.treat_timeout_as_error())
      {
        try 
        {
          //- call user defined <handle_timeout>
          if (this->daq_state_is(ContinuousDAQ::RUNNING))	
            this->handle_timeout();
        }
        catch (const DAQException & e) { e.dump(); }
        catch (...) { }
      }
      //- should we treat timeout as an error? : yes
      else
      {
        //- do nothing if already in fault.
        //- implementation note: this works cause the <daq thread>...
        //-   1. is the only <ContinuousDAQ actor> to post <DSL_MSG_TIMEOUT> messages
        //-   2. doesn't change the <ContinuousAI> internal state 
        if (this->daq_state_is(ContinuousDAQ::FAULT))
          break;
        try 
        {
          //- stop daq (will not throw)
          this->stop_i(true);
          //- extract error from msg data
          TimeoutException & e = msg.get_data<TimeoutException>();
          //- latch error & switch state to <FAULT>
          this->latch_error(e);
          //- call user defined <handle_error>
          this->handle_error(e);
        }
        catch (const DAQException & e) { e.dump(); }
        catch (...) { }
      }
    }
    break;
    // <MSG_ERROR> message
    //-----------------------------
    case MessageType::DSL_MSG_ERROR:
    {
      //- do nothing if already in fault. 
      //- implemantation note: this works cause the <daq thread>...
      //-   1. is the only <ContinuousDAQ actor> to post <DSL_MSG_ERROR> messages
      //-   2. doesn't change the <ContinuousAI> internal state 
      if (this->daq_state_is(ContinuousDAQ::FAULT))
        break;
      try 
      {
        //- stop daq (will not throw)
        this->stop_i(true);
        //- extract error from msg data
        DAQException & e = msg.get_data<DAQException>();
        //- latch error & switch state to <FAULT>
        this->latch_error(e);
        //- call user defined <handle_error>
        this->handle_error(e);
      }
      catch (const DAQException & e) { e.dump(); }
      catch (...) { }
    }
    break;
    // <msg> has a type we do not handle
    //-----------------------------------
    default:
      this->handle_user_msg(msg);
      break;
  }
}

// ============================================================================
// ContinuousAI::handle_user_msg
// ============================================================================
void ContinuousAI::handle_user_msg (yat::Message & msg)
{
  //- this will simply trash/ignore the message...
  return;
}

// ============================================================================
// ContinuousAI::init
// ============================================================================
void ContinuousAI::init (unsigned short _type, unsigned short _id, bool _calibration)
    throw (DAQException)
{
  //- std::cout << "ContinuousAI::init <-" << std::endl;

  //- return if already initialized
  if (this->initialized()) 
    return; 

  //- std::cout << "  start underlying task" << std::endl;
  //- start the underlying yat::task
  this->go(10000); // tmo 10s to let hardware init end

  try 
  {
    //- std::cout << "  instanciate the board" << std::endl;
    //- instanciate the board - get exclusive access
    this->digitizer_ = 0;
    this->digitizer_ = Digitizer::instanciate(_type, _id, _calibration, true);
    //- std::cout << "  end of instanciation" << std::endl;
  }
  catch (const DAQException & e) 
  {
    //- release digitizer
    if (this->digitizer_)
    {
      this->digitizer_->release();
      this->digitizer_ = 0;
    }
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw;
  }
  catch (...) 
  {
    //- release digitizer
    if (this->digitizer_)
    {
      this->digitizer_->release();
      this->digitizer_ = 0;
    }
    //- latch error & switch state to <FAULT>
    DAQException e("ContinuousAI::init");
    this->latch_error(e);
    throw e;
  }
   
  //- switch state to STANDBY (ready to run with default DAQ config)
  this->state_ = ContinuousDAQ::STANDBY;
  //- std::cout << "ContinuousAI::init ->" << std::endl;
}

// ============================================================================
// ContinuousAI::configure
// ============================================================================
void ContinuousAI::configure (const ContinuousAIConfig& _config, size_t tmo_ms)
  throw (DAQException, DeviceBusyException)
{
  //- std::cout << "ContinuousAI::configure <-" << std::endl;
  
  //- operation allowed?
  this->validate_request("ContinuousAI::configure");
  
  POST_MSG_WITH_DATA(MessageType::DSL_MSG_CONFIG, 
  									 MessagePriority::DSL_MSG_PRIO_CONFIG, 
                     _config, 
                     sizeof(ContinuousAIConfig), 
                     tmo_ms);

  //- std::cout << "ContinuousAI::configure ->" << std::endl;
}
  
// ============================================================================
// ContinuousAI::configure_i
// ============================================================================
void ContinuousAI::configure_i (const ContinuousAIConfig & _config)
    throw (DAQException)
{
  //- std::cout << "ContinuousAI::configure_i <-" << std::endl;

  //- operation allowed?
  if (! this->initialized()) 
  {
    throw DAQException("BAD_INIT",
                       "ContinuousAI::init must be (successfully) called prior to any other dsl::ContinuousAI functions",
                       "ContinuousAI::configure");
  }

  //- is DAQ running ?
  bool was_running = this->daq_state_is(ContinuousDAQ::RUNNING);
  
  //- if DAQ running, then stop it...
  try 
  {
    if (was_running) 
    {
    	//- std::cout << "ContinuousAI::configure_i::daq is running. calling stop_i..." << std::endl;
    	this->stop_i();
      //- std::cout << "ContinuousAI::configure_i::ok - daq is now stopped" << std::endl;
    }
  }
  catch (const DAQException& e)
  {
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw;
  }
  catch (...) 
  {
    DAQException e("DAQ stop failed",
                   "unknown exception caught while trying to stop DAQ for hardware reconfiguration",
                   "ContinuousAI::ContinuousAI::configure_i");
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw e;
  }

  //- check ADC(s) per channel
  bool more_than_one_adc_per_channel = _config.get_num_adc_per_channel() != dsl::one_adc_per_channel;
  bool more_than_half_of_channels_active = _config.num_active_channels() > (this->digitizer_->num_channels() / 2);
  if (more_than_half_of_channels_active && more_than_one_adc_per_channel)
  {
    throw DAQException("invalid num. of ADC(s) per channel specified",
                       "invalid configuration - the specified <num. of ADC(s) per channel> is not compatible with the number of active channels",
                       "ContinuousAI::configure_i");
  }

  //- get <num of samples per channel>
  unsigned long depth = _config.get_buffer_depth();

  //- actual depth is <num of samples per channel> * <num of active channels>
  depth *= _config.num_active_channels();

  //- be sure buffer size is even
  if (depth % 2)
  {
    //- std::cout << "the result of [{DAQ buffer depth} x {num. of active channels}] must be even" << std::endl;
    throw DAQException("invalid DAQ buffer depth",
                       "the result of [{DAQ buffer depth} x {num. of active channels}] must be even",
                       "ContinuousAI::configure_i");
  }

  //- be sure timeout is valid
  size_t correct_timeout = static_cast<size_t>(1000. * _config.get_buffer_depth() / _config.get_sampling_rate());
  if (_config.get_timeout() < correct_timeout)
  {
    //- std::cout << "timeout in msecs must be >= {1000. * buffer_depth_in_samples / sampling_rate_in_hz}" << std::endl;
    throw DAQException("invalid timeout specified",
                       "timeout in msecs must be >= {1000 * buffer_depth / sampling_rate}",
                       "ContinuousAI::configure_i");   
  }

  //- ok. let's copy <_config> locally
  this->config_ = _config;

  //- set timeout (ajust it cause notification come from the DAQ thread)
  this->set_timeout_msg_period(static_cast<size_t>(this->config_.get_timeout() * 1.1));
  this->enable_timeout_msg(true);
  
  //- configure task's msgQ
  this->msgq_wm_unit(this->config_.msgq_wm_unit_);
 	this->msgq_lo_wm(this->config_.msgq_lo_wm_);
 	this->msgq_hi_wm(this->config_.msgq_hi_wm_);

  //- if DAQ was running then restart it...
  try 
  {
    if (was_running) 
    {
    	//- std::cout << "ContinuousAI::handle_hardware_calibration_i::restarting daq..." << std::endl;
    	this->start_i();
    	//- std::cout << "ContinuousAI::handle_hardware_calibration_i::ok - daq successfully restarted" << std::endl;
  	}
  }
  catch (const DAQException& e)
  {
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw;
  }
  catch (...) 
  {
    DAQException e("DAQ stop failed",
                   "unknown exception caught while trying to restart DAQ after hardware calibration",
                   "ContinuousAI::ContinuousAI::configure_i");
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw e;
  }

  this->dump();

  //- std::cout << "ContinuousAI::configure_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::start
// ============================================================================
void ContinuousAI::start (size_t tmo_ms)
  throw (DAQException, DeviceBusyException)
{
  //- std::cout << "ContinuousAI::start <-" << std::endl;
    
  //- operation allowed?
  this->validate_request("ContinuousAI::start");
  
  POST_MSG(MessageType::DSL_MSG_START, MessagePriority::DSL_MSG_PRIO_START, tmo_ms);
  
  //- std::cout << "ContinuousAI::start ->" << std::endl;
}

// ============================================================================
// ContinuousAI::start_i
// ============================================================================
void ContinuousAI::start_i ()
    throw (DAQException)
{
  //- std::cout << "ContinuousAI::start_i <-" << std::endl;

  //- properly initialized?
  if (! this->initialized()) 
  {
    throw DAQException("BAD_INIT",
                       "ContinuousAI::init must be (successfully) called prior to any other dsl::ContinuousAI functions",
                       "ContinuousAI::start");
  }

  //- operation allowed?
  if (! this->daq_state_is(ContinuousDAQ::STANDBY)) 
		return;
  
  try 
  {
    //- configure hardware
    //- std::cout << "ContinuousAI::start_i::configuring hw..." << std::endl;
    this->digitizer_->configure_continuous_ai(this->config_);
    //- std::cout << "ContinuousAI::start_i::hw configured!" << std::endl;
  } 
  catch (const DAQException& e) 
  {
    //- latch error & switch state to <FAULT>
    e.dump(); 
    this->latch_error(e);
    throw;
  }
  catch (...) 
  {
    DAQException e("ContinuousAI::start_i");
    this->latch_error(e);
    throw e;
  }

  try 
  {
    //- start daq
    //- std::cout << "ContinuousAI::start_i::starting daq..." << std::endl;
    this->digitizer_->start_continuous_ai();
    //- std::cout << "ContinuousAI::start_i::daq started!" << std::endl;
  } 
  catch (const DAQException& e) 
  {
    //- latch error & switch state to <FAULT>
    e.dump(); 
    this->latch_error(e);
    throw;
  }
  catch (...) 
  {
    DAQException e("ContinuousAI::start_i");
    this->latch_error(e);
    throw e;
  }
  
  //- instanciate/start daq thread if not already exists
  if (! this->daq_thread_)
  { 
    //- std::cout << "ContinuousAI::start_i::instanciating daq thread <-" << std::endl;
    
    //- instanciate daq thread
    this->daq_thread_ = DSL_NEW(DaqThread(this->config_.daq_mode()));
    if (! this->daq_thread_) 
    {
      throw DAQException("out of memory",
                         "out of memory error [DAQThread instanciation failed]",
                         "ContinuousAI::init");
    }
    
    //- std::cout << "ContinuousAI::start_i::daq thread instanciated ->" << std::endl;
    //- std::cout << "ContinuousAI::start_i::spawning thread <-" << std::endl;
    
    //- start daq thread
    this->daq_thread_->start_undetached(this); 
    
    //- std::cout << "ContinuousAI::start_i::daq thread spawn ->" << std::endl;
  }
  
#if defined (_NI_SUPPORT_) && defined(HAS_THREADING_PROBLEM)

  //- just resume it otherwise 
  else 
  {
    //- std::cout << "ContinuousAI::start_i::resuming daq thread <-" << std::endl;

    this->daq_thread_->resume();
    
    //- std::cout << "ContinuousAI::start_i::daq thread resumed ->" << std::endl;
  }

#endif //- (_NI_SUPPORT_) && defined(HAS_THREADING_PROBLEM)

  //- switch state...
	this->state_ = ContinuousDAQ::RUNNING;

  //- std::cout << "ContinuousAI::start_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::stop
// ============================================================================
void ContinuousAI::stop (size_t tmo_ms)
  throw (DAQException, DeviceBusyException)
{
  //- std::cout << "ContinuousAI::stop <-" << std::endl;
    
  //- operation allowed?
  this->validate_request("ContinuousAI::stop");
  
  POST_MSG(MessageType::DSL_MSG_STOP, MessagePriority::DSL_MSG_PRIO_STOP, tmo_ms);
  
  //- std::cout << "ContinuousAI::stop ->" << std::endl;
}

// ============================================================================
// ContinuousAI::abort
// ============================================================================
void ContinuousAI::abort (size_t tmo_ms)
  throw (DAQException, DeviceBusyException)
{
  //- std::cout << "ContinuousAI::abort <-" << std::endl;
    
  //- operation allowed?
  this->validate_request("ContinuousAI::abort");
  
  POST_MSG(MessageType::DSL_MSG_ABORT, MessagePriority::DSL_MSG_PRIO_ABORT, tmo_ms);
  
  //- std::cout << "ContinuousAI::abort ->" << std::endl;
}

// ============================================================================
// ContinuousAI::stop_i
// ============================================================================
void ContinuousAI::stop_i (bool stop_on_error)
    throw (DAQException)
{
  //- std::cout << "ContinuousAI::stop_i <-" << std::endl;

  //- properly initialized?
  if (! this->initialized()) 
  {  
  	if (! stop_on_error)
  	  throw DAQException("BAD_INIT",
                         "ContinuousAI::init must be (successfully) called prior to any other dsl::ContinuousAI functions",
      	                 "ContinuousAI::start");
  }

  //- operation allowed?
  if (! this->daq_state_is(ContinuousDAQ::RUNNING))
		return;

#if defined (_NI_SUPPORT_) && defined(HAS_THREADING_PROBLEM)

  //- suspend daq thread
  if (this->daq_thread_) 
  {
    //- std::cout << "ContinuousAI::stop_i::suspending daq thread <-" << std::endl;
    
    this->daq_thread_->suspend();
    
    //- std::cout << "ContinuousAI::stop_i::daq thread suspended ->" << std::endl;
  }
  
#else //- (_NI_SUPPORT_) && defined(HAS_THREADING_PROBLEM)

  //- stop daq thread
  if (this->daq_thread_) 
  {
    //- std::cout << "ContinuousAI::stop_i::waiting for thread to quit <-" << std::endl;
    
    this->daq_thread_->exit();
    this->daq_thread_ = 0;
    
    //- std::cout << "ContinuousAI::stop_i::daq thread exited ->" << std::endl;
  }
  
#endif //- (_NI_SUPPORT_) && defined(HAS_THREADING_PROBLEM)

  //- stop daq
  try 
  {
    this->digitizer_->stop_continuous_ai();
  } 
  catch (const DAQException& e) 
  {
  	if (! stop_on_error)
    {
      //- latch error & switch state to <FAULT>
      this->latch_error(e);
      throw;
    }
  }
  catch (...) 
  {
  	if (! stop_on_error)
    {
      DAQException e("ContinuousAI::stop_i");
      this->latch_error(e);
      throw e;
    }
  }

  if (! stop_on_error)
		this->state_ = ContinuousDAQ::STANDBY;
  
  //- std::cout << "ContinuousAI::stop_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::fini
// ============================================================================
void ContinuousAI::fini ()
  throw (DAQException, DeviceBusyException)
{
  //- std::cout << "ContinuousAI::fini <-" << std::endl;
    
  //- operation allowed?
  this->validate_request("ContinuousAI::fini");
  
  try
  {
  	this->exit();
	}
  catch (...)
  {
		//- should we really ignore error here?
  }
  
  //- std::cout << "ContinuousAI::fini ->" << std::endl;
}

// ============================================================================
// ContinuousAI::fini_i
// ============================================================================
void ContinuousAI::fini_i ()
	throw (DAQException)
{
  //- std::cout << "ContinuousAI::fini_i <-" << std::endl;

  if (! this->initialized()) 
		return;

  //- asks the daq thread to quit
  if (this->daq_thread_) 
  {
    //- std::cout << "ContinuousAI::fini_i::waiting for thread to quit <-" << std::endl;
    this->daq_thread_->exit();
    this->daq_thread_ = 0;
    //- std::cout << "ContinuousAI::fini_i::waiting for thread to quit ->" << std::endl;
  }

  //- stop daq
  yat::Timer t;
  do
  {
    try 
    {
      this->digitizer_->stop_continuous_ai();
      break;
    } 
    catch (const DeviceBusyException&) 
    {
      if (t.elapsed_sec() < 5)
      	continue;
      else 
      	throw;
    }
    catch (const DAQException&) 
    {
      throw;
    }
    catch (...) 
    {
      throw;
    }
	}
  while (1);
  
  //- release daq hardware (this is a yat::SharedObject)
  this->digitizer_->release();
  this->digitizer_ = 0;

	this->state_ = ContinuousDAQ::UNKNOWN;

  //- std::cout << "ContinuousAI::fini_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::calibrate_hardware
// ============================================================================
void ContinuousAI::calibrate_hardware ()
  throw (DAQException, DeviceBusyException)
{
  //- std::cout << "ContinuousAI::calibrate_hardware <-" << std::endl;

  //- operation allowed?
  this->validate_request("ContinuousAI::calibrate_hardware");
    
  POST_MSG(MessageType::DSL_MSG_CALIB, MessagePriority::DSL_MSG_PRIO_CALIB, 0);
  
  //- std::cout << "ContinuousAI::calibrate_hardware ->" << std::endl;
}

// ============================================================================
// ContinuousAI::calibrate_hardware_i
// ============================================================================
void ContinuousAI::calibrate_hardware_i ()
     throw (DAQException)
{
  //- std::cout << "ContinuousAI::handle_hardware_calibration_i <-" << std::endl;

  //- properly initialized?
  if (! this->initialized()) 
  {
    throw DAQException("BAD_INIT",
                       "ContinuousAI::init must be (successfully) called prior to any other dsl::ContinuousAI functions",
                       "ContinuousAI::calibrate_hardware");
  }

  //- we are now handling hw calibration (might take a while) 
  //- this will help client to get appropriate exception (DeviceBusyException)
	this->exception_status_ = ContinuousDAQ::HANDLING_CALIBRATION;

  //- is DAQ running ?
  bool was_running = this->daq_state_is(ContinuousDAQ::RUNNING);
  
  try 
  {
    //- stop DAQ if running
    if (was_running) 
    {
    	//- std::cout << "ContinuousAI::handle_hardware_calibration_i::daq is running. calling stop_i..." << std::endl;
    	this->stop_i();
      //- std::cout << "ContinuousAI::handle_hardware_calibration_i::ok - daq is now stopped" << std::endl;
    }
  }
  catch (const DAQException& e)
  {
    this->exception_status_ = ContinuousDAQ::HANDLING_NONE;
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw;
  }
  catch (...) 
  {
    this->exception_status_ = ContinuousDAQ::HANDLING_NONE;
    DAQException e("DAQ stop failed",
                   "unknown exception caught while trying to stop DAQ for hardware calibration",
                   "ContinuousAI::handle_hardware_calibration_i");
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw e;
  }

  try 
  {
    //- calibrate hardware
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i::calling digitizer::auto_calibrate..." << std::endl;
    this->digitizer_->auto_calibrate();
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i::ok - auto_calibrate returned successfully" << std::endl;
  }
  catch (const DAQException& e)
  {
    this->exception_status_ = ContinuousDAQ::HANDLING_NONE;
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw;
  }
  catch (...) 
  {
    this->exception_status_ = ContinuousDAQ::HANDLING_NONE;
    DAQException e("DAQ stop failed",
                   "unknown exception caught while trying to calibrate the hardware",
                   "ContinuousAI::handle_hardware_calibration_i");
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw e;
  }

  try 
  {
    //- reconfigure the DAQ (release/register done is digitizer_->auto_calibrate)
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i::(re)configuring the board..." << std::endl;
    this->configure_i(this->config_);
    //- std::cout << "ContinuousAI::handle_hardware_calibration_i::ok - board successfully configured" << std::endl;
  }
  catch (const DAQException& e)
  {
    this->exception_status_ = ContinuousDAQ::HANDLING_NONE;
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw;
  }
  catch (...) 
  {
    this->exception_status_ = ContinuousDAQ::HANDLING_NONE;
    DAQException e("DAQ stop failed",
                    "unknown exception caught while trying to reconfigure DAQ after hardware calibration",
                   "ContinuousAI::handle_hardware_calibration_i");
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw e;
  }

  try 
  {
    //- restart DAQ if running
    if (was_running) 
    {
    	//- std::cout << "ContinuousAI::handle_hardware_calibration_i::restarting daq..." << std::endl;
    	this->start_i();
    	//- std::cout << "ContinuousAI::handle_hardware_calibration_i::ok - daq successfully restarted" << std::endl;
  	}
  }
  catch (const DAQException& e)
  {
    this->exception_status_ = ContinuousDAQ::HANDLING_NONE;
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw;
  }
  catch (...) 
  {
    this->exception_status_ = ContinuousDAQ::HANDLING_NONE;
    DAQException e("DAQ stop failed",
                   "unknown exception caught while trying to restart DAQ after hardware calibration",
                   "ContinuousAI::handle_hardware_calibration_i");
    //- latch error & switch state to <FAULT>
    this->latch_error(e);
    throw e;
  }

  this->exception_status_ = ContinuousDAQ::HANDLING_NONE;
    
  //- std::cout << "ContinuousAI::handle_hardware_calibration_i ->" << std::endl;
}

// ============================================================================
// ContinuousAI::pulse_daq
// ============================================================================
void ContinuousAI::pulse_daq ()
	throw (DAQException)
{
  //- properly initialized?
  if (! this->initialized() || ! this->daq_thread_) 
    return;
  
  //- pulse/trigger daq
  this->daq_thread_->raise_daq_pulse();
}
      
// ============================================================================
// ContinuousAI::daq_thread_entry_point
// ============================================================================
void ContinuousAI::daq_thread_entry_point (void * _arg)
{
  //- std::cout << "DaqThread::entering main loop <-" << std::endl;

  //- get thread control flag (controls the almost infinite loop)
  DaqThread * dt = reinterpret_cast<DaqThread*>(_arg);
  if (dt == 0) return;

  //- the digitizer data
  Waveforms * data = 0;

  //- infinite loop 
  while (dt->go_on())
  {
    try 
    {
			//- might have to have for a daq pulse (user triggered daq)
			if (dt->suspended() || dt->daq_mode() == dsl::user_pulsed)
      {
      	//- //- std::cout << "DaqThread::waiting for daq pulse..." << std::endl;
      	dt->wait_daq_pulse();
        //- //- std::cout << "DaqThread::daq pulsed!" << std::endl;
      }
      //- skip remaing steps if thread abort requested while waiting for
      //- daq to be pulsed or thread to be resumed
      if (! dt->go_on())
      {
        //- std::cout << "DaqThread::exit requested..." << std::endl;
        break;
      }
      //- reset data pointer
      data = 0;
      //- get data from hardware
      data = this->digitizer_->get_waveforms(this->config_.get_num_waveforms(), 
      																			 this->config_.get_timeout());
      if (! data) 
      { 
        try
        {
        	POST_ERROR("INVALID_DATA",
                   	 "got unexpected null data pointer from hardware",
                   	 "ContinuousAI::daq_thread_entry_point");
          //- suspend the DAQ thread (task will handle the error msg)
        	dt->suspend();
        }
        catch (...)
        {
					//- std::cout << "DaqThread::POST_ERROR failed! [err. was: INVALID_DATA]" << std::endl;
        }
        continue;
      }  
      //- skip remaing steps if thread abort requested while getting data
      if (! dt->go_on())
      {
        //- std::cout << "DaqThread::exit requested..." << std::endl;
        delete data;
        break;
      }
      //- post msg for processing
      try
      {
#if defined(_SIMULATION_)
      	size_t data_size = 1024 * 1024;
#else
      	size_t data_size = data->size();
#endif 
      	//- //- std::cout << "DaqThread::posting data msg" << std::endl;
				POST_MSG_WITH_DATA(MessageType::DSL_MSG_DATA, 
        									 MessagePriority::DSL_MSG_PRIO_DATA, 
                           data, 
                           data_size, 
                           0);
      }
      catch (...)
      {
				//- std::cout << "DaqThread::POST_MSG_WITH_DATA failed!" << std::endl;
      }
    }
    catch (const dsl::TimeoutException& _tmo_ex)
    {
      try
      {
        if (this->config_.treat_timeout_as_error())
				  POST_EXCEPTION(_tmo_ex);
        else
          POST_TIMEOUT(_tmo_ex);
      }
      catch (...)
      {
				//- std::cout << "DaqThread::POST_TIMEOUT failed!" << std::endl;
      }
      delete data;
    }
    catch (const DAQException& _ex)
    {
    	try
      {
				POST_EXCEPTION(_ex);
        //- suspend the DAQ thread (task will handle the error msg)
        dt->suspend();
      }
      catch (...)
      {
				//- std::cout << "DaqThread::POST_EXCEPTION failed! [err. was: DAQ EXCEPTION]" << std::endl;
      }
      delete data;
    }
    catch (...)
    {
      try
      {
      	POST_ERROR("unknown error",
                 	 "unknown exception caugth from Digitizer::get_data",
                 	 "ContinuousAI::daq_thread_entry_point");
        //- suspend the DAQ thread (task will handle the error msg)
        dt->suspend();
      }
      catch (...)
      {
				//- std::cout << "DaqThread::POST_EXCEPTION failed! [err. was: UNKNOWN EXCEPTION]" << std::endl;
      }
      delete data;
    }
  }

  //- std::cout << "DaqThread::exited main loop ->" << std::endl;
}

// ============================================================================
// ContinuousAI::hardware
// ============================================================================
const Digitizer & ContinuousAI::hardware () const throw (DAQException)
{
  if (this->digitizer_ == 0)
  {
    throw DAQException("initialization failed",
                       "this ContinuousAI instance is not properly initialized",
                       "ContinuousAI::hardware");
  }
  
  return *this->digitizer_;
}

// ============================================================================
// ContinuousAI::dump
// ============================================================================
void ContinuousAI::dump () const
{
  std::cout << "----------------------------" << std::endl;
  std::cout << "-- ContinuousAI             " << std::endl;
  std::cout << "----------------------------" << std::endl;

  //- return if not initialized
  if (! this->initialized()) 
  {
    std::cout << ". not initialized" << std::endl;
    return; 
  }

  switch (this->ContinuousDAQ::daq_state()) 
  {
    case ContinuousDAQ::STANDBY:
      std::cout << ". state: STANDBY" << std::endl;
      break;
    case ContinuousDAQ::RUNNING:
      std::cout << ". state: RUNNING" << std::endl;
      break;
    case ContinuousDAQ::FAULT:
      std::cout << ". state: FAULT" << std::endl;
      break;
    default:
      std::cout << ". state: UNKNOWN" << std::endl;
      break;
  }

  std::cout << "-- DAQ Hardware: " << std::endl;

  this->digitizer_->dump(); 

  std::cout << "----------------------------" << std::endl;
}

} // namespace dsl



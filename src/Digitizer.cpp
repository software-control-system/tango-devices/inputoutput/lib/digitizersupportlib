// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Digitizer.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <dsl/DSLConfig.h> 
#if defined(_NI_SUPPORT_) 
# include <dsl/ni/NIDigitizer.h>
#elif defined(_ACQIRIS_SUPPORT_) || defined(_AGILENT_SUPPORT_)
# include <dsl/acqiris/AcqirisDigitizer.h>
#endif

namespace dsl {

// ============================================================================
// Digitizer::instanciate
// ============================================================================
Digitizer * Digitizer::instanciate (unsigned short _type, 
                                    unsigned short _id,
                                    bool _calibration,
                                    bool _exclusive_access)
  throw (DAQException)
{
  std::cout << "Digitizer::instanciate() entering -->> calibration = " << _calibration << " - exclusive = " << _exclusive_access << std::endl;
  Digitizer * d = 0;

#if defined(_NI_SUPPORT_) 

  d = dsl::NIDigitizer::instanciate(_type, _id, _exclusive_access);
  
#elif defined(_ACQIRIS_SUPPORT_) || defined(_AGILENT_SUPPORT_)

  d = dsl::AcqirisDigitizer::instanciate(_type, _id, _exclusive_access, _calibration);
  
#endif
  return d;
}

// ============================================================================
// Digitizer::Digitizer
// ============================================================================
Digitizer::Digitizer (unsigned short _type, 
                      unsigned short _id) 
  : CalibrableHardware(_type, _id)
  , hh_(kUNDEFINED_HH)
{
  //- std::cout << "Digitizer::Digitizer <-" << std::endl;
  //- std::cout << "Digitizer::Digitizer ->" << std::endl;
}

// ============================================================================
// Digitizer::~Digitizer
// ============================================================================
Digitizer::~Digitizer ()
{
  //- std::cout << "Digitizer::~Digitizer <-" << std::endl;
  //- std::cout << "Digitizer::~Digitizer ->" << std::endl;
}

// ============================================================================
// Digitizer::check_registration
// ============================================================================
void Digitizer::check_registration () const throw (DAQException) 
{
#if ! defined (_SIMULATION_)
  if (this->hh_ ==  kUNDEFINED_HH) 
  {
	  throw DAQException(static_cast<const char*>("invalid registration identifier"),
							              static_cast<const char*>("board initialization failed"),
							              static_cast<const char*>("Digitizer::check_registration"));
  }
#endif
}

} // namespace dsl




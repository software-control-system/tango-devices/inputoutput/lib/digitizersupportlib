// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    DSL.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSL.h>
#if defined(_NI_SUPPORT_) 
# include <dsl/ni/NIDigitizer.h>
#elif defined(_ACQIRIS_SUPPORT_)
# include <dsl/acqiris/AcqirisDigitizer.h>
#endif

namespace dsl {
 
// ============================================================================
// DSL::init
// ============================================================================
void DSL::init ()
    throw (Exception)
{
#if ! defined (_SIMULATION_) && defined(_NI_SUPPORT_)  
  //- IVI bug workaround --------------------
  ViSession vi;
  ::niScope_init(static_cast<ViRsrc>("dummy_hw_rsrc_name"), VI_FALSE, VI_FALSE, &vi);
  ::niScope_close(vi);
#endif
}

// ============================================================================
// DSL::close
// ============================================================================
void DSL::close ()
    throw (Exception)
{
  //- noop
}

} // namespace dsl




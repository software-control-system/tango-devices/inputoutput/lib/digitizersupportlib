// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    CalibrableHardware.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#if defined(_SIMULATION_)
# include <yat/threading/Thread.h>
#endif
#include <dsl/CalibrableHardware.h>

namespace dsl { 

// ============================================================================
// CalibrableHardware::CalibrableHardware
// ============================================================================
CalibrableHardware::CalibrableHardware (unsigned short _type,  unsigned short _id)
  : Hardware(_type, _id) 
{
  //- std::cout << "CalibrableHardware::CalibrableHardware <-" << std::endl;
  //- std::cout << "CalibrableHardware::CalibrableHardware ->" << std::endl;
}

// ============================================================================
// CalibrableHardware::~CalibrableHardware
// ============================================================================
CalibrableHardware::~CalibrableHardware ()
{
  //- std::cout << "CalibrableHardware::~CalibrableHardware <-" << std::endl;
  //- std::cout << "CalibrableHardware::~CalibrableHardware ->" << std::endl;
}

// ============================================================================
// CalibrableHardware::auto_calibrate
// ============================================================================
void CalibrableHardware::auto_calibrate ()
  throw (DAQException)
{
#if ! defined(_SIMULATION_)
    
  throw DAQException("MISSING_FEATURE",
                          "this hardware does not support <auto calibration>",
                          "CalibrableHardware::auto_calibrate",
                           -1);
#else

  //- std::cout << "CalibrableHardware::auto_calibrate <-" << std::endl;
  
  yat::Thread::sleep(3000);
  
  //- std::cout << "CalibrableHardware::auto_calibrate ->" << std::endl;
  
#endif //- _SIMULATION_
}

} // namespace dsl








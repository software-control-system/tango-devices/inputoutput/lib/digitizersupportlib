// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed DownConverter Support Library
//
// = FILENAME
//    DownConverter.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifdef DOWN_CONVERTER_SUPPORT

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#if defined(_NI_SUPPORT_)
# include <dsl/ni/NIDownConverter.h>
#else 
# include <dsl/DownConverter.h>
#endif

namespace dsl {

// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#if defined(_NI_SUPPORT_) 
# define CD_HW_SUPPORT "current impl. supports NI boards: 5600"
#elif defined(_ACQIRIS_SUPPORT_)
# define CD_HW_SUPPORT "current impl. does not support any Acqiris board"
#else
# error no manufacturer specified
#endif
 
// ============================================================================
// DownConverter::instanciate
// ============================================================================
DownConverter * DownConverter::instanciate (unsigned short _type, 
                                            unsigned short _id,
                                            bool _exclusive_access)
  throw (DAQException)
{
  DownConverter * d = 0;

#if defined(_NI_SUPPORT_) 
  d = dsl::NIDownConverter::instanciate(_type, _id, _exclusive_access);
#endif
  
  return d;
}

// ============================================================================
// DownConverter::DownConverter
// ============================================================================
DownConverter::DownConverter (unsigned short _type, unsigned short _id) 
 : Hardware(_type, _id)
{
  std::cout << "DownConverter::DownConverter <-" << std::endl;
  std::cout << "DownConverter::DownConverter ->" << std::endl;
}

// ============================================================================
// DownConverter::~DownConverter
// ============================================================================
DownConverter::~DownConverter ()
{
  std::cout << "DownConverter::~DownConverter <-" << std::endl;
  std::cout << "DownConverter::~DownConverter ->" << std::endl;
}

// ============================================================================
// DownConverter::get_attenuation
// ============================================================================
const DownConverter::Attenuation& DownConverter::get_attenuation () const 
{
  return this->att_;
}

// ============================================================================
// DownConverter::get_tuning
// ============================================================================
const DownConverter::Tuning& DownConverter::get_tuning () const
{
  return this->tuning_;
}

} // namespace dsl

#endif


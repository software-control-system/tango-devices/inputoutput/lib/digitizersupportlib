// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    NI5122.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h> 

#if defined(_NI_SUPPORT_)

#include <iostream>
#include <dsl/ni/NI5122.h>

namespace dsl { 
   
// ============================================================================
// STATICS
// ============================================================================
yat::Mutex NI5122::repository_lock;
//-----------------------------------------------------------------------------
NI5122::Repository NI5122::repository;
//-----------------------------------------------------------------------------

// ============================================================================
// NI5122::instanciate
// ============================================================================
NI5122 * NI5122::instanciate (unsigned short _id, bool _exclusive_access)
  throw (DAQException)
{
  //- lock
  yat::AutoMutex<> guard(NI5122::repository_lock);

  //- search requested board in the repository
  RepositoryIterator it = NI5122::repository.find(_id);

  //- already instanciated?
  if (it != NI5122::repository.end()) 
  {
    //- can't get ownership if _exclusive_access requested
    if (_exclusive_access || it->second->exclusive_access_) 
    {
      //- can't get exclusive access to hardware
      throw DAQException("hardware already in use",
                              "could not get exclusive access to the hardware",
                              "NI5122::instanciate");
    }
    //- share the board
    else 
    {
      return it->second->duplicate();
    }
  }

  //- instanciate the digitizer
  NI5122* d = new NI5122(_id);
  if (d == 0) 
  {
    //- out of memory
    throw DAQException("out of memory",
                            "out of memory error",
                            "NI5122::instanciate");
  }

  //- update board's <exclusive_access> flag
  d->exclusive_access_ = _exclusive_access;

  //- open driver session
  try 
  {
    d->register_hardware();
  }
  catch (const DAQException&)
  {
    //- release digitizer
    d->release();
    throw;
  }
  catch (...)
  {
    //- release digitizer
    d->release();
    //- hardware registration failed
    throw DAQException("hardware registration failed",
                            "unknown exception caught while trying to register the board",
                            "NI5122::instanciate");
  }

  return d;
}

// ============================================================================
// NI5122::duplicate
// ============================================================================
NI5122 * NI5122::duplicate ()
{
  return dynamic_cast<NI5122*>(this->SharedObject::duplicate()); 
}
 
// ============================================================================
// NI5122::NI5122
// ============================================================================
NI5122::NI5122(unsigned short _id) 
  : NIDigitizer(dsl::NI_5122, _id)
{
  //- std::cout << "NI5122::NI5122 <-" << std::endl;

  //- store digitizer into the repository
  RepositoryValue pair(_id, this);
  NI5122::repository.insert(pair);

  //- std::cout << "NI5122::NI5122 ->" << std::endl;
}

// ============================================================================
// NI5122::~NI5122
// ============================================================================
NI5122::~NI5122()
{
  //- lock
  yat::AutoMutex<> guard(NI5122::repository_lock);
										 
  //- std::cout << "NI5122::~NI5122 <-" << std::endl;

  //- release hardware
  this->release_hardware();

  //- remove board from repository
  RepositoryIterator it = NI5122::repository.find(this->id_);
  if (it != NI5122::repository.end()) 
  {
    NI5122::repository.erase(it);    
  }

   //- std::cout << "NI5122::~NI5122 <-" << std::endl;
}

// ============================================================================
// NI5122::clock_frequency
// ============================================================================
double NI5122::max_sampling_rate () const
{
	return (double)NI_5122_MAX_SAMPLING_RATE;
}

// ============================================================================
// Returns the numbers of channels of the board
// ============================================================================
unsigned short NI5122::num_channels () const
{
	return NI_5122_NUM_CHANNELS;
}

// ============================================================================
// Return the input impedance of the board
// ============================================================================
double NI5122::input_impedance () const
{
	return (double)NI_5122_INPUT_IMPEDANCE;
}

// ============================================================================
// NI5122::dump
// ============================================================================
void NI5122::dump () const
{
	//- std::cout << ". manufacturer.......National Instruments" << std::endl;
	//- std::cout << ". type...............5122" << std::endl;
	//- std::cout << ". board id..........." << this->id_ << std::endl;
	//- std::cout << ". hardware handle...." << this->hh_ << std::endl;
	//- std::cout << ". exclusive access..." << (this->exclusive_access_ ? "yes" : "no") << std::endl;
}

} // namespace dsl

#endif //- _NI_SUPPORT_

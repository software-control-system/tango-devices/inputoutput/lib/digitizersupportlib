// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    NI5620.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h> 

#if defined(_DOWN_CONVERTER_SUPPORT_) && defined(_NI_SUPPORT_)

#include <iostream>
#include <dsl/Rsrc.h>
#include <dsl/ni/NI5620.h>

namespace dsl { 

// ============================================================================
// STATICS
// ============================================================================
yat::Mutex NI5620::repository_lock;
//-----------------------------------------------------------------------------
NI5620::Repository NI5620::repository;
//-----------------------------------------------------------------------------

// ============================================================================
// NI5620::instanciate
// ============================================================================
NI5620 * NI5620::instanciate (unsigned short _id, bool _exclusive_access)
  throw (DAQException)
{
  //- lock
  yat::AutoMutex<> guard(NI5620::repository_lock);

  //- search requested board in the repository
  RepositoryIterator it = NI5620::repository.find(_id);

  //- already instanciated?
  if (it != NI5620::repository.end()) 
  {
    //- can't get ownership if _exclusive_access requested
    if (_exclusive_access || it->second->exclusive_access_) 
    {
      //- can't get exclusive access to hardware
      throw DAQException("hardware already in use",
                              "could not get exclusive access to the hardware",
                              "NI5620::instanciate");
    }
    //- share the board
    else 
    {
      return it->second->duplicate();
    }
  }

  //- instanciate the digitizer
  NI5620* d = new NI5620(_id);
  if (d == 0) 
  {
    //- out of memory
    throw DAQException("out of memory",
                            "out of memory error",
                            "NI5620::instanciate");
  }

  //- update board's <exclusive_access> flag
  d->exclusive_access_ = _exclusive_access;

  //- open driver session
  try 
  {
    d->register_hardware();
  }
  catch (const DAQException&)
  {
    //- release digitizer
    d->release();
    throw;
  }
  catch (...)
  {
    //- release digitizer
    d->release();
    //- hardware registration failed
    throw DAQException("hardware registration failed",
                            "unknown exception caught while trying to register the board",
                            "NI5620::instanciate");
  }

  return d;
}

// ============================================================================
// NI5620::duplicate
// ============================================================================
NI5620 * NI5620::duplicate ()
{
  return dynamic_cast<NI5620*>(this->SharedObject::duplicate()); 
}
 
// ============================================================================
// NI5620::NI5620
// ============================================================================
NI5620::NI5620(unsigned short _id) 
  : NIDigitizer(dsl::NI_5620, _id)
{
  //- std::cout << "NI5620::NI5620 <-" << std::endl;

  //- store digitizer into the repository
  RepositoryValue pair(_id, this);
  NI5620::repository.insert(pair);

  //- std::cout << "NI5620::NI5620 ->" << std::endl;
}

// ============================================================================
// NI5620::~NI5620
// ============================================================================
NI5620::~NI5620()
{
  //- lock
  yat::AutoMutex<> guard(NI5620::repository_lock);
										 
  //- std::cout << "NI5620::~NI5620 <-" << std::endl;

  //- release hardware
  this->release_hardware();

  //- remove board from repository
  RepositoryIterator it = NI5620::repository.find(this->id_);
  if (it != NI5620::repository.end()) 
  {
    NI5620::repository.erase(it);    
  }

   //- std::cout << "NI5620::~NI5620 <-" << std::endl;
}

// ============================================================================
// NI5620::clock_frequency
// ============================================================================
double NI5620::max_sampling_rate () const
{
  return (double)NI_5620_MAX_SAMPLING_RATE;
}

// ============================================================================
// Returns the numbers of channels of the board
// ============================================================================
unsigned short NI5620::num_channels () const
{
	return NI_5620_NUM_CHANNELS;
}

// ============================================================================
// Return the input impedance of the board
// ============================================================================
double NI5620::input_impedance () const
{
	return (double)NI_5620_INPUT_IMPEDANCE;
}

// ============================================================================
// Configures the digitizer
// ============================================================================
void NI5620::configure_continuous_ai (const dsl::ContinuousAIConfig& config)
{
  this->NIDigitizer::configure_continuous_ai(config);

  //- enable both DDC and DITHER
  ViStatus err = ::niScope_SetAttributeViBoolean( this->hh_,
										                              VI_NULL,
										                              NISCOPE_ATTR_DDC_ENABLE,
										                              VI_TRUE);
  if (err)
  { 
    throw DAQException(static_cast<const char*>("the DDC could not be enabled  [niScope_SetAttributeViBoolean failed]"),
                            dsl::niscope_error_text (this->hh_, err),
						                static_cast<const char*>("NI5620::configure_continuous_ai"));
  }

  err = ::niScope_SetAttributeViBoolean(this->hh_, 
										                    VI_NULL, 
                                        NISCOPE_ATTR_ENABLE_DITHER,
                                        VI_TRUE);
  if (err)
  { 
    throw DAQException(static_cast<const char*>("the analog DITHER could not be enabled  [niScope_SetAttributeViBoolean failed]"),
                            dsl::niscope_error_text (this->hh_, err),
						                static_cast<const char*>("NI5620::configure_continuous_ai"));
  }
}

// ============================================================================
// NI5620::dump
// ============================================================================
void NI5620::dump () const
{
	//- std::cout << ". manufacturer.......National Instruments" << std::endl;
	//- std::cout << ". type...............5620" << std::endl;
	//- std::cout << ". board id..........." << this->id_ << std::endl;
	//- std::cout << ". hardware handle...." << this->hh_ << std::endl;
	//- std::cout << ". exclusive access..." << (this->exclusive_access_ ? "yes" : "no") << std::endl;
}

} // namespace dsl

#endif // _NI_SUPPORT_ && _DOWN_CONVERTER_SUPPORT_

// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    NI5600.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h> 

#if defined(_DOWN_CONVERTER_SUPPORT_) && defined(_NI_SUPPORT_)

#include <iostream>
#include <dsl/Rsrc.h>
#include <dsl/ni/NI5600.h>

namespace dsl { 

// ============================================================================
// STATICS
// ============================================================================
yat::Mutex NI5600::repository_lock;
//-----------------------------------------------------------------------------
NI5600::Repository NI5600::repository;
//-----------------------------------------------------------------------------

// ============================================================================
// NI5600::instanciate
// ============================================================================
NI5600 * NI5600::instanciate (unsigned short _id, bool _exclusive_access)
  throw (DAQException)
{
  //- lock
  yat::AutoMutex<> guard(NI5600::repository_lock);

  //- search requested board in the repository
  RepositoryIterator it = NI5600::repository.find(_id);

  //- already instanciated?
  if (it != NI5600::repository.end()) 
  {
    //- can't get ownership if _exclusive_access requested
    if (_exclusive_access || it->second->exclusive_access_) 
    {
      //- can't get exclusive access to hardware
      throw DAQException("hardware already in use",
                              "could not get exclusive access to the hardware",
                              "NI5600::instanciate");
    }
    //- share the board
    else 
    {
      return it->second->duplicate();
    }
  }

  //- instanciate the digitizer
  NI5600* d = new NI5600(_id);
  if (d == 0) 
  {
    //- out of memory
    throw DAQException("out of memory",
                            "out of memory error",
                            "NI5600::instanciate");
  }

  //- update board's <exclusive_access> flag
  d->exclusive_access_ = _exclusive_access;

  //- open driver session
  try 
  {
    d->register_hardware();
  }
  catch (const DAQException&)
  {
    //- release digitizer
    d->release();
    throw;
  }
  catch (...)
  {
    //- release digitizer
    d->release();
    //- hardware registration failed
    throw DAQException("hardware registration failed",
                            "unknown exception caught while trying to register the board",
                            "NI5600::instanciate");
  }

  return d;
}

// ============================================================================
// NI5600::duplicate
// ============================================================================
NI5600 * NI5600::duplicate ()
{
  return dynamic_cast<NI5600*>(this->SharedObject::duplicate()); 
}
 
// ============================================================================
// NI5600::NI5600
// ============================================================================
NI5600::NI5600(unsigned short _id) 
  : NIDownConverter(dsl::NI_5600, _id)
{
  //- std::cout << "NI5600::NI5600 <-" << std::endl;

  //- store digitizer into the repository
  RepositoryValue pair(_id, this);
  NI5600::repository.insert(pair);

  //- std::cout << "NI5600::NI5600 ->" << std::endl;
}

// ============================================================================
// NI5600::~NI5600
// ============================================================================
NI5600::~NI5600()
{
  //- lock
  yat::AutoMutex<> guard(NI5600::repository_lock);
										 
  //- std::cout << "NI5600::~NI5600 <-" << std::endl;

  //- release hardware
  this->release_hardware();

  //- remove board from repository
  RepositoryIterator it = NI5600::repository.find(this->id_);
  if (it != NI5600::repository.end()) 
  {
    NI5600::repository.erase(it);    
  }

  //- std::cout << "NI5600::~NI5600 <-" << std::endl;
}

// ============================================================================
// Returns the numbers of channels of the board
// ============================================================================
unsigned short NI5600::num_channels () const
{
	return NI_5600_NUM_CHANNELS;
}

// ============================================================================
// NI5600::dump
// ============================================================================
void NI5600::dump () const
{
	//- std::cout << ". manufacturer.......National Instruments" << std::endl;
	//- std::cout << ". type...............5600 [downconverter]" << std::endl;
	//- std::cout << ". board id..........." << this->id_ << std::endl;
	//- std::cout << ". hardware handle...." << this->task_id_ << std::endl;
	//- std::cout << ". exclusive access..." << this->exclusive_access_ ? "yes" : "no") << std::endl;
}

} // namespace dsl

#endif // _NI_SUPPORT_ && _DOWN_CONVERTER_SUPPORT_

// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed NIDownConverter Support Library
//
// = FILENAME
//    NIDownConverter.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h> 

#if defined(_DOWN_CONVERTER_SUPPORT_) && defined(_NI_SUPPORT_)

#include <iostream>
#include <dsl/ni/NI5600.h>

// ============================================================================
// CONSTs
// ============================================================================
#define RSRC_NAME_PREFIX static_cast<const char*>("Dev")

namespace dsl {

// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define CD_HW_SUPPORT static_cast<const char*>("current impl. supports the following NI boards: 5600")
 
// ============================================================================
// NIDownConverter::instanciate
// ============================================================================
DownConverter * NIDownConverter::instanciate (unsigned short _type, 
                                              unsigned short _id,
                                              bool _exclusive_access)
  throw (DAQException)
{
  NIDownConverter * d = 0;

  //- type of board
  switch (_type)  
  {
    //- NI-5600
    case dsl::NI_5600:
      d = NI5600::instanciate (_id, _exclusive_access);
      break;
    
    //- unsupported hardware
    default:
      throw DAQException(static_cast<const char*>("unsupported or invalid hardware type"),
                              CD_HW_SUPPORT,
                              static_cast<const char*>("NIDownConverter::instanciate"));
      break;
  }

  return d;
}

// ============================================================================
// NIDownConverter::NIDownConverter
// ============================================================================
NIDownConverter::NIDownConverter (unsigned short _type, 
                                  unsigned short _id) 
  : DownConverter(_type, _id), 
    task_id_(kUNDEFINED_TID)
{
  //- std::cout << "NIDownConverter::NIDownConverter <-" << std::endl;
  //- std::cout << "NIDownConverter::NIDownConverter ->" << std::endl;
}

// ============================================================================
// NIDownConverter::~NIDownConverter
// ============================================================================
NIDownConverter::~NIDownConverter ()
{
  //- std::cout << "NIDownConverter::~NIDownConverter <-" << std::endl;
  //- std::cout << "NIDownConverter::~NIDownConverter ->" << std::endl;
}

// ============================================================================
// NIDownConverter::register_hardware
// ============================================================================
int NIDownConverter::register_hardware ()
{

#if !defined(_SIMULATION_) 

  //- initialize
  int err = ::niTuner_init(this->id_, &this->task_id_);
  
  if (err)
  { 
    throw DAQException(static_cast<const char*>("initialization failed [niTuner_init failed]"),
                            static_cast<const char*>("board registration failed [niTuner_init gave error]"),
		 		                    static_cast<const char*>("NIDownConverter::register_hardware"));
  }
  
  return 0;

#else // _SIMULATION_
	
	return 0;
	
#endif // _SIMULATION_  
}

// ============================================================================
// NIDownConverter::release_hardware
// ============================================================================
int NIDownConverter::release_hardware ()
{
	//- be sure the digitizer is registered
	if (this->task_id_ == kUNDEFINED_TID) 
  {
	  return 0;
	}

#if !defined(_SIMULATION_) 

	//- actual unregistering - niscope close...
  int err = ::niTuner_close(this->task_id_);

  if (err)
  { 
    throw DAQException(static_cast<const char*>("closing failed [niInit_close failed]"),
                            dsl::nituner_error_text(this->task_id_, err),
		 		                    static_cast<const char*>("NIDownConverter::release_hardware"));
  }

  return 0;
  
#else // _SIMULATION_
	
	return 0;
	
#endif // _SIMULATION_ 
}

// ============================================================================
// NIDownConverter::check_registration
// ============================================================================
void NIDownConverter::check_registration () const throw (DAQException) 
{
  if (this->task_id_ ==  kUNDEFINED_TID) 
  {
	  throw DAQException(static_cast<const char*>("invalid registration identifier"),
							              static_cast<const char*>("board initialization failed"),
							              static_cast<const char*>("NIDownConverter::check_registration"));
  }
}

// ============================================================================
// NIDownConverter::set_attenuation
// ============================================================================
void NIDownConverter::set_attenuation (const Attenuation& a) throw (DAQException)
{
  if (a.reference_level < 0)
    this->att_.reference_level = 0;
  else if (a.reference_level > 50)
    this->att_.reference_level = 50;

  if (a.mixer_level > 0)
    this->att_.mixer_level = 0;
  else if (a.mixer_level > 1000)
    this->att_.mixer_level = 1001;
  
  if ((this->att_.mixer_level != 1001) && (a.mixer_level < (this->att_.reference_level - 50)))
    this->att_.mixer_level = this->att_.reference_level - 50;

  int err = ::niTuner_setAttenuation(this->task_id_, 
                                     this->att_.reference_level,
                                     this->att_.mixer_level,
                                     &this->att_.actual_attenuation,
                                     &this->att_.scale_factor);
  if (err)
  { 
    throw DAQException(static_cast<const char*>("could not set attenuation [niTuner_setAttenuation failed]"),
                            dsl::nituner_error_text(this->task_id_, err),
		 		                    "NIDownConverter::set_attenuation");
  }
}

// ============================================================================
// NIDownConverter::set_tuning
// ============================================================================
void NIDownConverter::set_tuning (const Tuning& t) throw (DAQException)
{
  ::memset(&this->tuning_, 0, sizeof(Tuning));
  this->tuning_.desired_rf_frequency = t.desired_rf_frequency;
  this->tuning_.span = t.span;

  int err = ::niTuner_setFreq(this->task_id_, 
                              this->tuning_.desired_rf_frequency,
                              this->tuning_.span,
                              &this->tuning_.actual_if_frequency,
                              &this->tuning_.actual_rf_tuned_frequency,
                              &this->tuning_.frequency_shift);

  if (err)
  { 
    throw DAQException(static_cast<const char*>("could not tune the downconverter [niTuner_setFreq failed]"),
                            dsl::nituner_error_text(this->task_id_, err),
		 		                    static_cast<const char*>("NIDownConverter::set_tuning"));
  }

  err = ::niTuner_sendSoftwareTrigger (this->task_id_);
  if (err)
  { 
    throw DAQException(static_cast<const char*>("could not trigger the downconverter [niTuner_sendSoftwareTrigger failed]"),
                            dsl::nituner_error_text(this->task_id_, err),
		 		                    static_cast<const char*>("NIDownConverter::set_tuning"));
  }

  //- std::cout << "IF::" << this->tuning_.actual_if_frequency << std::endl;
  //- std::cout << "Actual RF tuned freq::" << this->tuning_.actual_rf_tuned_frequency << std::endl;
  //- std::cout << "Frequency shift::" << this->tuning_.frequency_shift << std::endl;
}


// ============================================================================
// NIDownConverter::set_reference_clock
// ============================================================================
void NIDownConverter::set_reference_clock (DCReferenceClock rc) throw (DAQException)
{
  unsigned int ni_rc;

  switch (rc)
  {
    case dc_internal_ref_clock:
      ni_rc = NITUNER_REF_INTERNAL;
      break;
    case dc_external_ref_clock:
      ni_rc = NITUNER_REF_EXTERNAL;
      break;
    default:
      throw DAQException(static_cast<const char*>("invalid parameter"),
                              static_cast<const char*>("invalid clock source specified"),
		 		                      static_cast<const char*>("NIDownConverter::set_tuning"));
  }

  int err = ::niTuner_configReferenceClock(this->task_id_, ni_rc);

  if (err)
  { 
    throw DAQException(static_cast<const char*>("could not set the reference clock [niTuner_configReferenceClock failed]"),
                            dsl::nituner_error_text(this->task_id_, err),
		 		                    static_cast<const char*>("NIDownConverter::set_reference_clock"));
  }
}

} // namespace dsl

#endif // _NI_SUPPORT_ && _DOWN_CONVERTER_SUPPORT_

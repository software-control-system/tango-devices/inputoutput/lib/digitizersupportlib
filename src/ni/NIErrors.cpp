// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    NIError.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h> 

#if defined(_NI_SUPPORT_)

#include <dsl/ni/NIRsrc.h>

// ============================================================================ 
// error code to error text
// ============================================================================ 
#if ! defined(_SIMULATION_)
static char lg_niscope_err_txt[IVI_MAX_MESSAGE_BUF_SIZE];
static char lg_nituner_err_txt[MAX_ERROR_DESCRIPTION];
#endif

namespace dsl {

// ============================================================================
// ERROR HANDLING: error code to error text 
// ============================================================================ 
const char* niscope_error_text (dsl::HardwareHandle _hh, int _err_code)
{
#if ! defined(_SIMULATION_)
 ::niScope_error_message(_hh, _err_code, lg_niscope_err_txt);
 return static_cast<const char*>(lg_niscope_err_txt);
#else
 return "no error";
#endif
}

#ifdef DOWN_CONVERTER_SUPPORT
// ============================================================================ 
// ERROR HANDLING: error code to error text 
// ============================================================================ 
const char* nituner_error_text (int _tid, int _err_code)
{
#if ! defined(_SIMULATION_)
 ::niTuner_errorHandler (_tid, _err_code, 0, lg_nituner_err_txt);
 return static_cast<const char*>(lg_nituner_err_txt);
#else
 return "no error";
#endif
}
#endif //- DOWN_CONVERTER_SUPPORT

} // namespace dsl

#endif //- _NI_SUPPORT_

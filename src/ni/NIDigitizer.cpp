// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed NIDigitizer Support Library
//
// = FILENAME
//    NIDigitizer.cpp
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h> 

#if defined(_NI_SUPPORT_)

#include <iostream>
#include <yat/threading/Thread.h>
#include <dsl/ni/NI5122.h>
#include <dsl/ni/NI5620.h>

// ============================================================================
// CONSTs
// ============================================================================
#define DEV_NAME_PREFIX                   static_cast<const char*>("Dev")
#define DAQ_NAME_PREFIX                   static_cast<const char*>("DAQ::")
#define DEFAULT_CHANNEL_PROB_ATTENUATION  1.0
#define DEFAULT_TRIG_HOLDOFF              0.0
#define ENABLE_CHANNEL                    true
#define DEFAULT_CHANNEL_OFFSET            0.0
#define REF_POSITION                      0.0
#define NUM_RECORDS                       1
#define REF_CLOCK_FQ                      10000000
#define EMPTY_STRING                      ""
#define TIMEOUT_EXCEEDED                  0xbffa2003

// ============================================================================
// WORKAROUND FOR STRANGE BEHAVIOUR !!!
// ============================================================================
#define MAX_SUPPORTED_NUM_CHANNELS 8

#if ! defined(_SIMULATION_)
static struct niScope_wfmInfo g_ni_wfm_info[MAX_SUPPORTED_NUM_CHANNELS];
#endif

namespace dsl {
 
// ============================================================================
// SUPPORTED HARDWARE
// ============================================================================
#define SUPPORTED_HARDWARE static_cast<const char*>("current DSL impl. supports the following NI boards: 5122, 5620")

// ============================================================================
// NIDigitizer::instanciate
// ============================================================================
Digitizer * NIDigitizer::instanciate (unsigned short _type, 
                                      unsigned short _id,
                                      bool _exclusive_access)
  throw (DAQException)
{
  NIDigitizer * d = 0;

  //- type of board
  switch (_type)  
  {
    //- NI-5122
    case dsl::NI_5122:
      d = NI5122::instanciate (_id, _exclusive_access);
      break;
#if defined(DOWN_CONVERTER_SUPPORT)
    //- NI-5620
    case dsl::NI_5620:
      d = NI5620::instanciate (_id, _exclusive_access);
      break;
#endif
    //- unsupported hardware
    default:
      throw DAQException(static_cast<const char*>("unsupported or invalid hardware type"),
                              SUPPORTED_HARDWARE,
                              static_cast<const char*>("NIDigitizer::instanciate"));
      break;
  }

  return d;
}

// ============================================================================
// NIDigitizer::NIDigitizer
// ============================================================================
NIDigitizer::NIDigitizer (unsigned short _type, 
                          unsigned short _id) 
  : Digitizer(_type, _id), daq_buffer_depth_ (0)
{
  //- std::cout << "NIDigitizer::NIDigitizer <-" << std::endl;
  //- std::cout << "NIDigitizer::NIDigitizer ->" << std::endl;
}

// ============================================================================
// NIDigitizer::~NIDigitizer
// ============================================================================
NIDigitizer::~NIDigitizer ()
{
  //- std::cout << "NIDigitizer::~NIDigitizer <-" << std::endl;
  //- std::cout << "NIDigitizer::~NIDigitizer ->" << std::endl;
}

// ============================================================================
// NIDigitizer::register_hardware
// ============================================================================
int NIDigitizer::register_hardware ()
{

#if !defined(_SIMULATION_) 

  int err;

  //- build resource name
  std::string rname(DAQ_NAME_PREFIX);

  //- add <this->id_> to the string (should use std C++ streams)
  char id_str[3] = {0,0,0};
  ::sprintf(id_str, "%d", this->id_);

  //- concat
  rname += std::string(id_str);

  //- try to initialize with <DAQ::id> as device name
  //- std::cout << "NIDigitizer::register_hardware::try with rsrc name: " << rname << std::endl;
  //- try initialize with <Dev+id> as device name
  err = ::niScope_init(const_cast<char*>(rname.c_str()), false, true, &this->hh_);
  if (err)
  { 
    //- std::cout << "NIDigitizer::register_hardware::initialzation failed with rsrc name: " << rname << std::endl;
    //- retry to initialize with <id> as device name
    rname = DEV_NAME_PREFIX + std::string(id_str);
    //- std::cout << "NIDigitizer::register_hardware::try with rsrc name: " << rname << std::endl;
    //- retry 
    err = ::niScope_init(const_cast<char*>(rname.c_str()), false, true, &this->hh_);
    if (err)
    { 
      //- std::cout << "NIDigitizer::register_hardware::initialzation failed with rsrc name: " << rname << std::endl;
      //- retry to initialize with <id> as device name
      rname = std::string(id_str);
      //- std::cout << "NIDigitizer::register_hardware::try with rsrc name: " << rname << std::endl;
      //- retry 
      err = ::niScope_init(const_cast<char*>(rname.c_str()), false, true, &this->hh_);
      if (err)
      { 
        //- std::cout << "NIDigitizer::register_hardware::initialzation failed with rsrc name: " << rname << std::endl;
        throw DAQException(static_cast<const char*>("initialization failed [niScope_init failed]"),
                                static_cast<const char*>("board registration failed [niScope_init gave error]"),
		 		                        static_cast<const char*>("NIDigitizer::register_hardware"));
      }
    }
  }

  return 0;

#else // _SIMULATION_
	
	return 0;
	
#endif // _SIMULATION_  
}

// ============================================================================
// NIDigitizer::release_hardware
// ============================================================================
int NIDigitizer::release_hardware ()
{
	//- be sure the digitizer is registered
	if (this->hh_ == kUNDEFINED_HH) 
  {
	  return 0;
	}

#if !defined(_SIMULATION_) 

  int err;

	//- actual unregistering - niscope close...
  err = ::niScope_close(this->hh_);

  if (err)
  { 
    throw DAQException(static_cast<const char*>("closing failed [niScope_close failed]"),
                            dsl::niscope_error_text (this->hh_, err),
		 		                    static_cast<const char*>("NIDigitizer::release_hardware"));
  }

  return 0;
  
#else // _SIMULATION_
	
	return 0;
	
#endif // _SIMULATION_ 
} 

// ============================================================================
// NIDigitizer::configure_continuous_ai
// ============================================================================
void NIDigitizer::configure_continuous_ai (const dsl::ContinuousAIConfig& _ai_config)
    throw (DAQException)
{
  //- store config locally
  this->ai_config_ = _ai_config;

#if !defined(_SIMULATION_) 

  int err;

  //- check registration id (may throw an exception)
  this->check_registration();

  //- CHANNELS CONFIGURATION -------------------------------------------------
  //--------------------------------------------------------------------------

  ViReal64  impedance;
  ViReal64  bandwidth;
  ViInt32   coupling;
  char      chan_id[2] = {0, 0};

  //- get active channels
  const ActiveAIChannels chans = this->ai_config_.get_active_channels();
  
  for (size_t i = 0; i < chans.size(); i++)
  {
    //- impedance
    impedance = (chans[i].impedance == impedance_50Ohms) 
		          ? NISCOPE_VAL_50_OHMS 
		          : NISCOPE_VAL_1_MEG_OHM;

	  //- bandwidth
	  switch (chans[i].bandwidth)
	  {
	    case bandwidth_20MHz:
		    bandwidth = NISCOPE_VAL_20MHZ_BANDWIDTH;
	      break;
	    case bandwidth_100MHz:
		    bandwidth = NISCOPE_VAL_100MHZ_BANDWIDTH;
	      break;
	    default:
		    bandwidth = NISCOPE_VAL_FULL_BANDWIDTH;
	      break;
    }

    //- build chan id string
    ::memset(chan_id, 0,  2 * sizeof(char));
    ::sprintf(chan_id, "%d", i);

    //- configure channel
    err = ::niScope_ConfigureChanCharacteristics(this->hh_,
												                         chan_id,
												                         impedance,
											                           bandwidth);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("channel configuration failed  [niScope_ConfigureChanCharacteristics failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }

    //- coupling
    coupling = (chans[i].coupling == ac_coupling) 
		         ? NISCOPE_VAL_AC 
		         : NISCOPE_VAL_DC;

    //- configure channel
    err = ::niScope_ConfigureVertical(this->hh_,
												              chan_id,
												              chans[i].range,
												              chans[i].offset, 
                                      coupling, 
                                      DEFAULT_CHANNEL_PROB_ATTENUATION, 
                                      ENABLE_CHANNEL);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("channel configuration failed [niScope_ConfigureVertical failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }
  } //- for each active channel 


  //- BOARD CONFIGURATION ----------------------------------------------------
  //--------------------------------------------------------------------------

  //- horizontal configuration -----------------------------------------------
  //--------------------------------------------------------------------------

  ViReal64  min_sample_rate = this->ai_config_.get_sampling_rate();
  ViInt32   min_num_pts = this->ai_config_.get_buffer_depth();
  ViReal64  ref_position = REF_POSITION;
  ViInt32   num_records = NUM_RECORDS;
  ViBoolean enforce_realtime = true;

  err = ::niScope_ConfigureHorizontalTiming(this->hh_,
                                            min_sample_rate,
                                            min_num_pts,
                                            ref_position,
                                            num_records,
                                            enforce_realtime);
  if (err)
  { 
    throw DAQException(static_cast<const char*>("horizontal configuration failed [niScope_ConfigureHorizontalTiming failed]"),
                            dsl::niscope_error_text (this->hh_, err),
						                static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
  }


  //- trigger configuration --------------------------------------------------
  //--------------------------------------------------------------------------
  if (this->ai_config_.get_trigger_source() != dsl::trigger_internal_src)
  {

    //- external digital trigger config
    ViConstString atrig_source;
    switch (this->ai_config_.get_trigger_source())
    {
      case trigger_digital_external_src:
          throw DAQException(static_cast<const char*>("trigger configuration failed [invalid source specified]"),
                                  static_cast<const char*>("use PFI[0..2] for digital external trigger"),
						                      static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
        break;
      case trigger_digital_pfi0_src:
        atrig_source = NISCOPE_VAL_PFI_0;
        break;
      case trigger_digital_pfi1_src:
        atrig_source = NISCOPE_VAL_PFI_1;
        break;
      case trigger_digital_pfi2_src:
        atrig_source = NISCOPE_VAL_PFI_2;
        break;
      case trigger_analog_chan0_src:
        atrig_source = "0";
        break;
      case trigger_analog_chan1_src:
        atrig_source = "1";
        break;
      case trigger_analog_external_src:
        atrig_source = NISCOPE_VAL_EXTERNAL;
        break;
      default:
        atrig_source = NISCOPE_VAL_IMMEDIATE;
        break;
    }

    ViInt32 atrig_slope;
    switch (this->ai_config_.get_trigger_slope())
    {
      case trigger_slope_positive:
        atrig_slope = NISCOPE_VAL_POSITIVE;
        break;
      case trigger_slope_negative:
        atrig_slope = NISCOPE_VAL_NEGATIVE;
        break;
      default:
        atrig_slope = NISCOPE_VAL_POSITIVE;
        break;
    }

    ViInt32 atrig_coupling;
    switch (this->ai_config_.get_trigger_coupling())
    {
      case trigger_coupling_dc:
        atrig_coupling = NISCOPE_VAL_DC;
        break;
      case trigger_coupling_ac:
        atrig_coupling = NISCOPE_VAL_AC;
        break;
      case trigger_coupling_dc_50Ohms:
        atrig_coupling = NISCOPE_VAL_DC;
        break;
      case trigger_coupling_ac_50Ohms:
        atrig_coupling = NISCOPE_VAL_AC;
        break;
      default:
        atrig_coupling = NISCOPE_VAL_DC;
        break;
    }

    switch (this->ai_config_.get_trigger_source())
    {
      case trigger_digital_pfi0_src:
      case trigger_digital_pfi1_src:
      case trigger_digital_pfi2_src:
        err = ::niScope_ConfigureTriggerDigital(this->hh_,
                                                atrig_source,
                                                atrig_slope,
                                                DEFAULT_TRIG_HOLDOFF,
                                                this->ai_config_.get_trigger_delay());
        if (err)
        { 
          throw DAQException(static_cast<const char*>("trigger configuration failed [niScope_ConfigureTriggerDigital failed]"),
                                  dsl::niscope_error_text (this->hh_, err),
						                      static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
        }
        break;
      case trigger_analog_chan0_src:
      case trigger_analog_chan1_src:
      case trigger_analog_external_src:
        err = ::niScope_ConfigureTriggerEdge(this->hh_,
                                             atrig_source,
                                             this->ai_config_.get_trigger_level(),
                                             atrig_slope,
                                             atrig_coupling,
                                             DEFAULT_TRIG_HOLDOFF,
                                             this->ai_config_.get_trigger_delay());
        if (err)
        { 
          throw DAQException(static_cast<const char*>("trigger configuration failed [niScope_ConfigureTriggerEdge failed]"),
                                  dsl::niscope_error_text (this->hh_, err),
						                      static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
        }
        break;
    }
  }
  else
  {
    //- internal software trigger config
    err = ::niScope_ConfigureTriggerImmediate(this->hh_);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("trigger configuration failed [niScope_ConfigureTriggerImmediate failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }
  }


  //- 10MHz external reference clock configuration ---------------------------
  //--------------------------------------------------------------------------
  if (this->ai_config_.external_reference_clock_enabled())
  {
    err = ::niScope_ConfigureClock(this->hh_,
                                   NISCOPE_VAL_EXTERNAL,
                                   NISCOPE_VAL_NO_SOURCE,
                                   NISCOPE_VAL_NO_SOURCE,
                                   VI_TRUE);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("10MHz external reference clock configuration failed [niScope_ConfigureClock failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }
  }

  //- external sampling clock configuration ----------------------------------
  //--------------------------------------------------------------------------

  if (this->ai_config_.get_sampling_rate_source() == dsl::external_sampling_rate_source)
  {
    ViInt32 timebaseDiv = 1;

    // Sampling clock configuration
    err = niScope_SetAttributeViString(this->hh_,
                                       VI_NULL,
                                       NISCOPE_ATTR_SAMP_CLK_TIMEBASE_SRC,
                                       NISCOPE_VAL_CLK_IN);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("external sampling clock configuration failed [NISCOPE_ATTR_SAMP_CLK_TIMEBASE_SRC failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }

    err = niScope_SetAttributeViReal64(this->hh_, 
                                       VI_NULL, 
                                       NISCOPE_ATTR_SAMP_CLK_TIMEBASE_RATE,
                                       min_sample_rate);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("external sampling clock configuration failed [NISCOPE_ATTR_SAMP_CLK_TIMEBASE_RATE failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }

    err = niScope_SetAttributeViInt32(this->hh_, 
                                      VI_NULL,
                                      NISCOPE_ATTR_SAMP_CLK_TIMEBASE_DIV,
                                      timebaseDiv);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("external sampling clock configuration failed [NISCOPE_ATTR_SAMP_CLK_TIMEBASE_DIV failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }

    // Sampling clock output activation
    err = niScope_SetAttributeViString(this->hh_,
                                       VI_NULL,
                                       NISCOPE_ATTR_EXPORT_SAMP_CLK_OUTPUT_TERM,
                                       NISCOPE_VAL_CLK_OUT);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("sampling clock output activation failed [NISCOPE_ATTR_EXPORT_SAMP_CLK_OUTPUT_TERM failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }

  }

  //- build active channels string
  this->chan_id_.str("");
  for (size_t i = 0; i < chans.size(); i++)
  {
    this->chan_id_ << chans[i].id;
    if (i < chans.size() - 1)
      this->chan_id_ << ",";
  }
  this->chan_id_ << std::ends;

  //- get actual num of waveforms 
  ViInt32 num_wfms;
  err = ::niScope_ActualNumWfms(this->hh_, this->chan_id_.str().c_str(), &num_wfms);
  if (err)
  { 
    throw DAQException(static_cast<const char*>("DAQ buffer setup failed [niScope_ActualNumWfms failed]"),
                            dsl::niscope_error_text (this->hh_, err),
						                static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
  }

  //- add processing step (if enabled) and compute actual buffer depth
  if (this->ai_config_.fft_mode() != dsl::fft_none)
  {
    //- add processing
    ViInt32 measurement_type;
    switch (this->ai_config_.fft_mode())
    {
      case dsl::fft_amplitude_spectrum_db:
        measurement_type = NISCOPE_VAL_FFT_AMP_SPECTRUM_DB;
        break;
      case dsl::fft_phase_spectrum:
        measurement_type = NISCOPE_VAL_FFT_PHASE_SPECTRUM;
        break;
      case dsl::fft_amplitude_spectrum_volts_rms:
        measurement_type = NISCOPE_VAL_FFT_AMP_SPECTRUM_VOLTS_RMS;
        break;
    }

    err = ::niScope_AddWaveformProcessing(this->hh_,
                                          this->chan_id_.str().c_str(),
                                          measurement_type);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("signal processing configuration failed [niScope_AddWaveformProcessing failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }
    //- can't compute DAQ buffer depth here. see get_data
    this->daq_buffer_depth_ = 0;
  }
  //- no signal processing
  else
  {
    //- clear any previously configured signal processing
    err = ::niScope_ClearWaveformProcessing (this->hh_,
                                             this->chan_id_.str().c_str());
    if (err)
    { 
      throw DAQException(static_cast<const char*>("signal processing configuration failed [niScope_ClearWaveformProcessing failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }
    //- just compute actual buffer depth
    ViInt32 actual_record_len;
    err = ::niScope_ActualRecordLength(this->hh_, &actual_record_len);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("DAQ buffer setup failed [niScope_ActualRecordLength failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }
    //- store actual buffer depth locally
    this->daq_buffer_depth_ = static_cast<unsigned long>(actual_record_len * num_wfms);
  }


#endif // _SIMULATION_ 
}

// ============================================================================
// NIDigitizer::start_continuous_ai
// ============================================================================
void NIDigitizer::start_continuous_ai ()
    throw (DAQException)
{
  //- std::cout << "NIDigitizer::start_continuous_ai <-" << std::endl;

  //- check registration id (may throw an exception)
  this->check_registration();

#if ! defined(_SIMULATION_)

  //- check PLL lock status
  if (this->ai_config_.fft_mode() != dsl::fft_none)
  {
    //- std::cout << "NIDigitizer::start_continuous_ai::FFT enabled" << std::endl; 
    ViBoolean locked = VI_FALSE;
    for (unsigned int i = 0; ;i++)
    {

      ViStatus err = ::niScope_GetAttributeViBoolean(this->hh_,
										                                 VI_NULL,
										                                 NISCOPE_ATTR_PLL_LOCK_STATUS,
										                                 &locked);
      if (err)
      { 
        throw DAQException(static_cast<const char*>("could not obtain PLL lock status [niScope_GetAttributeViBoolean failed]"),
                                dsl::niscope_error_text (this->hh_, err),
						                    static_cast<const char*>("NIDigitizer::start_continuous_ai"));
      }
      if (locked || (i > 7)) break;
      yat::Thread::sleep(1);
    }

    if (locked == VI_FALSE)
    { 
      throw DAQException(static_cast<const char*>("PLL is not locked"),
                              static_cast<const char*>("PLL is not locked after 7 seconds"),
						                  static_cast<const char*>("NIDigitizer::start_continuous_ai"));
    }
    else
    {
      //- std::cout << "NIDigitizer::start_continuous_ai::PLL locked" << std::endl;
    }
  }
  
#endif // _SIMULATION_

  //- std::cout << "NIDigitizer::start_continuous_ai ->" << std::endl;
}

// ============================================================================
// NIDigitizer::stop_continuous_ai
// ============================================================================
void NIDigitizer::stop_continuous_ai ()
    throw (DAQException)
{
  //- std::cout << "NIDigitizer::stop_continuous_ai <-" << std::endl;
  
#if !defined(_SIMULATION_)

  int err;

  //- check registration id (may throw an exception)
  this->check_registration();

  err = ::niScope_Abort(this->hh_);

  if (err)
  { 
    throw DAQException(static_cast<const char*>("abort failed [niScope_Abort failed]"),
                            dsl::niscope_error_text (this->hh_, err),
						                static_cast<const char*>("NIDigitizer::stop_continuous_ai"));
  }

#endif // _SIMULATION_

  //- std::cout << "NIDigitizer::stop_continuous_ai ->" << std::endl;
}

// ============================================================================
// NIDigitizer::get_data
// ============================================================================
dsl::AIScaledData * NIDigitizer::get_data (unsigned long timeout_ms) 
    throw (DAQException)
{
  //- std::cout << "NIDigitizer::get_data <-" << std::endl;
  dsl::AIScaledData * data = this->get_data_i(timeout_ms);
  //- std::cout << "NIDigitizer::get_data ->" << std::endl;
  return data; 
}

// ============================================================================
// NIDigitizer::get_waveforms
// ============================================================================
dsl::Waveforms * NIDigitizer::get_waveforms (size_t dummy_num_wfms,unsigned long timeout_ms) 
    throw (DAQException)
{
  //- std::cout << "NIDigitizer::get_waveforms <-" << std::endl;

#if ! defined(_SIMULATION_)

  ViInt32 num_wfms;
  ViStatus err = ::niScope_ActualNumWfms(this->hh_, this->chan_id_.str().c_str(), &num_wfms);
  if (err)
  { 
    throw DAQException(static_cast<const char*>("DAQ buffer setup failed [niScope_ActualNumWfms failed]"),
                            dsl::niscope_error_text (this->hh_, err),
						                static_cast<const char*>("NIDigitizer::get_waveforms"));
  }

  dsl::AIScaledData * wfms_data = this->get_data_i(timeout_ms);

  dsl::Waveforms * wfms = new dsl::Waveforms;
  if (wfms == 0)
  { 
    delete wfms_data;
    throw DAQException("OUT_OF_MEMORY",
                            "memory allocation failed",
						                "NIDigitizer::get_waveforms");
  }

  dsl::AIScaledData * wfm_ord = 0;
  dsl::AIScaledData * wfm_abs = 0;

  // In legacy version the calculation was 
  //   unsigned long wfm_len = wfms_data->depth() / (unsigned long)num_wfms;
  // but was no more compatible

  unsigned long wfm_len = wfms_data->length() / (unsigned long)num_wfms;

  if (num_wfms > 1)
  {
    for (ViInt32 i = 0; i < num_wfms; i++)
    {
      wfm_ord = DSL_NEW(dsl::AIScaledData(wfm_len));
      if (wfm_ord == 0)
      {
        delete wfms_data;
        delete wfms;
        throw DAQException("out of memory",
                                "memory allocation failed",
                                "NIDigitizer::get_waveforms");
      }

      ::memcpy(wfm_ord->base(), wfms_data->base() + i * wfm_len, wfm_len);

      dsl::Waveform * wfm = new dsl::Waveform;
      if (wfm == 0)
      { 
        delete wfm_ord;
        delete wfms_data;
        delete wfms;
        throw DAQException("OUT_OF_MEMORY",
                                "memory allocation failed",
						                    "NIDigitizer::get_waveforms");
      }

      wfm_abs = DSL_NEW(dsl::AIScaledData(wfm_len));
      if (wfm_abs == 0)
      {
        delete wfm;
        delete wfm_ord;
        delete wfms_data;
        delete wfms;
        throw DAQException("out of memory",
                                "memory allocation failed",
                                "NIDigitizer::get_waveforms");
      }

      for (unsigned long j = 0; j < wfm_len; j++)
      {
        *(wfm_abs->base() + j) =  g_ni_wfm_info[i].relativeInitialX + j * g_ni_wfm_info[i].xIncrement;
      }

      wfm->ord = wfm_ord;
      wfm->abs = wfm_abs;
      wfm->relative_ts = g_ni_wfm_info[i].relativeInitialX;
      wfm->absolute_ts = g_ni_wfm_info[i].absoluteInitialX;
      wfm->x_increment = g_ni_wfm_info[i].xIncrement;
      wfm->gain = g_ni_wfm_info[i].gain;
      wfm->offset = g_ni_wfm_info[i].offset;

      wfms->push_back(wfm);
    } 
    //- data obtained from get_data_i is no more needed...
    delete wfms_data;
  }
  else 
  {
    dsl::Waveform * wfm = new dsl::Waveform;
    if (wfm == 0)
    { 
      delete wfms_data;
      delete wfms;
      throw DAQException("OUT_OF_MEMORY",
                         "memory allocation failed",
						             "NIDigitizer::get_waveforms");
    }

    wfm_abs = DSL_NEW(dsl::AIScaledData(wfm_len));
    if (wfm_abs == 0)
    {
      delete wfm;
      delete wfms_data;
      delete wfms;
      throw DAQException("out of memory",
                         "memory allocation failed",
                         "NIDigitizer::get_waveforms");
    }

    for (unsigned long j = 0; j < wfm_len; j++)
    {
      *(wfm_abs->base() + j) =  g_ni_wfm_info[0].relativeInitialX + j * g_ni_wfm_info[0].xIncrement;
    }

    wfm->ord = wfms_data;    
    wfm->abs = wfm_abs;
    wfm->relative_ts = g_ni_wfm_info[0].relativeInitialX;
    wfm->absolute_ts = g_ni_wfm_info[0].absoluteInitialX;
    wfm->x_increment = g_ni_wfm_info[0].xIncrement;
    wfm->gain = g_ni_wfm_info[0].gain;
    wfm->offset = g_ni_wfm_info[0].offset;

    wfms->push_back(wfm);
  }
  
#else // _SIMULATION_
  
  if (! this->ai_config_.simulator())
  {
  	yat::Thread::sleep(1);
    return new dsl::Waveforms;
  }
  
  Yat::Timer t;
    
  dsl::Waveforms * wfms = this->ai_config_.simulator()->get_waveforms(num_wfms);
  
  if (t.elapsed_msec() >= timeout_ms)
  { 
  	delete wfms;
    throw DAQException("TIMEOUT_EXPIRED",
                       "simulator::get_waveforms took more than the <specified timeout> to complete",
						           "NIDigitizer::get_waveforms");
  } 

#endif //- _SIMULATION_


  //- std::cout << "NIDigitizer::get_waveforms ->" << std::endl;

  return wfms;
}

// ============================================================================
// NIDigitizer::get_data_i
// ============================================================================
dsl::AIScaledData * NIDigitizer::get_data_i (unsigned long timeout_ms) 
    throw (DAQException)
{
  //- std::cout << "NIDigitizer::get_data_i <-" << std::endl;
  
  //- allocate the destination buffers
  dsl::AIScaledData * d = 0;

#if !defined(_SIMULATION_)
 
  ViStatus err;

  //- signal processing enabled (FFT)
  if (this->ai_config_.fft_mode() == dsl::fft_none)
  {
    d = DSL_NEW(dsl::AIScaledData(this->daq_buffer_depth_));
    if (d == 0)
    {
      throw DAQException("out of memory",
                         "memory allocation failed",
                         "NIDigitizer::get_data");
    }
    err = ::niScope_Read(this->hh_,
                         this->chan_id_.str().c_str(),
                         static_cast<double>(timeout_ms) / 1000.0,
                         this->ai_config_.get_buffer_depth(),
                         d->base(),
                         static_cast<niScope_wfmInfo*>(g_ni_wfm_info));	  
    if (err)
    {
      if (err == TIMEOUT_EXCEEDED)
      {
        delete d;
        ::niScope_Abort(this->hh_);
        throw dsl::TimeoutException(dsl::TimeoutException::NO_DAQ_TRIGGER);
      }
      else
      { 
        delete d;
        throw DAQException(static_cast<const char*>("could not read channels [niScope_Read failed]"),
                           dsl::niscope_error_text(this->hh_, err),
						               static_cast<const char*>("NIDigitizer::get_data_i"),
                           err);
      }
    }
  }
  //- signal processing enabled (FFT)
  else 
  {
    //- initiate DAQ
    err = ::niScope_InitiateAcquisition(this->hh_);
    if (err)
    {
      delete d;
      throw DAQException(static_cast<const char*>("could not initiate acquisition [niScope_initiateAcquisition failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::get_data_i"),
                              err);
    }
    //- get measurement type
    ViInt32 measurement_type;
    switch (this->ai_config_.fft_mode())
    {
      case dsl::fft_amplitude_spectrum_db:
        measurement_type = NISCOPE_VAL_FFT_AMP_SPECTRUM_DB;
        break;
      case dsl::fft_phase_spectrum:
        measurement_type = NISCOPE_VAL_FFT_PHASE_SPECTRUM;
        break;
      case dsl::fft_amplitude_spectrum_volts_rms:
        measurement_type = NISCOPE_VAL_FFT_AMP_SPECTRUM_VOLTS_RMS;
        break;
    }
    //- setup DAQ buffer
    ViInt32 num_wfms;
    err = ::niScope_ActualNumWfms(this->hh_, this->chan_id_.str().c_str(), &num_wfms);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("DAQ buffer setup failed [niScope_ActualNumWfms failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }
    ViInt32 actual_measurement_wfm_size;
    err = ::niScope_ActualMeasWfmSize(this->hh_, measurement_type, &actual_measurement_wfm_size);
    if (err)
    { 
      throw DAQException(static_cast<const char*>("DAQ buffer setup failed [niScope_ActualMeasWfmSize failed]"),
                              dsl::niscope_error_text (this->hh_, err),
						                  static_cast<const char*>("NIDigitizer::configure_continuous_ai"));
    }
    //- store actual buffer depth locally
    this->daq_buffer_depth_ = static_cast<unsigned long>(actual_measurement_wfm_size * num_wfms);
    //- allocate buffer
    d = DSL_NEW(dsl::AIScaledData(this->daq_buffer_depth_));
    if (d == 0)
    {
      throw DAQException("out of memory",
                              "memory allocation failed",
                              "NIDigitizer::get_data");
    }
    //- fetch the data
    err = ::niScope_FetchArrayMeasurement(this->hh_, 
                                          this->chan_id_.str().c_str(),
                                          static_cast<double>(timeout_ms) / 1000.0,
                                          measurement_type, 
                                          actual_measurement_wfm_size, 
                                          d->base(), 
                                          static_cast<niScope_wfmInfo*>(g_ni_wfm_info));
    if (err)
    {
      if (err == TIMEOUT_EXCEEDED)
      {
        delete d;
        ::niScope_Abort(this->hh_);
        throw dsl::TimeoutException(dsl::TimeoutException::NO_DAQ_TRIGGER);
      }
      else
      { 
        delete d;
        throw DAQException(static_cast<const char*>("could not fetch data [niScope_FetchArrayMeasurement failed]"),
                                dsl::niscope_error_text (this->hh_, err),
						                    static_cast<const char*>("NIDigitizer::get_data_i"),
                                err);
      }
    }
  }

#else

  d = DSL_NEW(dsl::AIScaledData(this->daq_buffer_depth_));
  if (d == 0)
  {
    throw DAQException("out of memory",
                            "memory allocation failed",
                            "NIDigitizer::get_data_i");
  }
  
	yat::Thread::sleep(1);

#endif // _SIMULATION_

  //- std::cout << "NIDigitizer::get_data_i ->" << std::endl;

  return d;
}

// ============================================================================
// NIDigitizer::auto_calibrate
// ============================================================================
void NIDigitizer::auto_calibrate ()
  throw (DAQException)
{
	//- std::cout << "NIDigitizer::auto_calibrate <-" << std::endl;
  
#if !defined(_SIMULATION_)

  int err = ::niScope_CalSelfCalibrate(this->hh_, VI_NULL, VI_NULL);

  if (err)
  { 
    throw DAQException(static_cast<const char*>("auto calibration failed [niScope_CalSelfCalibrate failed]"),
                            dsl::niscope_error_text (this->hh_, err),
						                static_cast<const char*>("NIDigitizer::auto_calibrate"));
  }

#else

	yat::Thread::sleep(2500);

#endif

	//- std::cout << "NIDigitizer::auto_calibrate ->" << std::endl;
}

} // namespace dsl

#endif // _NI_SUPPORT_



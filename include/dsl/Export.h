// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Export.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _EXPORT_H_
#define _EXPORT_H_

#if defined(WIN32)
# if defined(DSL_HAS_DLL) 
#   if defined (DSL_BUILD)
#     define DSL_EXPORT __declspec(dllexport)
#   else
#     define DSL_EXPORT __declspec(dllimport)
#   endif
# else
#   define DSL_EXPORT
# endif
#else
# define DSL_EXPORT
#endif

#endif // _EXPORT_H_




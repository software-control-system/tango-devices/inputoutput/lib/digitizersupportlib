// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    CalibrableHardware.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _CALIBRABLE_HARDWARE_H_
#define _CALIBRABLE_HARDWARE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/Rsrc.h>
#include <dsl/Hardware.h>
#include <dsl/DAQException.h>

namespace dsl {

// ============================================================================
//! The calibrable boards base class.  
// ============================================================================
class DSL_EXPORT CalibrableHardware : public Hardware
{
public:

  /**
   * Performs hardware auto-calibration.
   */
  virtual void auto_calibrate () 
    throw (DAQException);

protected:

  /**
   * Constructor.
   */
  CalibrableHardware (unsigned short _type, unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~CalibrableHardware ();
};

} // namespace dsl

#endif // _CALIBRABLE_HARDWARE_H_





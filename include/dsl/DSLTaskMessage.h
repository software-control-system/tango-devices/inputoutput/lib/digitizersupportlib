// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    DSLTaskMessage.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _DSL_MESSAGE_H_
#define _DSL_MESSAGE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/threading/Message.h>

namespace dsl {

// ============================================================================
//! Message types
// ============================================================================
//! 
//! detailed description to be written
//! 
// ============================================================================
struct DSL_EXPORT MessageType
{
  enum 
  {
      DSL_MSG_CONFIG = yat::FIRST_USER_MSG,
      DSL_MSG_CALIB, 
      DSL_MSG_START, 
      DSL_MSG_STOP,  
      DSL_MSG_ABORT,  
      DSL_MSG_DATA, 
      DSL_MSG_TIMEOUT,      
      DSL_MSG_ERROR,
      DSL_FIRST_USER_MSG
  };
};

// ============================================================================
//! Message priorities
// ============================================================================
//! 
//! detailed description to be written
//! 
// ============================================================================
struct DSL_EXPORT MessagePriority
{
  enum 
  {
    DSL_MSG_PRIO_CONFIG   = DEFAULT_MSG_PRIORITY,
  	DSL_MSG_PRIO_CALIB    = MAX_USER_PRIORITY - 3,
    DSL_MSG_PRIO_START    = DEFAULT_MSG_PRIORITY,
    DSL_MSG_PRIO_STOP     = DEFAULT_MSG_PRIORITY,
    DSL_MSG_PRIO_ABORT    = MAX_USER_PRIORITY - 1,
    DSL_MSG_PRIO_DATA     = DEFAULT_MSG_PRIORITY,
    DSL_MSG_PRIO_TIMEOUT  = MAX_USER_PRIORITY - 2,
    DSL_MSG_PRIO_ERROR    = MAX_USER_PRIORITY,
  };
};

} // namespace dsl

#endif // _DSL_MESSAGE_H_


// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ContinuousAIConfig.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_AI_CONFIG_H_
#define _CONTINUOUS_AI_CONFIG_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h>
#include <vector>
#include <yat/threading/MessageQ.h>
#include <dsl/Rsrc.h>
#include <dsl/DAQException.h>
#if defined(_SIMULATION_)
# include <dsl/Simulator.h>  
#endif

namespace dsl 
{

// ============================================================================
// Create a dedicated type for DAQ events callback
// ============================================================================
#if defined(WIN32)
  typedef void (*DAQCallback) ();
#else
  typedef void (*DAQCallback) (int);
#endif

// ============================================================================
// Create a dedicated type for std::vector of dsl::ActiveAIChannel
// ============================================================================
typedef std::vector<ActiveAIChannel> ActiveAIChannels;

// ============================================================================
//! The DSL continuous analog input configuration class.  
// ============================================================================
//!  
//! This class encapsulates the ContinuousAI configuration parameters.
//! Most of this parameters are defined in <Manufacturer>Rsrc.h.
//! 
// ============================================================================
class DSL_EXPORT ContinuousAIConfig
{
  friend class ContinuousAI;
  friend class Digitizer;

public:

  /**
   * ContinousAIConfig config ctor.
   */
  ContinuousAIConfig ();
  
  /**
   * ContinousAIConfig copy ctor.
   * @param src The source configuration.
   */
  ContinuousAIConfig (const ContinuousAIConfig& src);

  /**
   * ContinousAIConfig operator=.
   * @param src The source configuration.
   */
  ContinuousAIConfig& operator= (const ContinuousAIConfig& src);

  /**
   * ContinousAIConfig dtor.
   */
  virtual ~ContinuousAIConfig ();

  /**
   * Adds an AI channels to the ActiveAIChannel list.
   * See class ActiveAIChannel in Rsrc.h for details.
   * @param ac The channel to be activated/enabled.
   */
  void add_active_channel (const ActiveAIChannel& ac);

  /**
   * Returns the <ActiveAIChannel> list.
   * @return The active channels list.
   */
  ActiveAIChannels & get_active_channels ();

  /**
   * Returns the current number of active channels.
   * @return The number of active channels.
   */
  int num_active_channels () const;

  /**
   * Set the number of ADC(s) per channel.
   * See defintion of AINumAdcPerChannel for possible values
   * Defaults to dsl::one_adc_per_channel.
   * @param number of ADC(s) per channel.
   */
  void set_num_adc_per_channel (AINumAdcPerChannel n_adc);

  /**
   * Returns the current number of ADC(s) per channel.
   * @return The number of ADC(s) per channel.
   */
  AINumAdcPerChannel get_num_adc_per_channel () const; 

  /**
   * Set the DAQ trigger source.
   * See defintion of AITriggerSource for possible values
   * Defaults to dsl::internal_software.
   * @param trig_source DAQ trigger source.
   */
  void set_trigger_source (AITriggerSource trig_source);

  /**
   * Returns the DAQ trigger source.
   * See defintion of AITriggerSource for returned values.
   * @return The DAQ trigger source.
   */
	AITriggerSource get_trigger_source () const;

  /**
   * Set the DAQ trigger level.
   * Set the voltage threshold for the trigger in V
   * Defaults is 2.5 V (TTL level).
   * @param trig_level DAQ trigger level.
   */
  void set_trigger_level (TriggerLevel trig_level);

  /**
   * Returns the DAQ trigger level in V.
   * @return The DAQ trigger level.
   */
   TriggerLevel get_trigger_level () const;

  /**
   * Set if a rising or a falling edge triggers the digitizer.
   * Use one of the following: dsl::trigger_slope_positive, dsl::trigger_slope_negative
   * Defaults to dsl::trigger_slope_positive.
   * @param trig_slope DAQ trigger slope.
   */
  void set_trigger_slope (AITriggerSlope trig_slope);

  /**
   * Returns the DAQ trigger slope.
   * @return The DAQ trigger slope.
   */
  AITriggerSlope get_trigger_slope () const;

  /**
   * Set the analog trigger coupling.
   * See AITriggerCoupling definition for possible values.
   * @param src DAQ trigger coupling.
   */
   void set_trigger_coupling (AITriggerCoupling trig_coupling);

  /**
   * Returns the analog trigger coupling.
   * See AITriggerCoupling definition for returned values.
   * @return The DAQ trigger coupling.
   */
   AITriggerCoupling get_trigger_coupling () const;

  /**
   * Set the DAQ trigger delay.
   * Set the trigger delay time in seconds
   * Defaults is 0.0 s
   * @param trig_delay DAQ trigger delay.
   */
  void set_trigger_delay (double trig_delay);

  /**
   * Returns the DAQ trigger delay.
   * @return The DAQ trigger delay.
   */
  double get_trigger_delay () const;
  
  /**
   * Set the DAQ sampling rate. This configuration parameter is only valid if the AI 
   * conversion signal source (i.e. DAC sampling source) has been set to 
   * dsl::internal_timer. Valid range is board dependent.
   * @param sr The DAQ sampling rate in Hertz (board dependent value). Defaults to 100kHz.
   */
  void set_sampling_rate (double sr);

  /**
   * Returns the DAQ sampling rate. This configuration parameter is only valid if the 
   * AI conversion signal source (i.e. DAC sampling source) has been set to 
   * dsl::internal_timer.
   *
   * @return The DAQ sampling rate in Hertz (board dependent value).
   */
  double get_sampling_rate () const;

  /**
   * Enable the DAQ 10MHz external reference clock.
   */
  void enable_10MHz_external_reference_clock ();

  /**
   * Disable the DAQ 10MHz external reference clock.
   */
  void disable_10MHz_external_reference_clock ();

  /**
   * Returns true if the DAQ 10MHz external reference clock is enabled, false otherwise.
   */
  bool external_reference_clock_enabled () const;

  /**
   * Set the DAQ sampling rate source.
   * Use one of the following: dsl::internal_sampling_rate_source,
   *  dsl::external_sampling_rate_source
   * Defaults to dsl::internal_sampling_rate_source.
   * @param sampling_rate_source DAQ sampling rate source.
   */
  void set_sampling_rate_source (AISamplingRateSource sampling_rate_source);

  /**
   * Returns the DAQ sampling rate source.
   * @return The DAQ sampling rate source.
   */
	AISamplingRateSource get_sampling_rate_source () const;

  /**
   * Set the DAQ buffer depth. The user defined callback ContinuousAI::handle_input
   * will be called each time "depth" samples have been acquired on each active channel.
   * The specified value must be even (otherwise an exception will be thrown at DAQ startup).
   *
   * @param db_depth The DAQ buffer depth in samples per active channel. Defaults to: 1000.
   */ 
  void set_buffer_depth (unsigned long db_depth);

  /**
   * Returns the DAQ buffer depth.
   * @return The DAQ buffer depth in samples per active channel.
   */
  unsigned long get_buffer_depth () const;

  /**
   * Set the num of waveforms to acquire each time the hardware is read.
   * The each waveform will have a depth of <ContinuousAIConfig::get_buffer_depth> samples each. 
   *
   * @param n_fwms The num of waveforms to acquire.
   */ 
  void set_num_waveforms (size_t n_fwms);

  /**
   * Returns the number of waveforms acquired each time the <ContinousAI::get_waveforms> is called.
   *
   * @return The number of waveforms.
   */ 
  size_t get_num_waveforms () const;

  /**
   * Set the DAQ timeout. The user defined callback ContinuousAI::handle_timeout
   * will be called each time the specified timeout elapsed and no data could been 
   * acquired. This provides a way to detect DAQ problems (such as DAQ buffer depth 
   * too small or no trigger activity). 
   * @param tmo_sec The DAQ timeout in milliseconds. Defaults to: 1000 (i.e. 1 second).
   */
  void set_timeout (unsigned long tmo_sec);

  /**
   * Returns the DAQ timeout.
   * @return The DAQ timeout in milliseconds.
   */
  unsigned long get_timeout () const;

  /**
   * Enable FFT signal processing
   * @param fft_mode The FFT mode.
   */
  void enable_fft (const AIFFTMode & fft_mode);

  /**
   * Disable FFT signal processing
   */
  void disable_fft ();

  /**
   * Returns the current FFT mode
   */
  const AIFFTMode & fft_mode () const;

  /**
   * Set daq mode to <mode>
   */
  void daq_mode (const DAQMode & mode);
  
  /**
   * Returns the current daq mode
   */
  const DAQMode & daq_mode () const;

  /**
   * Mutator for: treat <no data> timeout as error?
   */
  void treat_timeout_as_error (bool mode);

  /**
   * Accessor for: treat <no data> timeout as error?
   */
  bool treat_timeout_as_error () const;

  //- MsgQ low & high water marks unit mutator (see yat::MessageQ::WmUnit)
  void msgq_wm_unit (yat::MessageQ::WmUnit _u);
  
  //- MsgQ low & high water marks unit accessor (see yat::MessageQ::WmUnit)
  yat::MessageQ::WmUnit msgq_wm_unit () const;
  
  //- MsgQ low water mark mutator (_lo_wm is wm unit dependent)
  void msgq_lo_wm (size_t _lo_wm);
  
  //- MsgQ low water mark accessor (returned value is wm unit dependent)
  size_t msgq_lo_wm () const;
  
  //- MsgQ high water mark mutator (_hi_wm is wm unit dependent)
  void msgq_hi_wm (size_t _hi_wm);
  
  //- MsgQ high water mark accessor (returned value is wm unit dependent)
  size_t msgq_hi_wm () const;
  
#if defined(_SIMULATION_)
  /**
   * Set associated HW simulator (does not get ownership!)
   */
	void simulator (dsl::Simulator * s);

  /**
   * Returns associated HW simulator (NULL means none)
   */
  dsl::Simulator * simulator () const;
#endif

  /**
   * Dump this configuration.
   */
  void dump (); 

private:
  //- active channels
  ActiveAIChannels channels_;
  
  //- adc(s) per channel
  AINumAdcPerChannel adc_per_channel_;

  //- trigger source
  dsl::AITriggerSource trigger_source_;
  
  //- trigger level
  dsl::TriggerLevel trigger_level_;
  
  //- trigger slope
  dsl::AITriggerSlope trigger_slope_;
  
  //- trigger coupling
  dsl::AITriggerCoupling trigger_coupling_;
  
  //- trigger holdoff
  dsl::TriggerDelay trigger_delay_;
  
  //- 10MHz external reference clock on/off
  bool external_ref_clock_enabled_;
  
  //- sampling rate source
  AISamplingRateSource sampling_rate_source_;
  
  //- sampling rate 
  double sampling_rate_;
  
  //- num of waveforms to acquire
  unsigned long num_waveforms_;

  //- buffer depth in samples
  unsigned long buffer_depth_;
  
  //- daq timeout in msecs 
  unsigned long timeout_;
  
  //- fft mode
  AIFFTMode fft_mode_;

  //- daq mode
	DAQMode daq_mode_;

  //- <treat_timeout_as_error> flag
  bool treat_timeout_as_error_;
  
  //- low & high water marks unit 
  yat::MessageQ::WmUnit msgq_wm_unit_;
  
  //- low & high water marks for msqQ (this is wm dependent)
  size_t msgq_lo_wm_;
  size_t msgq_hi_wm_;

#if defined(_SIMULATION_)
  //- hw simulator
  dsl::Simulator * simulator_;
#endif
};

} // nemespace dsl

#if defined (__DSL_INLINE__)
# include "dsl/ContinuousAIConfig.i"
#endif // __DSL_INLINE__

#endif // _CONTINUOUS_AI_CONFIG_H_

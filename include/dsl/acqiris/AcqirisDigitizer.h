// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed AcqirisDigitizer Support Library
//
// = FILENAME
//    AcqirisDigitizer.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _ACQIRIS_DIGITIZER_H_
#define _ACQIRIS_DIGITIZER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/CalibrableHardware.h>
#include <dsl/DAQException.h>
#include <dsl/Digitizer.h>
#include <dsl/ContinuousAIConfig.h>

namespace dsl {

// ============================================================================
//! The AcqirisDigitizer boards abstract base class 
// ============================================================================
class DSL_EXPORT AcqirisDigitizer : public Digitizer
{
public:

  /**
   * Digitizer factory.
   * @param _type The type of the board.
   * @param _id The number The number of the card in the cPCI crate.
   * @param _exclusive_access Get exclusive access to the hardware (if set to true).
   * @param _calibration Calibration done in init phase (if set to true).
   * @return The instanciate board.
   */
  static Digitizer * instanciate (unsigned short _type, 
			                            unsigned short _id, 
			                            bool _exclusive_access,
										bool _calibration)
    throw (DAQException);

  /**
   * Continuous AI configuration.
   * @param config The DAQ configuration.
   */
  virtual void configure_continuous_ai (const dsl::ContinuousAIConfig& config) 
    throw (DAQException);

  /**
   * Start continous AI.
   */
  virtual void start_continuous_ai () 
    throw (DAQException);

  /**
   * Stop continous AI.
   */
  virtual void stop_continuous_ai () 
    throw (DAQException);

  /**
   * Returns acquired data.
   */
  virtual dsl::AIScaledData * get_data (unsigned long timeout_ms) 
    throw (DAQException);

  /**
   * Returns acquired waveforms.
   */
  virtual dsl::Waveforms * get_waveforms (size_t num_wfms, unsigned long timeout_ms) 
    throw (DAQException);

  /**
   * Performs hardware auto-calibration.
   */
  virtual void auto_calibrate () 
    throw (DAQException);

  /**
   * Returns true if the digitizer supports <10Mhz ref. clock output routing> capability,
   * returns false otherwise.
   */
  virtual bool has_10mhz_ref_clock_output_routing_support () const = 0;

protected:

  /**
   * Constructor.
   */
  AcqirisDigitizer (unsigned short _type, unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~AcqirisDigitizer();

  /**
   * Register the board with the driver.
   */
  virtual int register_hardware ();

  /**
   * Unregister the board with the driver.
   */
  virtual int release_hardware ();

  /**
   * DAQ buffer depth
   */
  unsigned long daq_buffer_depth_;
};

} // namespace dsl

#endif // _ACQIRIS_DIGITIZER_H_

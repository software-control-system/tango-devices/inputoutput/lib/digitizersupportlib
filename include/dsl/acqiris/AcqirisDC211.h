// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    AcqirisDC211.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _ACQIRIS_DC211_H_
#define _ACQIRIS_DC211_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <map>
#include <yat/threading/Mutex.h>
#include <dsl/acqiris/AcqirisDigitizer.h>

namespace dsl {

// ============================================================================
//! The ACQIRIS DC-211 digitizer abstraction.  
// ============================================================================
class DSL_EXPORT AcqirisDC211 : public AcqirisDigitizer
{
  typedef std::map<unsigned short, AcqirisDC211*> Repository;

  typedef Repository::iterator RepositoryIterator;

  typedef Repository::value_type RepositoryValue;

public:

  /**
   * Instanciate a AcqirisDC211 board.
   * @param _id The number of the card in the cPCI crate.
   * @param _calibration Calibration done in init phase (if set to true).   
   * @param _exclusive_access Allows or not the exclusive access (exclusive access by default).
   * @return A pointer to the card.
   * @throw DAQException 
   */
  static AcqirisDC211* instanciate (unsigned short _id, bool _calibration, bool _exclusive_access = true)
    throw (DAQException);

  /**
   * Duplicate this shared object (if no exclusive access).
   */
   AcqirisDC211 * duplicate ();


  /**
   * Dump hardware info.
   */
  virtual void dump () const;

  /**
   * Returns the numbers of channels of the board.
   */
  virtual unsigned short num_channels () const;

  /**
   * Returns true if the digitizer supports <10Mhz ref. clock output routing> capability,
   * returns false otherwise.
   */
  virtual bool has_10mhz_ref_clock_output_routing_support () const;
  
protected:

  /**
   * Constructor.
   */
	AcqirisDC211 (unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~AcqirisDC211();

  /**
   * Returns the maximum sampling rate in Hz.
   */
	virtual double max_sampling_rate () const;

private:
  /**
   * Boards repository.
   */
  static Repository repository;
  
  /**
   * Boards repository' lock.
   */
  static yat::Mutex repository_lock;
};

} // namespace dsl

#endif // _ACQIRIS_DC211_H_

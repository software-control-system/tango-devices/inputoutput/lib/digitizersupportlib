// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    AcqirisRsrc.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _ACQIRIS_RSRC_H_
#define _ACQIRIS_RSRC_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#if ! defined (_SIMULATION_)
#	include <vpptype.h>
#	include <AcqirisD1Import.h>
#endif

#define ACQIRIS_BOARDS_ID_OFFSET 100

namespace dsl {

// ============================================================================
// MISC TYPEs
// ============================================================================
#if ! defined (_SIMULATION_)
	typedef ViSession HardwareHandle;
	typedef ViStatus DigitizerErr;
#else
	typedef unsigned long HardwareHandle;
	typedef unsigned long DigitizerErr;
	typedef unsigned int ViInt32;
#endif

// ============================================================================
// DIGITIZERS: DUMMY HARDWARE HANDLE  
// ============================================================================
#define kUNDEFINED_HH 0xFFFFFFFF

// ============================================================================
// DIGITIZERS: IDENTIFIER
// ============================================================================
typedef enum 
{
  //- ACQIRIS-DC211
  ACQIRIS_DC211 = ACQIRIS_BOARDS_ID_OFFSET,
  //- ACQIRIS-DC252
  ACQIRIS_DC252
} 
AcqirisDigitizers;

// ============================================================================
// MAX NUM WAVEFORMS THAN CAN BE RETREIVED IN ONE CALL
// ============================================================================
#define ACQIRIS_MAX_NUM_WAVEFORMS 100

// ============================================================================
// DIGITIZERS: STRING IDENTIFIER
// ============================================================================
//- ACQIRIS/AGILENT-DC211
#define ACQIRIS_DC211_STR_ID "ACQIRIS::DC211"
#define AGILENT_DC211_STR_ID "AGILENT::DC211"
//- ACQIRIS/AGILENT-DC252
#define ACQIRIS_DC252_STR_ID "ACQIRIS::DC252"
#define AGILENT_DC252_STR_ID "AGILENT::DC252"

// ============================================================================
// DIGITIZERS: NUMBER OF CHANNELS
// ============================================================================
#define ACQIRIS_1_CHANNEL   1
#define ACQIRIS_2_CHANNELS  3
#define ACQIRIS_4_CHANNELS  15

// ============================================================================
// DIGITIZERS: NUM CHANNELS
// ============================================================================
#define ACQIRIS_DC211_NUM_CHANNELS 1
#define ACQIRIS_DC252_NUM_CHANNELS 2

// ============================================================================
// DIGITIZERS: MAX SAMPLING RATE
// ============================================================================
//- ACQIRIS-DC211: 4 GSamples/sec
#define ACQIRIS_DC211_MAX_SAMPLING_RATE 4000000000 
//- ACQIRIS-DC252: 8 GSamples/sec
#define ACQIRIS_DC252_MAX_SAMPLING_RATE 8000000000 

// ============================================================================
// DIGITIZERS: CHANNEL INPUT IMPEDANCE
// ============================================================================
#define ACQIRIS_VAL_50_OHMS    50
#define ACQIRIS_VAL_1_MEG_OHM  1

// ============================================================================
// DIGITIZERS: CHANNEL COUPLING + INPUT IMPEDANCE
// ============================================================================
#define ACQIRIS_DC_1_MEG_OHM  1
#define ACQIRIS_AC_1_MEG_OHM  2
#define ACQIRIS_DC_50_OHMS    3
#define ACQIRIS_AC_50_OHMS    4

// ============================================================================
// DIGITIZERS: CHANNEL BANDWIDTH LIMIT
// ============================================================================
#define ACQIRIS_VAL_FULL_BANDWIDTH    0
#define ACQIRIS_VAL_25MHZ_BANDWIDTH   1
#define ACQIRIS_VAL_700MHZ_BANDWIDTH  2
#define ACQIRIS_VAL_200MHZ_BANDWIDTH  3
#define ACQIRIS_VAL_20MHZ_BANDWIDTH   4
#define ACQIRIS_VAL_35MHZ_BANDWIDTH   5

// ============================================================================ 
// ERROR HANDLING: error code to error text
// ============================================================================ 
const char* acqiris_error_text (dsl::HardwareHandle _hh, int _err_code);

} // namespace dsl

#endif // _ACQIRIS_RSRC_H_

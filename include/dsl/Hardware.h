// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Hardware.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _HARDWARE_H_
#define _HARDWARE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>
#include <yat/threading/SharedObject.h>
#include <dsl/Rsrc.h>
#include <dsl/DAQException.h>

namespace dsl {

// ============================================================================
//! A base class for all ADLink daq board.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class DSL_EXPORT Hardware : public yat::SharedObject
{
public:
  /**
   * Dump hardware info.
   */
  virtual void dump () const;

protected:
  /**
   * Constructor.
   */
  Hardware (unsigned short _type, unsigned short _id);
  
  /**
   * Destructor.
   */
  virtual ~Hardware();
 
  /**
   * Register the board with the driver.
   */
  virtual int register_hardware () = 0;

  /**
   * Unregister the board with the driver.
   */
  virtual int release_hardware () = 0;

  /**
   * Check board registration. 
	 * Throws a DAQException if the board has an invalid registration id. Does
	 * nothing otherwise.
   */
	virtual void check_registration () const 
  	throw (DAQException) = 0;
	
  /**
   * The board type
   */
	unsigned short type_;

  /**
   * The board identifier
   */
	unsigned short id_;

  /**
   * True if board can't be shared
   */
  bool exclusive_access_;
  
  /**
   * True if calibration is required at init
   */
  bool is_calibration_at_init_;  
};

} // namespace dsl

#if defined (__DSL_INLINE__)
# include "dsl/Hardware.i"
#endif // __DSL_INLINE__

#endif // _HARDWARE_H_

// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    DSLConfig.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================


#ifndef _DSL_CONFIF_H_
#define _DSL_CONFIF_H_

//-----------------------------------------------------------------------------
// AGILENT <- MEANS -> ACQIRIS
//-----------------------------------------------------------------------------
#if defined(_ACQIRIS_SUPPORT_)
# define _AGILENT_SUPPORT_
#elif defined(_AGILENT_SUPPORT_)
# define _ACQIRIS_SUPPORT_
#endif

//-----------------------------------------------------------------------------
// WINDOWS PRAGMA
//-----------------------------------------------------------------------------
#if defined (WIN32)
# pragma warning( disable : 4786 )
# pragma warning( disable : 4290 )
# pragma warning( disable : 4996 )
#endif

//-----------------------------------------------------------------------------
// WIN32 DLL EXPORT STUFFS
//-----------------------------------------------------------------------------
#include <dsl/Export.h>

//-----------------------------------------------------------------------------
// DEBUG
//-----------------------------------------------------------------------------
#if defined(_DEBUG) || defined (DEBUG)
# define DSL_DEBUG
#else
# undef DSL_DEBUG
#endif


//-----------------------------------------------------------------------------
// INLINE
//-----------------------------------------------------------------------------
#if ! defined(DSL_DEBUG)
# define __DSL_INLINE__
# define DSL_INLINE inline
#else
# undef __DSL_INLINE__
# define DSL_INLINE
#endif

//-----------------------------------------------------------------------------
// NEW MACRO: DEALS WITH EXCEPTION THROWING ON ALLOCATION FAILURE
//-----------------------------------------------------------------------------
#define DSL_NEW(CTOR) new (std::nothrow) CTOR

//-----------------------------------------------------------------------------
// OPTIONAL DOWN CONVERTER SUPPORT
//-----------------------------------------------------------------------------
//#define DOWN_CONVERTER_SUPPORT

#endif




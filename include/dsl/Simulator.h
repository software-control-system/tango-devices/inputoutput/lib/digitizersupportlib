// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Simulator.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _SIMULTATOR_H_
#define _SIMULTATOR_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h>
#include <dsl/Waveform.h>

namespace dsl {


// ============================================================================
//! The DSL Simulators abstraction class.  
// ============================================================================
class DSL_EXPORT Simulator 
{
public:
  
  /**
   * Initialization. 
   */
  Simulator () {};
  
  /**
   * Release resources.
   */
  virtual ~Simulator () {};

  /**
   * Returns acquired waveforms.
   */
  virtual dsl::Waveforms * get_waveforms (size_t num_wfms, unsigned long timeout_ms) 
    throw (DAQException) = 0;

private:
  // = Disallow these operations.
  //--------------------------------------------
  Simulator& operator= (const Simulator&);
  Simulator(const Simulator&);
};

} // namespace dsl

#endif // _SIMULTATOR_H_


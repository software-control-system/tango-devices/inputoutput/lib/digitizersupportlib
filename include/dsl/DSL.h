// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    DSL.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ===========================================================================

#ifndef _DSL_H_
#define _DSL_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/Exception.h>

namespace dsl {

// ============================================================================
//! The DSL lib 
// ============================================================================
class DSL_EXPORT DSL 
{
public:
  /**
   * DSL library initialization
   */
  static void init ()
    throw (Exception);

  /**
   * DSL library termination
   */
  static void close ()
    throw (Exception);
};

} // namespace dsl

#endif // _DSL_H_







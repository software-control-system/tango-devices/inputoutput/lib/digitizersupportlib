// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ContinuousAIConfig.i
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

namespace dsl {

// ============================================================================
// ContinuousAIConfig::add_active_channel
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::add_active_channel (const ActiveAIChannel & _ac) 
{
  this->channels_.push_back(_ac);
}

// ============================================================================
// ContinuousAIConfig::get_active_channels
// ============================================================================
DSL_INLINE ActiveAIChannels & 
ContinuousAIConfig::get_active_channels ()
{
  return this->channels_;
}

// ============================================================================
// ContinuousAIConfig::num_active_channels
// ============================================================================
DSL_INLINE int 
ContinuousAIConfig::num_active_channels () const
{
  return this->channels_.size();
}

// ============================================================================
// ContinuousAIConfig::set_num_adc_per_channel
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_num_adc_per_channel (AINumAdcPerChannel n_adc)
{
  this->adc_per_channel_ = n_adc;
}

// ============================================================================
// ContinuousAIConfig::get_num_adc_per_channel
// ============================================================================
DSL_INLINE AINumAdcPerChannel 
ContinuousAIConfig::get_num_adc_per_channel () const
{
  return this->adc_per_channel_;
}

// ============================================================================
// ContinuousAIConfig::set_sampling_rate
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_sampling_rate (double _sr)
{
	this->sampling_rate_ = _sr;
}

// ============================================================================
// ContinuousAIConfig::get_sampling_rate
// ============================================================================
DSL_INLINE double 
ContinuousAIConfig::get_sampling_rate () const
{
	return this->sampling_rate_;
}

// ============================================================================
// ContinuousAIConfig::set_buffer_depth
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_buffer_depth (unsigned long _depth)
{
	this->buffer_depth_ = _depth;
}

// ============================================================================
// ContinuousAIConfig::get_buffer_depth
// ============================================================================
DSL_INLINE unsigned long 
ContinuousAIConfig::get_buffer_depth () const
{
	return this->buffer_depth_;
}

// ============================================================================
// ContinuousAIConfig::set_timeout
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_timeout (unsigned long _tmo_msec)
{
	this->timeout_ = _tmo_msec;
}

// ============================================================================
// ContinuousAIConfig::get_timeout
// ============================================================================
DSL_INLINE unsigned long
ContinuousAIConfig::get_timeout () const
{
	return this->timeout_;
}

// ============================================================================
// ContinuousAIConfig::set_trigger_source
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_trigger_source (AITriggerSource _trig_source)
{
	this->trigger_source_ = _trig_source;
}

// ============================================================================
// ContinuousAIConfig::get_trigger_source
// ============================================================================
DSL_INLINE dsl::AITriggerSource
ContinuousAIConfig::get_trigger_source () const
{
	return this->trigger_source_;
}

// ============================================================================
// ContinuousAIConfig::set_trigger_level
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_trigger_level (TriggerLevel _trig_level)
{
	this->trigger_level_ = _trig_level;
}

// ============================================================================
// ContinuousAIConfig::get_trigger_level
// ============================================================================
DSL_INLINE dsl::TriggerLevel
ContinuousAIConfig::get_trigger_level () const
{
	return this->trigger_level_;
}

// ============================================================================
// ContinuousAIConfig::set_trigger_slope
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_trigger_slope (AITriggerSlope _trig_slope)
{
	this->trigger_slope_ = _trig_slope;
}

// ============================================================================
// ContinuousAIConfig::get_trigger_slope
// ============================================================================
DSL_INLINE dsl::AITriggerSlope
ContinuousAIConfig::get_trigger_slope () const
{
	return this->trigger_slope_;
}


// ============================================================================
// ContinuousAIConfig::set_trigger_coupling
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_trigger_coupling (AITriggerCoupling _trig_coupling)
{
	this->trigger_coupling_ = _trig_coupling;
}

// ============================================================================
// ContinuousAIConfig::get_trigger_coupling
// ============================================================================
DSL_INLINE dsl::AITriggerCoupling
ContinuousAIConfig::get_trigger_coupling () const
{
	return this->trigger_coupling_;
}

// ============================================================================
// ContinuousAIConfig::set_trigger_delay
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_trigger_delay (double _trig_delay)
{
	this->trigger_delay_ = _trig_delay;
}

// ============================================================================
// ContinuousAIConfig::get_trigger_delay
// ============================================================================
DSL_INLINE double
ContinuousAIConfig::get_trigger_delay () const
{
	return this->trigger_delay_;
}

// ============================================================================
// ContinuousAIConfig::enable_10MHz_external_reference_clock
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::enable_10MHz_external_reference_clock ()
{
	this->external_ref_clock_enabled_ = true;
}

// ============================================================================
// ContinuousAIConfig::disable_10MHz_external_reference_clock
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::disable_10MHz_external_reference_clock ()
{
	this->external_ref_clock_enabled_ = false;
}

// ============================================================================
// ContinuousAIConfig::external_reference_clock_enabled
// ============================================================================
DSL_INLINE bool 
ContinuousAIConfig::external_reference_clock_enabled () const
{
	return this->external_ref_clock_enabled_;
}

// ============================================================================
// ContinuousAIConfig::set_sampling_rate_source
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_sampling_rate_source (AISamplingRateSource _sampling_rate_source)
{
	this->sampling_rate_source_ = _sampling_rate_source;
}

// ============================================================================
// ContinuousAIConfig::get_sampling_rate_source
// ============================================================================
DSL_INLINE AISamplingRateSource
ContinuousAIConfig::get_sampling_rate_source () const
{
	return this->sampling_rate_source_;
}

// ============================================================================
// ContinuousAIConfig::enable_fft
// ============================================================================
DSL_INLINE void
ContinuousAIConfig::enable_fft (const dsl::AIFFTMode & _fft_mode)
{
	this->fft_mode_ = _fft_mode;
}

// ============================================================================
// ContinuousAIConfig::disable_fft
// ============================================================================
DSL_INLINE void
ContinuousAIConfig::disable_fft ()
{
	this->fft_mode_ = dsl::fft_none;
}

// ============================================================================
// ContinuousAIConfig::fft_mode
// ============================================================================
DSL_INLINE const AIFFTMode & 
ContinuousAIConfig::fft_mode () const
{
	return this->fft_mode_;
}

// ============================================================================
// ContinuousAIConfig::fft_mode
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::daq_mode (const DAQMode & _mode)
{
	this->daq_mode_ = _mode;
}

// ============================================================================
// ContinuousAIConfig::fft_mode
// ============================================================================
DSL_INLINE const DAQMode & 
ContinuousAIConfig::daq_mode () const
{
	return this->daq_mode_;
}
  
// ============================================================================
// ContinuousAIConfig::treat_timeout_as_error
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::treat_timeout_as_error (bool mode)
{
	this->treat_timeout_as_error_ = mode;
}

// ============================================================================
// ContinuousAIConfig::treat_timeout_as_error
// ============================================================================
DSL_INLINE bool 
ContinuousAIConfig::treat_timeout_as_error () const
{
	return this->treat_timeout_as_error_;
}

// ============================================================================
// ContinuousAIConfig::msgq_lo_wm
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::msgq_lo_wm (size_t _lo_wm)
{
	this->msgq_lo_wm_ = _lo_wm;
}

// ============================================================================
// ContinuousAIConfig::msgq_lo_wm
// ============================================================================
DSL_INLINE size_t 
ContinuousAIConfig::msgq_lo_wm () const
{
	return this->msgq_lo_wm_;
}

// ============================================================================
// ContinuousAIConfig::msgq_hi_wm
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::msgq_hi_wm (size_t _hi_wm)
{
	this->msgq_hi_wm_ = _hi_wm;
}

// ============================================================================
// ContinuousAIConfig::msgq_hi_wm
// ============================================================================
DSL_INLINE size_t 
ContinuousAIConfig::msgq_hi_wm () const
{
	return this->msgq_hi_wm_;
}

// ============================================================================
// ContinuousAIConfig::msgq_hi_wm
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::msgq_wm_unit (yat::MessageQ::WmUnit _u)
{
	this->msgq_wm_unit_ = _u;
}

// ============================================================================
// ContinuousAIConfig::msgq_hi_wm
// ============================================================================
DSL_INLINE yat::MessageQ::WmUnit 
ContinuousAIConfig::msgq_wm_unit () const
{
	return this->msgq_wm_unit_;
}

// ============================================================================
// ContinuousAIConfig::set_num_waveforms
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::set_num_waveforms (size_t _n)
{
  this->num_waveforms_ = (_n > 0) ? _n : 1;
}

// ============================================================================
// ContinuousAIConfig::get_num_waveforms
// ============================================================================
DSL_INLINE size_t 
ContinuousAIConfig::get_num_waveforms () const
{
  return this->num_waveforms_;
}

#if defined(_SIMULATION_)
// ============================================================================
// ContinuousAIConfig::simulator
// ============================================================================
DSL_INLINE void 
ContinuousAIConfig::simulator (dsl::Simulator * s)
{
  this->simulator_ = s;
}

// ============================================================================
// ContinuousAIConfig::simulator
// ============================================================================
DSL_INLINE dsl::Simulator *
ContinuousAIConfig::simulator () const
{
  return this->simulator_;
}
#endif //- _SIMULATION_

} // namespace dsl



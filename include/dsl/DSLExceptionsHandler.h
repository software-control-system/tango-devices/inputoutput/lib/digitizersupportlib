// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    DSLExceptionHandler.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#pragma once

//-----------------------------------------------------------------------------
// WINDOWS PRAGMA
//-----------------------------------------------------------------------------
#if defined (WIN32)
# pragma warning (disable : 4286)
#endif
  
//=============================================================================
// _DSL_TO_TANGO_EXCEPTION MACRO
//=============================================================================
#define _DSL_TO_TANGO_EXCEPTION(_DSL_ex, _tango_ex) \
  Tango::DevErrorList error_list(_DSL_ex.errors.size()); \
  error_list.length(_DSL_ex.errors.size()); \
  for (int _ii = 0; _ii < _DSL_ex.errors.size(); _ii++) \
  { \
    error_list[_ii].reason = CORBA::string_dup(_DSL_ex.errors[_ii].reason.c_str()); \
    error_list[_ii].desc   = CORBA::string_dup(_DSL_ex.errors[_ii].desc.c_str()); \
    error_list[_ii].origin = CORBA::string_dup(_DSL_ex.errors[_ii].origin.c_str()); \
    switch (_DSL_ex.errors[_ii].severity) \
    { \
      case dsl::WARN: \
        error_list[_ii].severity = Tango::WARN; \
        break; \
      case dsl::PANIC: \
        error_list[_ii].severity = Tango::PANIC; \
        break; \
      case dsl::ERR: \
      default: \
        error_list[_ii].severity = Tango::ERR; \
        break; \
    } \
  } \
  Tango::DevFailed _tango_ex(error_list)

//=============================================================================
// _HANDLE_DSL_EXCEPTION MACRO
//=============================================================================
#define _HANDLE_DSL_EXCEPTION(_cmd, _origin) \
  catch (const dsl::DeviceBusyException& _DSL_ex) \
  { \
    _DSL_TO_TANGO_EXCEPTION(_DSL_ex, _tango_ex); \
    ERROR_STREAM << _tango_ex << std::endl; \
    TangoSys_OMemStream d; \
    d << "dsl::DeviceBusyException caught while trying to execute " \
      << _cmd \
      << std::ends; \
    TangoSys_OMemStream o; \
    o << _origin << " [" << __FILE__ << "::" << __LINE__ << "]" << std::ends; \
    Tango::Except::re_throw_exception(_tango_ex, \
                   static_cast<const char*>("DSL_HW_BUSY"), \
				           static_cast<const char*>(d.str().c_str()), \
				           static_cast<const char*>(o.str().c_str())); \
  } \
  catch (const DAQException& _DSL_ex) \
  { \
    _DSL_TO_TANGO_EXCEPTION(_DSL_ex, _tango_ex); \
    ERROR_STREAM << _tango_ex << std::endl; \
    TangoSys_OMemStream d; \
    d << "DAQException caught while trying to execute " \
      << _cmd \
      << std::ends; \
    TangoSys_OMemStream o; \
    o << _origin << " [" << __FILE__ << "::" << __LINE__ << "]" << std::ends; \
    Tango::Except::re_throw_exception(_tango_ex, \
                   static_cast<const char*>("DSL_ERROR"), \
				           static_cast<const char*>(d.str().c_str()), \
				           static_cast<const char*>(o.str().c_str())); \
  } \
  catch (...) \
  { \
    TangoSys_OMemStream d; \
    d << "unknown exception caught while trying to execute " \
      << _cmd \
      << std::ends; \
    TangoSys_OMemStream o; \
    o << _origin << " [" << __FILE__ << "::" << __LINE__ << "]" << std::ends; \
    Tango::DevErrorList errors(1); \
    errors.length(1); \
    errors[0].severity = Tango::ERR; \
    errors[0].reason = CORBA::string_dup("UNKNOWN_ERROR"); \
    errors[0].desc = CORBA::string_dup(d.str().c_str()); \
    errors[0].origin = CORBA::string_dup(o.str().c_str()); \
    Tango::DevFailed _tango_ex(errors); \
    ERROR_STREAM << _tango_ex << std::endl; \
    throw _tango_ex; \
  }

//=============================================================================
// _DSL_TRY MACRO
//=============================================================================
#define _DSL_TRY(_invoke, _cmd, _origin) \
  try \
  { \
    _invoke; \
  } \
  _HANDLE_DSL_EXCEPTION(_cmd, _origin)


//=============================================================================
// _HANDLE_DSL_EXCEPTION_REACTION MACRO
//=============================================================================
#define _HANDLE_DSL_EXCEPTION_REACTION(_cmd, _origin, _reaction) \
  catch (...) \
  { \
    _reaction; \
    _DSL_TRY(throw, _cmd, _origin) \
  }

//=============================================================================
// _DSL_TRY_ACTION MACRO
//=============================================================================
#define _DSL_TRY_ACTION(_invoke, _cmd, _origin, _action) \
  try { \
    _DSL_TRY(_invoke, _cmd, _origin) \
    _action; \
  } \
  catch (...) { \
    _action; \
    throw; \
  }

//=============================================================================
// _DSL_TRY_REACTION MACRO
//=============================================================================
#define _DSL_TRY_REACTION(_invoke, _cmd, _origin, _reaction) \
  try { \
    _DSL_TRY(_invoke, _cmd, _origin) \
  } \
  catch (...) { \
    _reaction; \
    throw; \
  }


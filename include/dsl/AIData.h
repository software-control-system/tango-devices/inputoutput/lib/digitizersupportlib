// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    AIData.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _AI_DATA_H_
#define _AI_DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/memory/DataBuffer.h>
#include <dsl/DSLConfig.h>

namespace dsl {

// ============================================================================
//! Create a dedicated type for AI raw data type
// ============================================================================
typedef double AIDataType;

// ============================================================================
//! Create a dedicated type for AI scaled data (i.e. expressed in volts)  
// ============================================================================
typedef yat::Buffer<AIDataType> AIScaledData;

} // namespace dsl

#endif // _AI_DATA_H_


// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ContinuousAI.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_AI_H_
#define _CONTINUOUS_AI_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/threading/Task.h>
#include <dsl/Digitizer.h>
#include <dsl/ContinuousDAQ.h>
#include <dsl/ContinuousAIConfig.h>

namespace dsl {

// ============================================================================
//! Forward declaration.
// ============================================================================ 
class DaqThread;

// ============================================================================
//! The DSL continuous daq class.  
// ============================================================================ 
class DSL_EXPORT ContinuousAI : public ContinuousDAQ, public yat::Task
{
  //- private friend class
	friend class DaqThread;
  
public:
  /**
   * Initialization. 
   */
  ContinuousAI ();
  
  /**
   * Release resources.
   */
  virtual ~ContinuousAI ();

  //
  // Continuous DAQ control methods
  //-----------------------------------------------------------

  /**
   * Initializes the continuous daq.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id, bool calibration = true)
      throw (DAQException);
      
  /**
   * Configure the continuous daq.
   */
  void configure (const ContinuousAIConfig& config, size_t tmo_ms = 0)
    throw (DAQException, DeviceBusyException);

  /**
   * Configure the continuous daq.
   */
  void configure (ContinuousAIConfig * config, size_t tmo_ms = 0)
      throw (DAQException, DeviceBusyException);

  /**
   * Returns the current configuration.
   */
  const ContinuousAIConfig & configuration () const;

  /**
   * Performs hardware auto-calibration asynchronously (i.e launch
   * calibration process then return immediately). See also ContinuousDAQ
   * calibration_completed and check_hardware_calibration.
   */
  virtual void calibrate_hardware ()
      throw (DAQException, DeviceBusyException);

  /**
   * Starts the continuous daq.
   */
  virtual void start (size_t tmo_ms = 0)
      throw (DAQException, DeviceBusyException);

  /**
   * Stops the continuous daq.
   */
  virtual void stop (size_t tmo_ms = 0)
      throw (DAQException, DeviceBusyException);

  /**
   * Aborts the continuous daq.
   */
  virtual void abort (size_t tmo_ms = 0)
      throw (DAQException, DeviceBusyException);
      
  /**
   * Asks the underlyig task to quit/exit.
   */
  virtual void fini ()
      throw (DAQException, DeviceBusyException);
  
  //
  // Triggers DAQ (does nothing if daq mode != dsl::user_pulsed)
  //-----------------------------------------------------------
  void pulse_daq ()
      throw (DAQException);
  
  //
  // User hooks
  //-----------------------------------------------------------

  /**
   * Data processing user hook.
   * It is the user responsability to delete the passed Waveforms.
   */
  virtual void handle_waveforms (Waveforms * wfms) = 0;  

  /**
   * DAQ exception user hook.
   */
  virtual void handle_error (const DAQException & ex) = 0;

  /**
   * Timeout user hook.
   * This default implementation does nothing.
   */
  virtual void handle_timeout ();
  
  /**
   * User defined msg hook.
   * The default implementation ignores the message.
   */
  virtual void handle_user_msg (yat::Message & msg);

  //
  // Misc. 
  //-----------------------------------------------------------

  /**
   * Dump DAQ info. 
   */
  virtual void dump () const;

  /**
   * Returns the underlying Digitizer
   */
  const Digitizer & hardware () const 
     throw (DAQException);

protected:
  /**
   * DAQ thread entry point. 
   */
  void daq_thread_entry_point (void * t);

  /**
   * yat::Task's message handler
   */
  virtual void handle_message (yat::Message & msg)
    throw (yat::Exception);

private:
  /**
   * Return true if initialized, false otherwise.
   */
   bool initialized () const;

  /**
   * Internal implementation of the configure the member.
   */
  void configure_i (const ContinuousAIConfig & config)
       throw (DAQException);

  /**
   * Internal implementation of the start member.
   */
  void start_i ()
       throw (DAQException);

  /**
   * Internal implementation of the stop member.
   */
  void stop_i (bool stop_on_error = false)
       throw (DAQException);

  /**
   * Internal implementation of the abort member.
   */
  void abort_i ()
       throw (DAQException);

  /**
   * Handle hardware calibration.
   */
  void calibrate_hardware_i ()
      throw (DAQException);
     
  /**
   * Called when task quits/exits.
   */
	void fini_i ()
		throw (DAQException);

  /**
   * The DAQ configuration.
   */
  ContinuousAIConfig config_;

  /**
   * The asynch DAQ activity
   */
  DaqThread * daq_thread_;

  /**
   * The underlying DAQ hardware.
   */
  Digitizer * digitizer_;

  // = Disallow these operations.
  //--------------------------------------------
  ContinuousAI& operator= (const ContinuousAI&);
  ContinuousAI(const ContinuousAI&);
};

} // namespace dsl

#if defined (__DSL_INLINE__)
# include "dsl/ContinuousAI.i"
#endif // __DSL_INLINE__

#endif // _CONTINUOUS_AI_H_


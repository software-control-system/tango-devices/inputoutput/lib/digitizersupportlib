// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Rsrc.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _RSRC_H_
#define _RSRC_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h>
#if defined(_NI_SUPPORT_) 
# include <dsl/ni/NIRsrc.h>
#elif defined(_ACQIRIS_SUPPORT_) || defined(_AGILENT_SUPPORT_)
# include <dsl/acqiris/AcqirisRsrc.h>
#endif

namespace dsl {

// ============================================================================
// Misc. types
// ============================================================================
typedef unsigned short ChanId;


// ============================================================================
// CHANNEL::COUPLING
// ============================================================================
typedef enum
{
  ac_coupling = 0,
  dc_coupling,
} Coupling;
//----------------------------------
#define DSL_DEFAULT_COUPLING dc_coupling

// ============================================================================
// CHANNEL::RANGE
// ============================================================================
typedef double Range;
//----------------------------------
#define DSL_DEFAULT_RANGE 10.0

// ============================================================================
// CHANNEL::OFFSET
// ============================================================================
typedef double Offset;
//----------------------------------
#define DSL_DEFAULT_OFFSET 0.0

// ============================================================================
// CHANNEL::BANDWITDTH
// ============================================================================
typedef enum
{
  bandwidth_20MHz = 0,
  bandwidth_25MHz,
  bandwidth_35MHz,
  bandwidth_100MHz,
  bandwidth_200MHz,
  bandwidth_700MHz,
  bandwidth_full
} Bandwidth;
//--------------------------------------
#define DSL_DEFAULT_BANDWIDTH dsl::bandwidth_full

// ============================================================================
// CHANNEL::IMPEDANCE
// ============================================================================
typedef enum
{
  impedance_50Ohms = 0,
  impedance_1MOhms
} Impedance;
//--------------------------------------
#define DSL_DEFAULT_IMPEDANCE dsl::impedance_50Ohms

// ============================================================================
// BOARD::trigger source
// ============================================================================
typedef enum
{
  trigger_internal_src = 0,
  trigger_digital_external_src,
  trigger_digital_pfi0_src,
  trigger_digital_pfi1_src,
  trigger_digital_pfi2_src,
  trigger_analog_chan0_src,
  trigger_analog_chan1_src,
  trigger_analog_chan2_src,
  trigger_analog_chan3_src,
  trigger_analog_external_src
} AITriggerSource;
//--------------------------------------
#define DSL_DEFAULT_TRIGGER_SOURCE dsl::trigger_internal_src

// ============================================================================
// BOARD::trigger level in V
// ============================================================================
typedef double TriggerLevel;
//----------------------------------
#define DSL_DEFAULT_TRIGGER_LEVEL 2.5

// ============================================================================
// BOARD::trigger slope
// ============================================================================
typedef enum
{
  trigger_slope_positive = 0,
  trigger_slope_negative
} AITriggerSlope;
//--------------------------------------
#define DSL_DEFAULT_TRIGGER_SLOPE dsl::trigger_slope_positive

// ============================================================================
// BOARD::trigger coupling
// ============================================================================
typedef enum
{
  trigger_coupling_dc = 0,
  trigger_coupling_ac,
  trigger_coupling_dc_50Ohms,
  trigger_coupling_ac_50Ohms
} AITriggerCoupling;
//--------------------------------------
#define DSL_DEFAULT_TRIGGER_COUPLING dsl::trigger_coupling_dc


// ============================================================================
// BOARD::trigger delay
// ============================================================================
typedef double TriggerDelay;
//----------------------------------
#define DSL_DEFAULT_DELAY 0.0

// ============================================================================
// BOARD::sampling clock source
// ============================================================================
typedef enum
{
  internal_sampling_rate_source = 0,
  external_sampling_rate_source
} AISamplingRateSource;
//--------------------------------------
#define DSL_DEFAULT_SAMPLING_RATE_SOURCE dsl::internal_sampling_rate_source

// ============================================================================
// BOARD::num of ADC(s) per channel
// ============================================================================
typedef enum
{
  one_adc_per_channel = 1,
  two_adc_per_channel,
} AINumAdcPerChannel;
//--------------------------------------
#define DSL_DEFAULT_NUM_ADC_PER_CHANNEL dsl::one_adc_per_channel

// ============================================================================
// BOARD::fft mode
// ============================================================================
typedef enum
{
  fft_none = 0,
  fft_amplitude_spectrum_db,
  fft_phase_spectrum,
  fft_amplitude_spectrum_volts_rms,
} AIFFTMode;
//--------------------------------------
#define DSL_DEFAULT_FFT_MODE dsl::fft_none

// ============================================================================
// DAQ::daq mode 
// ============================================================================
typedef enum
{
  continuous = 0,
  user_pulsed,
} DAQMode;
//--------------------------------------
#define DSL_DEFAULT_DAQ_MODE dsl::continuous

// ============================================================================
/// Active input channel configuration
// ============================================================================
class DSL_EXPORT ActiveAIChannel 
{
public:
  /**
   * Channel identifier
   */
  dsl::ChanId id;
  
  /**
   * Coupling
   */
  dsl::Coupling coupling;

  /**
   * Range
   */
  dsl::Range range;

  /**
   * Offset
   */
  dsl::Offset offset;

  /**
   * Bandwidth
   */
  dsl::Bandwidth bandwidth;

  /**
   * Input impedance
   */
  dsl::Impedance impedance;

  /**
   * Default constructor.
   */
  ActiveAIChannel () 
	  : id (0), 
	    coupling(DSL_DEFAULT_COUPLING), 
		  range(DSL_DEFAULT_RANGE),
      offset(DSL_DEFAULT_OFFSET),
		  bandwidth(DSL_DEFAULT_BANDWIDTH),
		  impedance(DSL_DEFAULT_IMPEDANCE)
  {/*noop*/};

  /**
   * Copy constructor.
   * @param _src The ActiveAIChannel to copy
   */
  ActiveAIChannel (const ActiveAIChannel& _src)
  {
    *this = _src;
  };

  /**
   * operator =.
   */
  const ActiveAIChannel & operator= (const ActiveAIChannel& _src)
  {
    if (&_src == this)
      return *this;
    id = _src.id;
    coupling = _src.coupling;
	  range = _src.range;
    offset = _src.offset;
	  bandwidth = _src.bandwidth;
	  impedance = _src.impedance;
    return *this;
  }

  /**
   * Destructor.
   */
  virtual ~ActiveAIChannel () {/*noop*/};
};

// ============================================================================
// BOARD::DownConverter::Attenuation
// ============================================================================
#define kMIN_DC_ATTENUATION    0.
#define kMAX_DC_ATTENUATION    50.
#define DSL_DEFAULT_DC_ATTENUATION kMIN_DC_ATTENUATION

// ============================================================================
// BOARD::DownConverter::Ref.Clock
// ============================================================================
typedef enum
{
  dc_internal_ref_clock = 0,
  dc_external_ref_clock
} DCReferenceClock;
//--------------------------------------
#define DSL_DEFAULT_DC_REF_CLOCK dc_internal_ref_clock


} // namespace dsl

#endif // _RSRC_H_

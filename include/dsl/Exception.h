// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Exception.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h>
#include <string>
#include <vector>

namespace dsl {

// ============================================================================
// DSL Errors severities 
// ============================================================================
typedef enum {
  WARN, 
  ERR, 
  PANIC
} ErrorSeverity;

// ============================================================================
//! The DSL exception abstraction base class.  
// ============================================================================
class DSL_EXPORT Error
{
public:

  /**
   * Initialization. 
   */
  Error ();

  /**
   * Initialization. 
   */
  Error (const char *reason,
				 const char *desc,
				 const char *origin,
	       int err_code = -1, 
	       int severity = dsl::ERR);
  

  /**
   * Initialization. 
   */
  Error (const std::string& reason,
				 const std::string& desc,
				 const std::string& origin, 
	       int err_code = -1, 
	       int severity = dsl::ERR);

  /**
   * Copy constructor. 
   */
  Error (const Error& src);

  /**
   * Error details: code 
   */
  virtual ~Error ();

  /**
   * operator= 
   */
  Error& operator= (const Error& _src);

  /**
   * Error details: reason 
   */
  std::string reason;

  /**
   * Error details: description 
   */
  std::string desc;

  /**
   * Error details: origin 
   */
  std::string origin;

  /**
   * Error details: code 
   */
  int code;

  /**
   * Error details: severity 
   */
  int severity;

};

// ============================================================================
// The DSL error list.	
// ============================================================================
typedef std::vector<Error> ErrorList;

// ============================================================================
//! The DSL exception abstraction base class.  
// ============================================================================
class DSL_EXPORT Exception
{
public:

  /**
   * Initialization. 
   */
  Exception ();

  /**
   * Initialization. 
   */
  Exception (const char *reason,
					   const char *desc,
					   const char *origin,
	           int err_code = -1, 
	           int severity = dsl::ERR);
  
  /**
   * Initialization. 
   */
  Exception (const std::string& reason,
					   const std::string& desc,
					   const std::string& origin, 
	           int err_code = -1, 
	           int severity = dsl::ERR);

  /**
   * Initialization. 
   */
  Exception (const Error& error);


  /**
   * Copy constructor. 
   */
  Exception (const Exception& src);

  /**
   * operator=
   */
  Exception& operator= (const Exception& _src); 

  /**
   * Release resources.
   */
  virtual ~Exception ();

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const char *reason,
					         const char *desc,
						       const char *origin, 
		               int err_code = -1, 
		               int severity = dsl::ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const std::string& reason,
                   const std::string& desc,
                   const std::string& origin, 
                   int err_code = -1, 
                   int severity = dsl::ERR);

  /**
   * Push the specified error into the errors list.
   */
  void push_error (const Error& error);

  /**
   * The errors list
   */
   ErrorList errors;
};

} // namespace dsl

#if defined (__DSL_INLINE__)
# include "dsl/Exception.i"
#endif // __DSL_INLINE__

#endif // _DSL_EXCEPTION_H_


// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Waveform.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _WAVEFORM_H_
#define _WAVEFORM_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DSLConfig.h>
#include <dsl/AIData.h>

namespace dsl {

// ============================================================================
//! The DSL Waveform abstraction class.  
// ============================================================================
struct DSL_EXPORT Waveform
{
  /**
   * Initialization. 
   */
  Waveform ();
  
  /**
   * Release resources.
   */
  virtual ~Waveform ();

  /**
   * The associated data: abscissa
   */
  AIScaledData * abs;

  /**
   * The associated data: ordinate
   */
  AIScaledData * ord;

  /**
   * The relative timestamp of the first sample 
   */
  double relative_ts;

  /**
   * The absolute timestamp of the first sample 
   */
  double absolute_ts;

  /**
   * The x increment
   */
  double x_increment;

  /**
   * The gain 
   */
  double gain;

  /**
   * The offset 
   */
  double offset;

private:
  // = Disallow these operations.
  //--------------------------------------------
  Waveform& operator= (const Waveform&);
  Waveform(const Waveform&);
};

// ============================================================================
//! The DSL Waveforms abstraction class.  
// ============================================================================
class DSL_EXPORT Waveforms 
{
public:
  
  /**
   * Initialization. 
   */
  Waveforms ();
  
  /**
   * Release resources.
   */
  virtual ~Waveforms ();

  /**
   * Adds a item at the end of the list.
   */
  void push_back (Waveform * wfm);

  /**
   * operator[].
   */
  Waveform * operator[] (size_t i) const;

  /**
   * Return the size (in bytes) of the actual data. 
   * @return size of underlying data in bytes.
   */
  virtual size_t size () const;

  /**
   * Number of dsl::Waveform in the list
   */
  size_t num_wfms () const;

private:
  //- waveforms repository
  std::vector<Waveform*> v;

  // = Disallow these operations.
  //--------------------------------------------
  Waveforms& operator= (const Waveforms&);
  Waveforms(const Waveforms&);
};

} // namespace dsl

#if defined (__DSL_INLINE__)
# include "dsl/Waveform.i"
#endif // __DSL_INLINE__

#endif // __WAVEFORM_H_


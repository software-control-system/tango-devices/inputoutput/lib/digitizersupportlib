// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    NIRsrc.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _NI_RSRC_H_
#define _NI_RSRC_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
//-include required ni-scope headers 
#if ! defined (_SIMULATION_)
#	include <niscope.h>
#	ifdef DOWN_CONVERTER_SUPPORT
#  include <nituner.h>
# endif 
#endif
 
#define NI_BOARDS_ID_OFFSET 0

namespace dsl {

// ============================================================================
// MISC TYPEs
// ============================================================================
#if ! defined (_SIMULATION_)
	typedef ViSession HardwareHandle;
	typedef ViStatus DigitizerErr;
#else
	typedef unsigned long HardwareHandle;
	typedef unsigned long DigitizerErr;
#endif

// ============================================================================
// DIGITIZERS: DUMMY HARDWARE HANDLE  
// ============================================================================
#define kUNDEFINED_HH   0
#define kUNDEFINED_TID -1

// ============================================================================
// DIGITIZERS: IDENTIFIER
// ============================================================================
typedef enum 
{
  //- NI-5122
  NI_5122 = NI_BOARDS_ID_OFFSET,
  //- NI-5620
  NI_5620
} 
NIDigitizers;

// ============================================================================
// DIGITIZERS: STRING IDENTIFIER
// ============================================================================
//- NI-5122
#define NI_5122_STR_ID "NI::5122"
//- NI-5620
#define NI_5620_STR_ID "NI::5620"

// ============================================================================
// DOWNCONVERTERS: IDENTIFIER
// ============================================================================
typedef enum 
{
  //- NI-5600
  NI_5600 = NI_BOARDS_ID_OFFSET
} 
NIDownConverters;

// ============================================================================
// DIGITIZERS: STRING IDENTIFIER
// ============================================================================
//- NI-5600
#define NI_5600_STR_ID "NI::5600"

// ============================================================================
// DIGITIZERS (AND OTHER HARDWARE): NUM CHANNELS
// ============================================================================
#define NI_5122_NUM_CHANNELS 2
#define NI_5600_NUM_CHANNELS 1
#define NI_5620_NUM_CHANNELS 2

// ============================================================================
// DIGITIZERS: MAX SAMPLING RATE
// ============================================================================
//- NI-5122: 100 Msamples/sec
#define NI_5122_MAX_SAMPLING_RATE 100000000
//- NI-5620: 100 Msamples/sec
#define NI_5620_MAX_SAMPLING_RATE 64000000

// ============================================================================
// DIGITIZERS: INPUT IMPEDANCE
// ============================================================================
//- NI-5122: 50 Ohms
#define NI_5122_INPUT_IMPEDANCE 50
//- NI-5620: 50 Ohms
#define NI_5620_INPUT_IMPEDANCE 50

// ============================================================================ 
// ERROR HANDLING: error code to error text
// ============================================================================ 
const char* niscope_error_text (dsl::HardwareHandle _hh, int _err_code);

#ifdef DOWN_CONVERTER_SUPPORT
  const char* nituner_error_text (int _task_id, int _err_code);
#endif

} // namespace dsl

#endif // _NI_RSRC_H_

// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    NI5620.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifdef DOWN_CONVERTER_SUPPORT

#ifndef _NI_5620_H_
#define _NI_5620_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <map>
#include <yat/threading/Mutex.h>
#include <dsl/DSLConfig.h>
#include <dsl/ni/NIDigitizer.h>

namespace dsl {

// ============================================================================
//! The NI 5620 digitizer abstraction.  
// ============================================================================
class DSL_EXPORT NI5620 : public NIDigitizer
{
  typedef std::map<unsigned short, NI5620*> Repository;

  typedef Repository::iterator RepositoryIterator;

  typedef Repository::value_type RepositoryValue;

public:

  /**
   * Instanciate a NI5620 board.
   * @param _id The number of the card in the cPCI crate.
   * @param _exclusive_access Allows or not the exclusive access (exclusive access by default).
   * @return A pointer to the card.
   * @throw DAQException 
   */
  static NI5620* instanciate (unsigned short _id, bool _exclusive_access = true)
    throw (DAQException);

  /**
   * Continuous AI configuration.
   * @param config The configuration.
   */
  virtual void configure_continuous_ai (const dsl::ContinuousAIConfig& config) 
    throw (DAQException);

  /**
   * Duplicate this shared object (if no exclusive access).
   */
   NI5620 * duplicate ();

  /**
   * Returns the numbers of channels of the board.
   */
  virtual unsigned short num_channels () const;

  /**
   * Dump hardware info.
   */
  virtual void dump () const;

protected:

  /**
   * Constructor.
   */
	NI5620 (unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~NI5620();

  /**
   * Returns the maximum sampling rate in Hz.
   */
	virtual double max_sampling_rate () const;

  /**
   * Returns the input impedance in Ohms.
   */
	virtual double input_impedance () const;

private:
  /**
   * Boards repository.
   */
  static Repository repository;
  
  /**
   * Boards repository' lock.
   */
  static yat::Mutex repository_lock;
};

} // namespace dsl

#endif // _NI_5620_H_

#endif // DOWN_CONVERTER_SUPPORT

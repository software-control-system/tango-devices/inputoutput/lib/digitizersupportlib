// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed NIDownConverter Support Library
//
// = FILENAME
//    NIDownConverter.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifdef DOWN_CONVERTER_SUPPORT

#ifndef _NI_DOWNCONVERTER_H_
#define _NI_DOWNCONVERTER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/DAQException.h>
#include <dsl/DownConverter.h>

namespace dsl {

// ============================================================================
//! The NIDownConverter boards abstract base class 
// ============================================================================
class DSL_EXPORT NIDownConverter : public DownConverter
{
public:

  /**
   * Digitizer factory.
   * @param _type The type of the board.
   * @param _id The number The number of the card in the cPCI crate.
   * @param _exclusive_access Get exclusive access to the hardware (if set to true).
   * @return The instanciate board.
   */
  static DownConverter * instanciate (unsigned short _type, 
			                                unsigned short _id, 
			                                bool _exclusive_access)
    throw (DAQException);

  /**
   * Check board registration. 
   * Throws a DAQException if the board has an invalid registration id. Does
   * nothing otherwise.
   */
  virtual void check_registration () const 
    throw (DAQException);

  /**
   * Set attenuation.
   * @param a The new attenuation value.
   */
  virtual void set_attenuation (const Attenuation& a) 
    throw (DAQException);

  /**
   * Set tuning.
   * @param a The new attenuation value.
   */
  virtual void set_tuning (const Tuning& t) 
    throw (DAQException);

  /**
   * Set the reference clock.
   * @return The current tuning.
   */
  virtual void set_reference_clock (DCReferenceClock rc) 
    throw (DAQException);

protected:

  /**
   * Constructor.
   */
  NIDownConverter (unsigned short _type, unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~NIDownConverter();

  /**
   * Register the board with the driver.
   */
  virtual int register_hardware ();

  /**
   * Unregister the board with the driver.
   */
  virtual int release_hardware ();

  /**
   * Task id (niTuner specific) 
   */
  int task_id_;
};

} // namespace dsl_ni

#endif // _NI_DOWNCONVERTER_H_

#endif // DOWN_CONVERTER_SUPPORT

// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    NI5600.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifdef DOWN_CONVERTER_SUPPORT

#ifndef _NI_5600_H_
#define _NI_5600_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <map>
#include <yat/threading/Mutex.h>
#include <dsl/DSLConfig.h>
#include <dsl/ni/NIDownConverter.h>

namespace dsl {

// ============================================================================
//! The NI 5600 digitizer abstraction.  
// ============================================================================
class DSL_EXPORT NI5600 : public NIDownConverter
{
  typedef std::map<unsigned short, NI5600*> Repository;

  typedef Repository::iterator RepositoryIterator;

  typedef Repository::value_type RepositoryValue;

public:

  /**
   * Instanciate a NI5600 board.
   * @param _id The number of the card in the cPCI crate.
   * @param _exclusive_access Allows or not the exclusive access (exclusive access by default).
   * @return A pointer to the card.
   * @throw DAQException 
   */
  static NI5600* instanciate (unsigned short _id, bool _exclusive_access = true)
    throw (DAQException);

  /**
   * Duplicate this shared object (if no exclusive access).
   */
   NI5600 * duplicate ();

  /**
   * Returns the numbers of channels of the board.
   */
  virtual unsigned short num_channels () const;

  /**
   * Dump hardware info.
   */
  virtual void dump () const;

protected:

  /**
   * Constructor.
   */
	NI5600 (unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~NI5600();

private:
  /**
   * Boards repository.
   */
  static Repository repository;
  
  /**
   * Boards repository' lock.
   */
  static yat::Mutex repository_lock;
};

} // namespace dsl

#endif // _NI_5600_H_

#endif // DOWN_CONVERTER_SUPPORT

// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed NIDigitizer Support Library
//
// = FILENAME
//    NIDigitizer.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _NI_DIGITIZER_H_
#define _NI_DIGITIZER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include <dsl/CalibrableHardware.h>
#include <dsl/DAQException.h>
#include <dsl/Digitizer.h>
#include <dsl/ContinuousAIConfig.h>

namespace dsl {

// ============================================================================
//! The NIDigitizer boards abstract base class 
// ============================================================================
class DSL_EXPORT NIDigitizer : public Digitizer
{
public:
  /**
   * Digitizer factory.
   * @param _type The type of the board.
   * @param _id The number The number of the card in the cPCI crate.
   * @param _exclusive_access Get exclusive access to the hardware (if set to true).
   * @return The instanciate board.
   */
  static Digitizer * instanciate (unsigned short _type, 
			                            unsigned short _id, 
			                            bool _exclusive_access)
    throw (DAQException);

  /**
   * Continuous AI configuration.
   * @param config The configuration.
   */
  virtual void configure_continuous_ai (const dsl::ContinuousAIConfig& config) 
    throw (DAQException);

  /**
   * Start continous AI.
   */
  virtual void start_continuous_ai () 
    throw (DAQException);

  /**
   * Stop continous AI.
   */
  virtual void stop_continuous_ai () 
    throw (DAQException);

  /**
   * Returns acquired data.
   */
  virtual dsl::AIScaledData * get_data ( unsigned long timeout_ms) 
    throw (DAQException);

  /**
   * Returns acquired waveforms.
   */
  virtual dsl::Waveforms * get_waveforms (size_t num_wfms, unsigned long timeout_ms) 
    throw (DAQException);

  /**
   * Performs hardware auto-calibration.
   */
  virtual void auto_calibrate () 
    throw (DAQException);

protected:
  /**
   * Constructor.
   */
  NIDigitizer (unsigned short _type, unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~NIDigitizer();

  /**
   * Register the board with the driver.
   */
  virtual int register_hardware ();

  /**
   * Unregister the board with the driver.
   */
  virtual int release_hardware ();

  /**
   * DAQ buffer depth
   */
  unsigned long daq_buffer_depth_;

  /**
   * Returns acquired data.
   */
  dsl::AIScaledData * get_data_i (unsigned long timeout_ms) 
    throw (DAQException);
  
private:
  /**
   * Active channels string
   */
  std::ostringstream chan_id_;
};

} // namespace dsl_ni

#endif // _NIDIGITIZER_H_

// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Waveform.i
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

namespace dsl {

// ============================================================================
// Waveforms::push_back
// ============================================================================
DSL_INLINE void Waveforms::push_back (dsl::Waveform* wfm)
{
  this->v.push_back(wfm);
}

// ============================================================================
// Waveforms::operator[]
// ============================================================================
DSL_INLINE dsl::Waveform* Waveforms::operator[] (size_t i) const
{
  i = (i < this->v.size()) ? i : this->v.size() - 1;
  
  return this->v[i];
}

// ============================================================================
// Waveforms::num_wfms
// ============================================================================
DSL_INLINE size_t Waveforms::num_wfms () const
{
  return this->v.size();
}


} // namespace dsl


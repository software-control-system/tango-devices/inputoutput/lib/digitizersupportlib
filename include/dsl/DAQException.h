// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    DAQException.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _DAQ_EXCEPTION_H_
#define _DAQ_EXCEPTION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/Exception.h>
#include <yat4tango/ExceptionHelper.h>
#include <dsl/DSLConfig.h>

namespace dsl {

// ============================================================================
//! DAQ exception class.  
// ============================================================================
class DSL_EXPORT DAQException : public yat::Exception
{
public:
  /**
   * Possible reasons for error.
   */
  enum {
    UNDEFINED = -1,
    NO_DAQ_TRIGGER,
    DAQ_BUFFER_OVERRUN,
    PROC_TASK_OVERLOAD,
    PERFORMING_CALIBRATION,
    RECOVERING_ERROR
  };
  
  /**
   * Initialization (unknown error). 
   */
  DAQException ();

  /**
   * Initialization. 
   */
  DAQException (const char *reason,
					      const char *desc,
					      const char *origin,
                int err_code = DAQException::UNDEFINED, 
                int severity = yat::ERR);

  /**
   * Initialization. 
   */
  DAQException (const std::string& reason,
					      const std::string& desc,
					      const std::string& origin,
                int err_code = DAQException::UNDEFINED, 
                int severity = yat::ERR);

  /**
   * Initialization (build an unknown exception caught from <origin>). 
   */
  DAQException (const char * origin);

  /**
   * Initialization (build an unknown exception caught from <origin>). 
   */
  DAQException (const std::string & origin);

  /**
   * Copy constructor 
   */
  DAQException(const yat::Exception & src);
  
  /**
   * Copy constructor 
   */
  DAQException(const DAQException & src);

  /**
   * Release resources.
   */
  virtual ~DAQException ();

  /**
   * operator=
   */
  DAQException& operator= (const DAQException& _src); 
  
  /**
   * Dump exception content onto the std ouput.
   */
  void dump () const;
};

// ============================================================================
//! Data lost exception class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class DSL_EXPORT TimeoutException : public DAQException
{
public:
  /**
   * Initialization. 
   */
  TimeoutException (int err_code);
  
  /**
   * Release resources.
   */
  virtual ~TimeoutException ();
};

// ============================================================================
//! Data lost exception class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class DSL_EXPORT DataLostException : public DAQException
{
public:
  /**
   * Initialization. 
   */
  DataLostException (int err_code);

  /**
   * Release resources.
   */
  virtual ~DataLostException ();
};

// ============================================================================
//! Device busy exception class.  
// ============================================================================
//!  
//! detailed description to be written
//! 
// ============================================================================
class DSL_EXPORT DeviceBusyException : public DAQException
{
public:
  /**
   * Initialization. 
   */
  DeviceBusyException (const char * reason,
					             const char * desc,
					             const char * origin,
                       int err_code = DAQException::UNDEFINED, 
                       int severity = yat::ERR);

  /**
   * Initialization. 
   */
  DeviceBusyException (const std::string & reason,
					             const std::string & desc,
					             const std::string & origin,
                       int err_code = DAQException::UNDEFINED, 
                       int severity = yat::ERR);
  
  /**
   * Release resources.
   */
  virtual ~DeviceBusyException ();
};

} // namespace dsl

#if defined (__DSL_INLINE__)
# include "dsl/DAQException.i"
#endif // __DSL_INLINE__

#endif // _DAQ_EXCEPTION_H_


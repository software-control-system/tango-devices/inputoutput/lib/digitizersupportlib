// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    Digitizer.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _DIGITIZER_H_
#define _DIGITIZER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/Rsrc.h>
#include <dsl/Waveform.h>
#include <dsl/CalibrableHardware.h>
#include <dsl/ContinuousAIConfig.h>
#include <dsl/Simulator.h> 

namespace dsl {

// ============================================================================
//! The digitizer boards abstract base class 
// ============================================================================
class DSL_EXPORT Digitizer : public CalibrableHardware
{
public:
  /**
   * Digitizer factory.
   * @param _type The board (i.e. hardware) identifier.
   * @param _id The board number on the PCI bus.
   * @param _calibration Calibration done in init phase (if set to true).
   * @param _exclusive_access Exclusive access to the hardware (if set to true).
   * @return The dsl::Digitizer instance.
   */
  static Digitizer * instanciate (unsigned short _type, 
			                            unsigned short _id, 
                                        bool _calibration,
			                            bool _exclusive_access = true)
    throw (DAQException);

  /**
   * Continuous AI (i.e. daq) configuration.
   * @param config The daq configuration.
   */
  virtual void configure_continuous_ai (const dsl::ContinuousAIConfig& config) 
    throw (DAQException) = 0;

  /**
   * Starts daq.
   */
  virtual void start_continuous_ai () 
    throw (DAQException) = 0;

  /**
   * Stops daq.
   */
  virtual void stop_continuous_ai () 
    throw (DAQException) = 0;

  /**
   * Returns acquired data.
   */
  virtual AIScaledData * get_data (unsigned long timeout_ms) 
    throw (DAQException) = 0;

  /**
   * Returns acquired waveforms.
   */
  virtual Waveforms * get_waveforms (size_t num_wfms, unsigned long timeout_ms) 
    throw (DAQException) = 0;

  /**
   * Returns the numbers of input channels.
   */
  virtual unsigned short num_channels () const = 0;

  /**
   * Check board registration. 
	 * Throws a DAQException if the board has an invalid registration identifier. 
   * Does nothing otherwise.
   */
	virtual void check_registration () const throw (DAQException);

#if defined(_SIMULATION_)
  /**
   * Set associated HW simulator (does not get ownership!)
   */
	void simulator (dsl::Simulator * s);
#endif

protected:

  /**
   * Constructor.
   */
  Digitizer (unsigned short _type, unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~Digitizer();

  /**
   * Returns the maximum sampling rate in Hz.
   */
	virtual double max_sampling_rate () const = 0;

  /**
   * Daq configuration .
   */
  ContinuousAIConfig ai_config_;

  /**
   * The driver session id (i.e. driver's digitizer identifier)
   */
  HardwareHandle hh_;
  
  /**
   *  HW simulator
   */
#if defined(_SIMULATION_)
  dsl::Simulator * simulator_;
#endif
};

} // namespace dsl

#endif // _DIGITIZER_H_

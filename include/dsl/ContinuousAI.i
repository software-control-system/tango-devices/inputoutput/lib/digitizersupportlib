// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ContinuousAI.i
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

namespace dsl {

// ============================================================================
// ContinuousAI::configure
// ============================================================================
DSL_INLINE void 
ContinuousAI::configure (ContinuousAIConfig * _config, size_t _tmo_ms) 
  throw (DAQException, DeviceBusyException)
{
  if (_config == 0) 
  {
    throw DAQException("invalid configuration specified",
                       "invalid configuration specified (null pointer)",
                       "ContinuousAI::configure");
  }
  
  this->configure(*_config, _tmo_ms);
}

// ============================================================================
// ContinuousAI::configuration
// ============================================================================
DSL_INLINE const ContinuousAIConfig & 
ContinuousAI::configuration () const
{
  return this->config_;
}

// ============================================================================
// ContinuousAI::initialized
// ============================================================================
DSL_INLINE bool
ContinuousAI::initialized () const
{
  return this->digitizer_ ? true : false;
}

// ============================================================================
// ContinuousAI::handle_timeout
// ============================================================================
DSL_INLINE void 
ContinuousAI::handle_timeout ()
{
  //-noop default implementation
} 

}
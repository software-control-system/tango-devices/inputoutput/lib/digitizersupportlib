// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ContinuousDAQ.i
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

namespace dsl {

// ============================================================================
// ContinuousDAQ::state
// ============================================================================
DSL_INLINE ContinuousDAQ::DaqState 
ContinuousDAQ::daq_state () const
{
  if (this->exception_status_ != HANDLING_NONE)
  {
	  switch (this->exception_status_)
    {
      case HANDLING_ERROR:
      case HANDLING_OVERRUN:
        return RUNNING;
        break;
      case HANDLING_CALIBRATION:
        return CALIBRATING_HW;
        break;
      default:
        break;
    }
  }
  return this->state_;
}

// ============================================================================
// ContinuousDAQ::daq_state_is
// ============================================================================
DSL_INLINE bool 
ContinuousDAQ::daq_state_is (DaqState s) const
{
  return this->state_ == s;
}
  
// ============================================================================
// ContinuousDAQ::daq_state_str
// ============================================================================
DSL_INLINE std::string
ContinuousDAQ::daq_state_str (DaqState s) const
{
	switch (s)
  {
		case ContinuousDAQ::STANDBY:
    	return std::string("STANDBY");
    	break;
		case ContinuousDAQ::RUNNING:
    	return std::string("RUNNING");
    	break;
 		case ContinuousDAQ::ALARM:
    	return std::string("ALARM");
    	break;
 		case ContinuousDAQ::CONFIGURING_HW:
    	return std::string("CONFIGURING HW");
    	break;
		case ContinuousDAQ::CALIBRATING_HW:
    	return std::string("CALIBRATING HW");
    	break;
		case ContinuousDAQ::FAULT:
    	return std::string("FAULT");
    	break;
    default:
      break;
  }
  return std::string("UNKNOWN");
}

// ============================================================================
// ContinuousDAQ::handling_exception
// ============================================================================
DSL_INLINE bool 
ContinuousDAQ::handling_exception () const
{
	return this->exception_status_ != ContinuousDAQ::HANDLING_NONE;
}

// ============================================================================
// ContinuousDAQ::validate_request
// ============================================================================
DSL_INLINE void
ContinuousDAQ::validate_request (const std::string & caller) const 
  throw (DeviceBusyException, DAQException)
{
  this->abort_request_if_busy(caller.c_str());
  
  this->abort_request_if_bad_state(caller.c_str());
}

// ============================================================================
// ContinuousDAQ::abort_request_if_busy
// ============================================================================
DSL_INLINE void
ContinuousDAQ::abort_request_if_busy (const std::string & caller) const 
  throw (DeviceBusyException)
{
  switch (this->exception_status_)
  {
    case ContinuousDAQ::HANDLING_ERROR:
    case ContinuousDAQ::HANDLING_OVERRUN:
      {
        throw DeviceBusyException("OPERATION_NOT_ALLOWED",
                                  "could not execute requested action - device is busy (handling exception)",
                                  caller.c_str(),
                                  DAQException::RECOVERING_ERROR);
      }
      break;
    case ContinuousDAQ::HANDLING_CALIBRATION:
      {
        throw DeviceBusyException("OPERATION_NOT_ALLOWED",
                                  "could not execute requested action - device is busy (calibrating DAQ hardware)",
                                  caller.c_str(),
                                  DAQException::PERFORMING_CALIBRATION);
      }
      break; 
    case ContinuousDAQ::HANDLING_NONE:
    default:
      break;
  }
}

// ============================================================================
// ContinuousDAQ::abort_request_if_bad_state
// ============================================================================
DSL_INLINE void
ContinuousDAQ::abort_request_if_bad_state (const std::string & caller) const 
  throw (DAQException)
{
  switch (this->state_)
  {
    case UNKNOWN:
      {
        throw DAQException("OPERATION_NOT_ALLOWED",
                           "could not execute requested action (DAQ is in <UNKNOWN> state)",
                           caller.c_str());
      }
      break;
    default:
      break;
  }
}

} // namespace dsl



// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed DownConverter Support Library
//
// = FILENAME
//    DownConverter.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifdef DOWN_CONVERTER_SUPPORT

#ifndef _DOWNCONVERTER_H_
#define _DOWNCONVERTER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <dsl/Rsrc.h>
#include <dsl/Hardware.h>

// ============================================================================
// DEPENDENCIES
// ============================================================================
#define kDEFAULT_SPAN 20.0
#define kDEFAULT_DESIRED_RF_FREQ 352.202

namespace dsl {

// ============================================================================
//! The DownConverter boards abstract base class 
// ============================================================================
class DSL_EXPORT DownConverter : public Hardware
{
public:

  /**
   * Attenuation
   */
   typedef struct _Attenuation
   {  
     _Attenuation::_Attenuation ()
       : reference_level(0), 
         mixer_level(0), 
         actual_attenuation(0.), 
         scale_factor(0.)
     {/*noop ctor*/};
     int reference_level;
     int mixer_level;
     double actual_attenuation;
     double scale_factor;
   } Attenuation;

  /**
   * Tuning
   */
   typedef struct _Tuning
   {  
     _Tuning::_Tuning ()
       : desired_rf_frequency(kDEFAULT_DESIRED_RF_FREQ), 
         span(kDEFAULT_SPAN), 
         actual_if_frequency(0.), 
         actual_rf_tuned_frequency(0.),
         frequency_shift(0.)
     {/*noop ctor*/};
     double desired_rf_frequency; // RD-WRT - in MHz
     double span; // RD-WRT - in MHz
     double actual_if_frequency; // RD only - in MHz
     double actual_rf_tuned_frequency; // RD only - in MHz
     double frequency_shift; // RD only - in MHz
   } Tuning;

  /**
   * DownConverter factory.
   * @param _type The type of the board.
   * @param _id The number The number of the card in the cPCI crate.
   * @param _exclusive_access Get exclusive access to the hardware (if set to true).
   * @return The instanciate board.
   */
  static DownConverter * instanciate (unsigned short _type, 
			                                unsigned short _id, 
			                                bool _exclusive_access = true)
    throw (DAQException);

  /**
   * Set attenuation.
   * @param a The new attenuation value.
   */
  virtual void set_attenuation (const Attenuation& a) 
    throw (DAQException) = 0;

  /**
   * Returns current attenuation.
   * @return The current attenuation.
   */
  virtual const Attenuation& get_attenuation () const;

  /**
   * Set tuning.
   * @param a The new attenuation value.
   */
  virtual void set_tuning (const Tuning& t) 
    throw (DAQException) = 0;

  /**
   * Get tuning.
   * @return The current tuning.
   */
  virtual const Tuning& get_tuning () const;

  /**
   * Set the reference clock.
   * @return The current tuning.
   */
  virtual void set_reference_clock (DCReferenceClock rc) 
    throw (DAQException) = 0;

protected:

  /**
   * Constructor.
   */
  DownConverter (unsigned short _type, unsigned short _id);

  /**
   * Destructor.
   */
  virtual ~DownConverter();

  /**
   * Attenuation
   */
  Attenuation att_;

  /**
   * Tuning
   */
  Tuning tuning_;
};

} // namespace dsl

#endif // _DOWNCONVERTER_H_

#endif // DOWN_CONVERTER_SUPPORT

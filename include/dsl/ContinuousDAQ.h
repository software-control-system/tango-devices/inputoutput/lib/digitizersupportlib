// ============================================================================
//
// = CONTEXT
//    TANGO Project - Hi Speed Digitizer Support Library
//
// = FILENAME
//    ContinuousDAQ.h
//
// = AUTHORS
//    N.Leclercq
//
// ============================================================================

#ifndef _CONTINUOUS_DAQ_H_
#define _CONTINUOUS_DAQ_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/threading/Mutex.h>
#include <dsl/DAQException.h>

// ============================================================================
// CONSTS
// ============================================================================
#define kDSL_kPOST_TIMEOUT  1000  //- 1s

namespace dsl {

// ============================================================================
//! The ASL ContinuousDAQ abstraction base class.  
// ============================================================================
//! Base class for all DSL continuous daq classes
// ============================================================================
class DSL_EXPORT ContinuousDAQ
{
  friend class ContinuousDAQHandler;

public:

  //- Continuous DAQ states
  //------------------------
  typedef enum 
  {
    STANDBY,
    RUNNING,
    ALARM,
    CONFIGURING_HW,
    CALIBRATING_HW,
    FAULT,
    UNKNOWN
  } DaqState;

  //- Continuous DAQ-end reasons
  //----------------------------
	typedef enum 
  {
    DAQEND_ON_UNKNOWN_EVT,
    DAQEND_ON_USER_REQUEST,
    DAQEND_ON_EXTERNAL_TRIGGER,
    DAQEND_ON_ERROR,
    DAQEND_ON_OVERRUN,
		DAQEND_ON_CALIB_REQUEST
  } DaqEndReason;
	
  /**
   * Initialization. 
   */
  ContinuousDAQ ();
  
  /**
   * Release resources.
   */
  virtual ~ContinuousDAQ ();

  /**
   * Initialize continuous DAQ.
   */
  virtual void init (unsigned short hw_type, unsigned short hw_id, bool calibration) 
    throw (DAQException, DeviceBusyException) = 0; 

  /**
   * Start continuous DAQ. 
   */
  virtual void start (size_t tmo_ms = 0) 
    throw (DAQException, DeviceBusyException) = 0; 

  /**
   * Stop continuous DAQ. 
   */
  virtual void stop (size_t tmo_ms = 0) 
    throw (DAQException, DeviceBusyException) = 0; 

  /**
   * Abort continuous DAQ. 
   */
  virtual void abort (size_t tmo_ms = 0) 
    throw (DAQException, DeviceBusyException) = 0; 

  /**
   * Performs hardware auto-calibration asynchronously.
   * This default implementation does nothing
   */
  virtual void calibrate_hardware ()
    throw (DAQException, DeviceBusyException);

  /**
   * Returns true if the calibration process completed, false otherwise. 
   * Doesn't say anything about acalibration success.
   */
  bool calibration_completed ();

  /**
   * Returns current DAQ state.
   */
	DaqState daq_state () const;

  /**
   * Returns true if <state_> == s, false otherwise
   */
  bool daq_state_is (DaqState s) const;
  
  /**
   * Returns a humanly readable state
   */
   std::string daq_state_str (DaqState s) const;

  /**
   * Returns true if an DAQ error has be latched, false otherwise
   */
   bool has_error () const;

   /**
   * Returns the latched DAQ error (might be empty)
   */
   const DAQException & error () const;

protected:
  /**
   * ExceptionStatus
   */
  typedef enum {
    //- no particular event!
	  HANDLING_NONE,  
    //- error notified, handling it!
	  HANDLING_ERROR,
    //- buffer overrun notified, handling it!
	  HANDLING_OVERRUN,
    //- auto calibration request, performing it!
	  HANDLING_CALIBRATION
  } ExceptionStatus;
	
  //- handling exeption? true if <exception_status_> != HANDLING_NONE
	ExceptionStatus exception_status_;
  
  /**
   * Returns true if <exception_status_> != HANDLING_NONE
   */
  bool handling_exception () const;

  /**
   * Throws the appriopriate exception if DAQ can't respond to external requests. 
   * Calls both abort_request_if_busy and abort_request_if_bad_state.
   */
  virtual void validate_request (const std::string & caller) const 
      throw (DeviceBusyException, DAQException);

  /**
   * Throws the appriopriate DeviceBusyException if DAQ
   * can't respond to external requests
   */
  virtual void abort_request_if_busy (const std::string & caller) const 
      throw (DeviceBusyException);

  /**
   * Throws the appriopriate DAQException if DAQ
   * can't respond to external requests
   */
  virtual void  abort_request_if_bad_state (const std::string & caller) const 
      throw (DAQException);

  /**
   * Latches the DAQ state and error
   * If <switch_to_fault> is set to <true> then switches state to <ContinuousDAQ::FAULT>
   * Does nothing if state is already set to <ContinuousDAQ::FAULT>
   */
  virtual void latch_error (const DAQException& e, bool switch_to_fault = true);

  /**
   * Reset the DAQ error then switches state <s>
   */
  virtual void reset_error (DaqState s);

  /**
   * A lock to protect the DAQ against race conditions.
   */
  yat::Mutex lock_;

  /**
   * Current DAQ state
   */
  DaqState state_;

  /**
   * DAQ error storage
   */
  DAQException daq_exception_;

private:
  // = Disallow these operations.
  //--------------------------------------------
  ContinuousDAQ& operator= (const ContinuousDAQ&);
  ContinuousDAQ(const ContinuousDAQ&);
};

} // namespace dsl

#if defined (__DSL_INLINE__)
# include "dsl/ContinuousDAQ.i"
#endif // __DSL_INLINE__

#endif // _CONTINUOUS_DAQ_H_

